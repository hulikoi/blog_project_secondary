<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 17/01/2019
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">
        <%--CSS External stylesheet--%>
        <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">
        <%--Bootstrap Icons--%>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
        <%--Votes (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Major+Mono+Display" rel="stylesheet">
        <%--Title (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
        <%--Date (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

        <title>Home Page</title>

    </head>

    <%@include file="Resources/nav.jsp" %>

    <body>
        <%--"slide-top" animation from external CSS stylesheet--%>
        <div class="container " id="slide-top">

            <br><br><br><br>

            <%--Display all articles that have been published--%>
            <core:forEach items="${HParticles}" var="HParticles">
                <jsp:useBean id="now" class="java.util.Date"/>
                <fmt:formatDate var="datenow" value="${now}" pattern="yyyy-MM-dd"/>
                <core:if test="${HParticles.publish_time<=datenow}">

                    <div class="card bg-light text-black">
                            <%--Display Banner picture--%>
                        <img class="card-img"
                             src="${HParticles.banner}" alt="Card image" height="270">
                            <%--Overlay ontop of banner image--%>
                        <div class="card-img-overlay">
                                <%--Display Vote Count--%>
                            <span style="float: right;">
                            <p style="font-family: 'Major Mono Display', monospace;font-size: large">${HParticles.votes} Votes</p>
                         </span>
                                <%--Title links to article--%>
                            <a href="SingleArticleServlet?articleID=${HParticles.articleID}" class="text-dark"
                               id="hp_article_title2" style="text-decoration: none">
                                <h5 class="card-title"
                                    style="font-family: 'Montserrat Subrayada', sans-serif;">${HParticles.title} </h5>
                            </a>
                                <%--View Article button links to article--%>
                            <p class="card-text" id="hp_article_content2"
                               style="font-family: 'Titillium Web', sans-serif;">${HParticles.publish_time}</p>
                            <a href="ArticleMaker?getuserarticles=true&articleUserID=${HParticles.user_id}"
                               target="_blank" class="text-dark">
                                <h6 id="hp_author2"
                                    style="font-family: 'Titillium Web', sans-serif;">${HParticles.user_id}</h6>
                            </a>
                        </div>
                    </div>
                    <br><br>

                </core:if>
            </core:forEach>

            <%--Credit artwork--%>
            <p style="color: grey">ALL IMAGES ARE FROM UNSPLASH(Aleksandar Iliev Tsvetkov|Olia Gozha|Evie
                                   Shaffer|Ricardo Gomez Angel)
                                   WEBSITE LOGO IS MADE USING PAINT.NET</p>
        </div>
    </body>
</html>