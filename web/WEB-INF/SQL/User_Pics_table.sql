create table group_project_userpics
(
  user_id varchar(80)  not null,
  url     varchar(200) null,
  constraint group_project_userpics_ibfk_1
    foreign key (user_id) references group_project_user_db (user_id)
      on delete cascade
);


INSERT INTO group_project_userpics (user_id, url) VALUES ('next', 'Photos/DEFAULT_1.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('next', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('next', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Batman', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Batman', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Batman', 'Photos/DEFAULT_1.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Robin', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Robin', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Robin', 'Photos/DEFAULT_1.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Wonderwoman', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Wonderwoman', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Wonderwoman', 'Photos/DEFAULT_1.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Cyborg', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Cyborg', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Cyborg', 'Photos/DEFAULT_1.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Shazam', 'Photos/DEFAULT_3.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Shazam', 'Photos/DEFAULT_2.jpg');
INSERT INTO group_project_userpics (user_id, url) VALUES ('Shazam', 'Photos/DEFAULT_1.jpg');

