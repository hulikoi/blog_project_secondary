CREATE TABLE group_project_article
(
    articel_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id varchar(80) NOT NULL,
    title varchar(200),
    content longtext,
    publish_time date,
    votes int DEFAULT 0,
    banner varchar(255),
    CONSTRAINT group_project_article_ibfk_1 FOREIGN KEY (user_id) REFERENCES group_project_user_db (user_id) ON DELETE CASCADE
);
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'The Anti-Vax Group Formerly Known as AVN', '<p>Those in the loop may already know that the long awaited name change for the Australian Vaccination Network (AVN), the anti-vaccine mouth piece of&nbsp;Meryl Dorey, ordered in late 2012 has recently occurred.</p>

<p>The new moniker for the group is Australian Vaccination-Skeptics Network, I assume the hyphenation is present so that the group might reasonably continue to use the AVN contraction making the transition easier.</p>

<p><strong>Backstory</strong></p>

<p>As reported on Sciblogs in 2012 in a <a href="http://sciblogs.co.nz/guestwork/2012/12/19/anti-vaccination-network-told-to-change-its-name-or-be-shut-down/">guest post</a> from Dr Racheal Dunlop (via an article originally posted on <a href="http://theconversation.edu.au/anti-vaccination-network-told-to-change-its-name-or-be-shut-down-11368">The Conversation</a>) the New South Wales Department of Fair Trading <a href="http://www.fairtrading.nsw.gov.au/About_us/News_and_events/Media_releases/2012_media_releases/20121215_australian_vaccination_network.html">ordered the AVN to change their name</a> citing:</p>

<blockquote>
<p>&ldquo;The Australian Vaccination Network does not present a balanced case for vaccination, does not present medical evidence to back-up its claims and therefore poses a serious risk of misleading the community,&rdquo;</p>
</blockquote>

<p>The AVN billed itself as an impartial information source on vaccine matters but even a cursory look at their website and associated materials reveals an organisation whose only purpose is to oppose vaccination. On the <a href="http://avn.org.au/vaccination-information/">information page</a> of their website the AVN states:</p>

<blockquote>
<p>&quot;the AVN says that you need to look at both sides of this issue, ask lots of questions, look at your own family&rsquo;s health history and then &ndash; and ONLY then &ndash; make a decision that you think is right for your child and yourself.&quot;</p>
</blockquote>

<p>further on,</p>

<blockquote>
<p>&quot;Let&rsquo;s look at it this way. If you were going out tomorrow to buy a new car and your budget was $35,000, you would not just go to the first car yard you came to and say, &ldquo;Here&rsquo;s $35,000. What can you give me?&rdquo; That would be stupid!</p>

<p>Instead, you would do research, read car magazines, speak with family and friends about their experiences, do some test drives and then, after you had satisfied yourself that you had found the right care [sic] for your needs, would you plonk down your hard-earned cash to buy a vehicle.&quot;</p>
</blockquote>

<p>This sounds reasonable, in the same vein what I would not recommend and would not do is to take as reliable information from a source whose only objective is to tear down the type of car I was looking at. It does not lend credibility to a view for it to only recognise the failings of a particular approach. Regardless, this is merely window dressing which provides a veneer of reasonableness for the group&#39;s anti-vaccination agenda.</p>

<p>This assessment may seem harsh and/or simplistic but it is difficult to come to any other conclusion considering the fact that every news story, article, personal anecdote, everything has some sort of negative light attributed to vaccines or the people who promote them.&nbsp;The assertion above can be refuted by a single instance of positive vaccine related coverage in any AVN publication. Have at it.</p>

<p>In 2010 the NSW Health Care Complaints Commission published a<a href="http://www.stopavn.com/HCCCPublicWarning.jpg"> public warning</a> advising the AVN to include a statement on their website stating that their purpose is anti-vaccine, that they do not provide medical advise and that vaccination decisions should be made with a doctor. The AVN appealed this warning and won but the judge in that case described Meryl Dorey as &quot;coy&quot; regarding they anti-vaccine nature of her group.</p>

<p><strong>Name Change</strong></p>

<p>As noted above, the AVN was ordered to change it&#39;s name in 2012 or risk being disincorporated. After fighting this order the name change has finally been instituted. Not everyone is happy with the change.</p>

<p>In an effort to restrict the AVN from using names that incorporate the word &quot;skeptic/sceptic&quot; members of the Australian Skeptics <a href="http://www.danbuzzard.net/journal/anti-vaxxers-resort-to-identity-fraud.html">registered variations </a>of the AVN name with the word &quot;skeptic/sceptic&quot; included. One of these is very similar to the final name change opted for by the AVN. The action taken to prevent this outcome was undertaken due to the perception that scepticism is not the appropriate description for action taken by a group that advances a particular (scientifically unsupported) conclusion. Scepticism is a process, informed by science, by which we can move ever closer to the truth by clearing away misunderstandings and incorrect conclusions.</p>

<p>This is antithetical to the message of the AVN which is essentially &quot;We have the truth and that truth is: vaccines are harmful&quot;.</p>

<p>It will be interesting to see where this goes in future, legal challenges may well be in the works.</p>

<p>Regarding the AVN referring to themselves as skeptics, they follow in the footsteps of climate change skeptics so this is really nothing new. For the moment I&#39;m preferring to be optimistic, perhaps those on AVN&#39;s side of the fence will take the Skeptic label to heart and be open to all evidence, even if it contradicts their own views.</p>

<p>OK, perhaps that borders on the delusional side of optimism.</p>

<p><strong>Vaccine Education</strong></p>

<p>The AVN styles themselves an educational resource for parents investigating vaccines, news of their name change comes on the heels of recent research investigating the effect of <a href="http://www.sciencebasedmedicine.org/messaging-and-public-health/">different educational approaches</a> attempting to increase vaccine compliance.</p>

<p>The <a href="http://www.dartmouth.edu/~nyhan/vaccine-misinformation.pdf">study</a> was a survey based investigation which tried multiple methods of educating parents about vaccines. The disappointing result was that none of the approaches succeeded in the goal of increasing intent to vaccinate. All of the approaches had negative side effects, ranging from actual decrease in intent to vaccinate to increased belief in vaccine/autism link or other serious vaccine side effects.</p>

<p>While these results need to be investigated further they fit into a larger known pattern known as the &quot;backfire effect&quot; where corrective information ends up reinforcing the false belief rather than replacing it with the facts.</p>

<p>In a climate such as this any public effort to educate could result in lower vaccine uptake, especially among those pre-disposed to be wary of vaccines in the first place. Such is the danger when dealing with the tangled web of bias that is human cognition.</p>

<p><strong>Other resources on this story</strong></p>

<p><a href="http://www.patheos.com/blogs/tokenskeptic/2014/03/avn-renamed-as-australian-vaccination-skeptics-network-despite-skeptics-efforts/">Kylie Sturgess of Token Skeptic</a></p>

<p><a href="http://doubtfulnews.com/2014/03/anti-vaccination-network-in-australia-picks-new-name-digs-at-skepticism/">Sharon Hill - Doubtful News</a></p>

<p><a href="http://www.dailytelegraph.com.au/news/nsw/australian-anti-vaccination-network-changes-misleading-name/story-fni0cx12-1226850759205">Daily Telegraph</a></p>
', '2019-02-06', 6, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'Energy Mirages and the False Hope of "Water Powered Cars"', '<p>This morning while perusing my news feeds I saw <a href="http://tribune.com.pk/story/416542/the-water-car-fraud/" target="_blank">this article</a> lamenting the state of scientific ignorance and bald political grandstanding in Pakistan. The specific item that prompted this lambasting of an entire country is the claim by one individual to be able the fuel cars using water - and the near unanimous support of this character within the political and scientific realms - despite the physical impossibility of this feat.</p>

<p>I think the author of this article is quite correct in his condemnation of this person and those who support him who tout this technology as a <a href="http://www.post-gazette.com/stories/news/world/craving-energy-and-glory-pakistan-revels-in-boast-of-water-run-car-647728/" target="_blank">solution to the country&#39;s energy woes</a>. That said I think he does his audience a disservice in not breaking down the claims more fully to explain why this &quot;invention&quot; is not all that it seems and why it will not act as a panacea for the dependence on fossil fuels and the deficit of energy that Pakistan endures.*</p>

<p>It is explained that you cannot run a car on water due to the fact that that it would require a reversal of the second law of thermodynamics. A law that is deemed so fundamental to the operation of the universe that it prompted this quote from a distinguished scientist:</p>

<blockquote>
<p>&quot;The law that entropy always increases, holds, I think, the supreme position among the laws of Nature. If someone points out to you that your pet theory of the universe is in disagreement with Maxwell&#39;s equations &mdash; then so much the worse for Maxwell&#39;s equations. If it is found to be contradicted by observation &mdash; well, these experimentalists do bungle things sometimes. But if your theory is found to be against the second law of thermodynamics I can give you no hope; there is nothing for it but to collapse in deepest humiliation. &rdquo;</p>

<p>--<a href="http://simple.wikipedia.org/wiki/Second_law_of_thermodynamics" target="_blank">Sir Arthur Stanley Eddington, The Nature of the Physical World (1927)</a></p>
</blockquote>

<p>But this is not the end of the story. For while the &quot;inventor&quot; and his supporters use the word &quot;fuel&quot; to refer to the water, it is a misnomer as we normally understand the word. A fuel is something that supplies energy, it stores energy that is created by one of any number of processes and enables it to be used to do work seconds, hours or millennia after the energy was first produced.</p>

<p>This is what fossil fuels are - the condensed energy of biological processes that occurred millions of years ago. We tap this energy and use it to run our cars, and depending on where you live, the entire rest of our lives.</p>

<p>So what&#39;s this to do with water?</p>

<p>Well, simply put water is the end product of energy use. It is not a storage medium it is a waste product. It would be like saying you&#39;ll run your furnace on ash. You would be laughed out of the human race. But say you&#39;ll use the magical liquid of life - water - and for some reason people think there&#39;s something to this idea.</p>

<p>Now, what is the proclaimed inventor claiming? When you get right down to it he knows the water isn&#39;t a fuel. He is in effect using the water as a convenient hydrogen source. It is the hydrogen that runs the car, and presumably the &quot;water-kit&quot; enables the car to process this hydrogen as it would petrol. The kit also contains an electrolysis component that splits the water into hydrogen and oxygen. I am unaware as to whether the oxygen released is retained to react with the hydrogen or whether atmospheric oxygen is used for this.</p>

<p>In any case the energy for running the car comes not from the water, but the batteries used to extract the hydrogen. The hydrogen then becomes the interim energy storage medium and the &quot;fuel&quot; for the car.</p>

<p>What we have then is the energy generation being pushed back a step, instead of being done at the car via petrol, it will be handled by the country&#39;s power plants.</p>

<p>I can well imagine that there are benefits to converting cars to this set-up. It effectively turns your automobile into one of this new fangled electric cars without the downside of looking like a self-righteous dick*. There are benefits to using electric cars even if the ultimate power generation comes from fossil fuel consuming power plants (which by my calculation more than half of Pakistan&#39;s electricity comes from) such as local air quality improvements. The ability to deal with emissions at centralised locations and the possibility of sequestering that pesky CO2 at the source.</p>

<p>I suspect however that in the rush to embrace the technology at issue here these peripheral concerns are not really being considered. And for a country that already has <a href="http://en.wikipedia.org/wiki/List_of_power_stations_in_Pakistan" target="_blank">too little electricity for the population it has</a> (40% of the country has no access to electricity, and demand is ever increasing for those that do) this does not sound like such a great idea and won&#39;t result in everyone having unlimited fuel for their cars. It can only add to the pressure on the already over-taxed electrical grid.</p>

<p>In addition it is being implied (if not outright stated) that water could be used to<a href="http://www.aaj.tv/2012/08/pakistani-engineer-agha-waqar-invents-water-kit-to-run-vehicles/" target="_blank"> run generators</a>. This is where you could justifiably call fraud. While there are conceivable reasons why you might convert a car to &quot;run&quot; on water those reason evaporate when you try to argue that the same can be done for a generator. I&#39;m sure you can see why. You end up just inserting an extra step in the energy generation process, well more like a loop. You have to provide energy to the water to extract the hydrogen and then burn the hydrogen back to water to get the energy. Thanks to that second law thingy you will never get more energy out of that reaction than you out in.</p>

<p>Not only do you insert a completely useless extra step, in doing so you guarantee that the whole process is less efficient. You literally get less combustion for your buck.</p>

<p>I hope that no government official is seriously considering funding a project to replace generators with water powered devices, though I gather millions may be spent investigating the possibility of employing this technology in Pakistan. I don&#39;t know where that money (assuming people don&#39;t wake up by then) is intended to go.</p>

<p>This is the concern whenever fringe theories and technologies are held up as the solution to our problems, that money will be wasted on these rather than put toward more worthy projects.</p>

<p>-----------------------------------------------------------------------------------------------------------</p>

<p>* A more thorough treatment is <a href="http://www.newspakistan.pk/2012/08/04/dr-attaur-rehman-unsatisfied-water-kit-formula/" target="_blank">here</a>, by former chairman of the Pakistani - Higher Education Commission&nbsp; Dr Attaur Rehman.</p>

<p>** Just kidding. For what it&#39;s worth I think electric cars are really cool and if I could spare the dosh would love to have one. But I gather there is something of a stigma and well it&#39;s a joke - lets not analyse it too much eh?</p>

<p>Related articles</p>

<ul>
	<li><a href="http://www.todayonline.com/World/EDC120805-0000032/Pakistan-water-fulled-car-claims-spark-joy,-worry" target="_blank"><img alt="" src="http://i.zemanta.com/104679755_80_80.jpg" /></a><a href="http://www.todayonline.com/World/EDC120805-0000032/Pakistan-water-fulled-car-claims-spark-joy,-worry" target="_blank">Pakistan water-fulled car claims spark joy, worry</a></li>
</ul>
', '2019-02-06', 56, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Wonderwoman', 'Why Do People Use Alternative Medicine?', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>I often read that the reason people are turning to complementary/alternative/integrative(take your pick) medicine is because they are dissatisfied with the care received from mainstream/conventional/&quot;western&quot;* medicine. This may be true for a small segment of the population, those with a chronic illness or with terminal cancer spring to mind. But is this generally true of altmed users? Those who pick up a bottle of homeopathic remedy from the pharmacy or occasionally visit a naturopath?</p>

<p>I don&#39;t think it&#39;s quite as simple as that. A study &quot;<a href="http://jama.ama-assn.org/content/279/19/1548.long" target="_blank">Why Patients Use Alternative Medicine</a>&quot; published in 1998 in the JAMA looked at this question using a survey sent to randomly selected participants. 1500 participants were sent the survey and 1035 completed it. Not too bad for a survey response rate.</p>

<p>The survey was geared to look at the use of altmed based on three paradigms:</p>

<blockquote>
<p>&quot;1. Dissatisfaction: Patients are dissatisfied with conventional treatment because it has been ineffective,5-6 has produced adverse effects, or is seen as impersonal, too technologically oriented, and/or too costly.</p>

<p>2. Need for personal control: Patients seek alternative therapies because they see them as less authoritarian16 and more empowering and as offering them more personal autonomy and control over their health care decisions.</p>

<p>3. Philosophical congruence: Alternative therapies are attractive because they are seen as more compatible with patients&#39; values, worldview, spiritual/religious philosophy, or beliefs regarding the nature and meaning of health and illness.&quot;</p>
</blockquote>

<p>According to the survey results satisfaction, or lack thereof,&nbsp; with conventional medicine did not correlate well with altmed use. 54% of respondents reported being &quot;highly satisfied&quot; with conventional medicine providers, of these 39% use alternative therapies. Of those who were highly dissatisfied (40%) only 9% were users of altmed.</p>

<p>It seemed as if those who were fans of medicine overall were more likely to participate in both camps. A sort of &quot;the more the merrier&quot; approach to health care.</p>

<p>What was predictive of alternative medicine use was personal philosophy. Those who considered there to be a strong mind/body/spirit connection as well as those who had had a &quot;transformational experience&quot; were more likely to use alt med than those who did not.</p>

<p>Education and health status also correlated with altmed use. Those with higher educations were more likely to use it, as were those who described themselves as having a lower health status.</p>

<p>The situation was slightly different for those who shunned conventional medicine altogether in order to embrace altmed. These folks tended to be distrustful of and dissatisfied with conventional practitioners, as well they desired a high degree of control over their health and believed in the importance and value of &quot;inner experiences&quot;.</p>

<p>This proportion of the population was quite small however - only 4.4% of the survey respondents fell into this group. Even so somehow the reasons for this group&#39;s embrace of altmed has been generalised to the wider population.</p>

<p>The observation that users of altmed tend to be greater consumers of health services overall is also supported by the paper &quot;<a href="http://jama.ama-assn.org/content/282/7/651.long" target="_blank">Association Between Use of Unconventional Therapies and Conventional Medical Services</a>&quot;. This survey had a base of 16,068 individuals from which to pull data representing a 77% response rate from the 24,676 pool that was originally sampled.</p>

<p>According to this survey only 6.5% of the population use both altmed and conventional medicine** (and 1.8% using only altmed), with this group making more visits to their physician than those who used conventional medicine only. One possible reason for this is the so-called &quot;worried well&quot;, a portion of the population that focuses on their health to a degree higher than would be expected given their health status. Support for this is given within the paper:</p>

<blockquote>
<p>&quot;Compared with those with only conventional visits, those who used both types of care had significantly more outpatient physician visits (7.9 vs 5.4; <em>P</em>&lt;.001), and used more of all types of preventive services except mammography. These groups did not differ significantly in inpatient care, prescription drug use, or number of emergency department visits.&quot;</p>
</blockquote>

<p>This on it&#39;s own does not show a &quot;worried well&quot; connection but in the comments section of the paper it was noted:</p>

<blockquote>
<p>&quot;...there was no difference in any of the 4 self-reported health measures between respondents who had physician visits only, and those who had those visits in conjunction with unconventional therapy. Poor health status appeared to drive use of health services in general, that is, those using no services reported better health than those using either conventional medical services or unconventional therapies. However, <strong>poor health was not associated with increased use of unconventional therapies over and above conventional medical care.</strong>&quot; [emphasis added]</p>
</blockquote>

<p>So it would seem, at least in this sample, that dissatisfaction with conventional care cannot be the driving force for the majority of altmed users. More plausible is that altmed users seek to make the most of every perceived avenue for health.</p>

<p>Another survey published in 2001 also supported the general conclusion that dissatisfaction with conventional medicine does not lead to altmed use for most consumers. &quot;<a href="http://www.annals.org/content/135/5/344.short" target="_blank">Perceptions about Complementary Therapies Relative to Conventional Therapies among Adults Who Use Both: Results from a National Survey</a>&quot; surveyed 831 respondents who used both regular and alternative medicine.</p>

<p>Of these 70% would visit a conventional medicine practitioner as their first port of call. Only 15% went to a altmed provider first. There was also no significant difference in the level of confidence in altmed providers and regular medical professionals.</p>

<p>To quote the conclusion:</p>

<blockquote>
<p>&quot;National survey data do not support the view that use of CAM therapy in the United States primarily reflects dissatisfaction with conventional care.&quot;</p>
</blockquote>

<p>From a paper presented at the Proceedings of the 1997 Conference of<br />
the Australian Association for Social Research and published in the Journal of Sociology; &quot;<a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.113.4086&amp;rep=rep1&amp;type=pdf#page=117" target="_blank">Postmodern values, dissatisfaction with conventional medicine and popularity of alternative therapies</a>&quot;[PDF File download]:</p>

<blockquote>
<p>&quot;Those individuals who value natural remedies, are against chemical drugs, do not favour technological progress, and welcome variety in choice of therapy are more likely to have a positive attitude towards alternative medicine.&quot;</p>
</blockquote>

<p>These attitudes were enveloped under the &quot;postmodern&quot; rubric and were found to be a better predictor of altmed use than satisfaction levels with regard the conventional medicine.</p>

<p>To elaborate on that point, a further finding was that dissatisfaction with <em>interactions</em> with physicians rather than health <em>outcomes</em> was associated altmed use. This is a subtle point and worth dwelling on as it seems to be a valid criticism of the way in which conventional medicine is practised. It was not that altmed users were unhappy with the actual results of the care received via conventional medicines but the way in which they feel they are treated by doctors.</p>

<p>It seems that those turning to altmed may feel that conventional doctors do not give enough respect, time, don&#39;t listen and are too authoritative. I don&#39;t want to put too much emphasis on this perspective as it isn&#39;t entirely consistent with the picture built up so far and the sample size of this survey was relatively small compared with the ones above (only 209 respondents), but it is worth considering.</p>

<p>In conclusion, while it might be true that some dissatisfaction does lead to an increase in the use of alternative medicine it seems unlikely to me that this is the main reason. I&#39;m not sure why it has become the go-to reason trotted out by participants on both sides of the debate, ease I suppose. I could of course be wrong, perhaps there is a mountain of research out there that I&#39;ve missed pointing in the complete opposite direction. I&#39;m willing to grant that possibility, in the absence of such though I&#39;ll have to go with personal philosophy being the largest contributing reason people use altmed.</p>

<p>--------------------------------------------------------------------</p>

<p>*I hate with a passion the label &quot;Western Medicine&quot;, what? - people from other cultures can&#39;t use science? Nonsense.</p>

<p>** I suspect that the wildly differing definitions of what constitutes &quot;Alternative&quot; medicine are to be blamed for the fluctuating figures around the proportion of users.<br />
-------------------------------------------------------------------------</p>

<p>Astin, J. (1998). Why Patients Use Alternative Medicine: Results of a National Study JAMA: The Journal of the American Medical Association, 279 (19), 1548-1553 DOI: <a href="http://dx.doi.org/10.1001/jama.279.19.1548">10.1001/jama.279.19.1548</a></p>

<p>Eisenberg DM, Kessler RC, Van Rompay MI, Kaptchuk TJ, Wilkey SA, Appel S, &amp; Davis RB (2001). Perceptions about complementary therapies relative to conventional therapies among adults who use both: results from a national survey. Annals of internal medicine, 135 (5), 344-51 PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/11529698">11529698</a></p>

<p>Druss, B. (1999). Association Between Use of Unconventional Therapies and Conventional Medical Services JAMA: The Journal of the American Medical Association, 282 (7), 651-656 DOI: <a href="http://dx.doi.org/10.1001/jama.282.7.651">10.1001/jama.282.7.651</a></p>

<p>Siahpush, M. (1998). Postmodern values, dissatisfaction with conventional medicine and popularity of alternative therapies Journal of Sociology, 34 (1), 58-70 DOI: <a href="http://dx.doi.org/10.1177/144078339803400106">10.1177/144078339803400106</a></p>

<p>Aditional reading:</p>

<p>Joy, J.M. (2004). <a href="http://thinktech.lib.ttu.edu/ttu-ir/bitstream/handle/2346/14516/31295019601268.pdf?sequence=1" target="_blank">Complementary and Alternative Medicine (CAM): Do Barriers to and Dissatisfaction with Traditional Care Affect CAM Utilization Patterns</a>, Masters Thesis, Texas Tech University Health Sciences Center</p>

<p>Related articles</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2012/01/24/chiropractic-for-the-21st-century/">Chiropractic for the 21st Century</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2012/03/21/first-do-no-harm/">First, Do No Harm</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2012/02/16/the-freedom-of-ignorance-health-freedom-what-is-it-and-do-we-want-it/">The Freedom of Ignorance: Health Freedom, What is it and Do We Want it?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2012/01/13/thai-yoga-massage-herald-wherefore-art-thou-sense/">Thai Yoga Massage: Herald, Wherefore Art Thou Sense?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2011/12/06/breaking-news-have-constipation-moxibustion-wont-help/">Breaking News!: Have Constipation? Moxibustion Won&rsquo;t Help!</a> (scepticon.wordpress.com)</li>
</ul>
', '2019-02-06', 98, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'First, Do No Harm', '<p>Primum non nocere</p>

<p>A while ago I was <a href="http://sciblogs.co.nz/skepticon/2010/11/01/anti-vaccination-in-nz/comment-page-1/#comment-495" target="_blank">accused</a> of advocating that doctors should break the Hippocratic oath. That the cardinal rule of &quot;First, do no harm&quot; should be ignored. Setting aside that this phrase <a href="http://en.wikipedia.org/wiki/Hippocratic_Oath#Modern_version" target="_blank">does not appear in the Hippocratic oath</a>, what do we mean by harm in the context of medicine?</p>

<p>I was reminded of this by a post on <a href="http://www.sciencebasedmedicine.org/index.php/keeping-the-customer-satisfied/" target="_blank">Science Based Medicine by David Gorski</a> in which he muses over the measure of patient satisfaction as a proxy for how well hospitals meet their obligations with regard to patient care. To kick it off the good doctor notes that many interventions used by modern medicine cause harm, often direct harm.</p>

<p>Does this mean that doctors are throwing out their obligation not to harm patients? No, because we recognise that the concept of harm in this case includes those harms that would occur were we to withhold treatment as well as recognising that the total harm is reliant on the amount of benefit obtained by the patient.</p>

<p>The accusation against me was in the context of my arguing against the claims of anti-vaccinationists that vaccines do more harm than good. I don&#39;t want to rehash that argument here but I do want to dwell a bit on our concept of harm and how it applies in the medical arena.</p>

<p>One of the themes that return to over and over again on this blog is that of risk vs benefit. The amount of harm or risk can only be appropriately assessed in light of the benefit accrued. As pointed out by doctor Gorski there are many procedures that hurt, they hurt a lot. Should a doctor refuse to perform them then? Even if they could save a patient&#39;s life?</p>

<p>Of course not.</p>

<p>It is plainly ridiculous to assert that short term harm out weighs long term benefit, it might but that calculation has to be made in each case. In many cases the benefit will be clear, in others less so. If a patient undergoes a painful procedure that is relatively short lived and then makes a full recovery then the choice is fairly simple. If the recovery is likely to be only partial and the patient&#39;s quality of life is ever after severely reduced then we may weigh up the benefits of that treatment differently.</p>

<p>What then of treatments that are good for most but may harm a few? These are the tricky cases and it depends on a few variables. One is can we identify the persons that will be harmed,&nbsp; second, how much harm are they likely to suffer and what percentage of the treatment population do they make up? Finally what total benefit will accrue to the population if treatment is green lit?</p>

<p>I listed these variables in the order of importance I estimate they have. If we can identify prospectively harmed persons then they may be removed from the treatment group, harm avoided. If this information is unavailable then we may move to the next criterion: how much harm will they suffer? If this is likely to be relatively mild then all to the good. If the harm is considerably more serious then we may stop the treatment altogether in order to avoid these instances. The last two variables may switch in order depending on the situation or individual values.</p>

<p>Should a vital treatment be withheld from the general population if a very small percentage with be greatly harmed by it? Frankly I don&#39;t know. Help - is there a Medical Ethicist in the house?</p>

<p>Often I&#39;ll find that the people who oppose modern medicine will emphasise the risks of medicine while over-hyping the benefits of alternative medicine. The claims that iatrogenic (caused by medical treatment) harms are enormous abound. This point of view seems completely ignore the benefits received by individuals and society by medicine in it&#39;s current form*.</p>

<p>This seems perverse to me, the idea appears to be that any risk is unacceptable - a completely untenable position to my mind - every action carries risk. I take my life in my hands every time I drive to work, but the risks a relatively low and the benefits are more important to me - and the majority of other drivers I suspect. To argue that we should abstain from automotive transport until it is completely safe misses the point entirely. As does decrying the risks of the majority of medical science in the face of the undoubted benefit received.</p>

<p>To conclude this ramble, harm or risk is part of the human condition. Arguably the complete removal of risk is not only impossible but not even desirable**. We have to live with risk and harm, the key is to ensure we balance these against the good that comes from acting in the world to oppose even greater harms.</p>

<p>----------------------------------------------------------------------------------------</p>

<p>*This is quite an extreme view and I would hasten to add that I don&#39;t think the majority of alternative medicine users (however you may break that group down) would subscribe to it, but it exists.</p>

<p>** Depending on how this might be achieved. Possibly we could make ourselves impervious to harm rather than removing everything that might harm us.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://www.sciencebasedmedicine.org/index.php/keeping-the-customer-satisfied/">Keeping the customer satisfied</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://scepticon.wordpress.com/2008/08/08/whats-the-harm/" target="_blank">What&#39;s The Harm</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2012/02/16/the-freedom-of-ignorance-health-freedom-what-is-it-and-do-we-want-it/" target="_blank">The Freedom of Ignorance: Health Freedom, What is it and Do We Want It?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2011/10/18/the-legitimate-risks-of-vaccines/" target="_blank">The Legitimate Risks of Vaccines</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/11/01/anti-vaccination-in-nz/" target="_blank">Anti Vaccination in NZ</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2011/10/17/defending-the-term-anti-vaccine/" target="_blank">Defending the Term &quot;Anti-Vaccine&quot;</a> (scepticon.wordpress.com)</li>
</ul>
', '2019-02-06', 45, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'The Role of Experiments in Science', '<p>In an attempt to widen my horizons I have been listening to the <a href="http://www.philosophynow.org/podcasts" target="_blank">Philosophy Now podcast</a> out of the UK. A recent episode concerned philosophy of science (<a href="http://www.philosophynow.org/podcasts/The_Limits_of_Science" target="_blank">ep 29</a>) and the host asked an interesting question: &quot;Why do we have to have experiments when sometimes we can get away with observations?&quot;.</p>

<p>The point he was trying to make was that when we think about science in an abstract way we also have the idea that experiments are at the core of the scientific method. This clashed somewhat with the person the host was talking with at the time who was emphasising observation as the way we confirm hypotheses in science.</p>

<p>The implication here is that we have two things, observations on the one hand and experiments on the other.</p>

<p>This appears to be a fairly common view, I have seen arguments accusing cosmology (specifically the big bang theory) of not being science because you can&#39;t perform an experiment to create a new universe. Similar arguments have been made for evolution.</p>

<p>I don&#39;t know how widely held this view is in the general population (as opposed to those who are set against certain findings of science) but the question of the podcast host implies that it&#39;s wide enough.</p>

<p>The problem with this view however is that there really aren&#39;t two things here that are different in kind. Rather, one is a sub-set of the other; experiments are a special kind of observation.</p>

<p>The whole point of an experiment is to interrogate nature in a specific kind of way. While we can passively observe an event and gain valuable information (say, watching the development of an embryo) we can also create an experiment that constrains the conditions in a particular way in order for us to draw more conclusive conclusions about the situation of interest (perhaps we knock out a gene and watch that embryo follow a different developmental path).</p>

<p>By using experiments we aren&#39;t doing anything fundamentally different, we are still observing what nature has to tell us about the world we inhabit, but we are trying to set up conditions that are meant to clarify what nature is saying. In this view experiments are nature&#39;s interpreter.*</p>

<p>Experiments also allow us to get access to things that we might not normally be able to see. For example high energy physics requires elaborate experiments in order to allow us to in some way visualise particles that are mind bogglingly small. We aren&#39;t creating the physics we observe we are simply delving into realms that would normally be hidden from us.</p>

<p>This was brought home to me a few years back when the attempts to listen for extraterrestrial signals by SETI were referred to as experiments. In this case we aren&#39;t setting up the conditions by which we control whether an ET sends us a signal, we are determining the conditions by which we would receive such a signal. At it&#39;s heart this activity is an observation, no different in it&#39;s intent from viewing a microbe under a microscope.</p>

<p>So it is that the ability to do or not do a experiment does not determine science from non-science (termed the demarcation problem and certainly not definitively settled). Experiments may have come to be thought of as the defining feature of science but they are really just a special case of something we all do every day - observe the world around us.</p>

<p>---------------------------</p>

<p>*If that&#39;s too narrow for you, how about experiments as nature&#39;s speech therapist?</p>
', '2019-02-06', 65, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Robin', 'The Freedom of Ignorance: Health Freedom, What is it and Do We Want it?', '<p>When policy around how herbal remedies, alternative medicines, supplements and all sorts of other practices outside the mainstream of medical practice is discussed the concept of &quot;Health Freedom&quot; inevitably comes up. It&#39;s not always couched in that term but the idea is that people should be free to choose whatever method of healthcare that they wish.</p>

<p>Sounds good right? Who wants to impinge in someone&#39;s freedom to make their own decisions? Isn&#39;t that what living in a free country is all about? Personal autonomy, the right to take action unfettered by how someone else thinks I should run my life. That&#39;s how I want to live, why should I want to take that away from others?</p>

<p>Well, I don&#39;t. But the notion of freedom has always come with a caveat (several actually), that is - it is inherently restricted by ignorance. Is someone who is uninformed about the actual state of affairs truly free?</p>

<p>That&#39;s what those who speak out about alternative medicine are actually trying to achieve. We aren&#39;t attempting to &quot;defend our turf&quot; or &quot;squelch the competition&quot; we are attempting to inform the public about the true underpinnings of these therapies and point out they they are either unsupported by science or have in fact been disproved.</p>

<p>As has been noted before, a majority of New Zealanders are unaware that Homeopathic medicines do not contain any active ingredient and yet many people think they are scientifically proven.</p>

<p>Education was also the intent of the co-ordinated Sciblogs rebuttal to the poorly conceived and executed series on alternative therapies printed by the Herald earlier this year. (see <a href="http://sciblogs.co.nz/bioblog/2012/01/10/leeches-health-asking-some-questions/" target="_blank">here</a>, <a href="http://sciblogs.co.nz/infectious-thoughts/2012/01/11/nz-heralds-alternative-therapies-week-letter-to-the-editor/" target="_blank">here</a>, <a href="http://sciblogs.co.nz/molecular-matters/2012/01/12/ayurvedic-medicine-are-you-kidding/" target="_blank">here</a> and <a href="http://sciblogs.co.nz/skepticon/2012/01/13/thai-yoga-massage-herald-wherefore-art-thou-sense/" target="_blank">here</a>)</p>

<p>Policies that are aimed at restricting access to herbal or alternative medicine usually are doing so from the aspect of quality control. Does the remedy or practice have good evidence of efficacy, is it safe? These are the questions that we should be asking about every medical practice, not just those in the &quot;alternative&quot; (or complementary, or integrative, pick your marketing phrase of choice) camp.</p>

<p>Unfortunately is is not in the interests of those pushing alternative modalities to undergo strict evidence based testing so the issue is re-packaged from a quality control issue to a &quot;freedom&quot; issue.</p>

<p>Similar tactics are seen in arenas outside the medical realm. In biology the evolution vs creationism/intelligent design &quot;debate&quot; is framed as &quot;Academic Freedom&quot; as is the debate around climate change. This is not a coincidence. Whether or not these decisions are made consciously or not there has been convergence on the &quot;Freedom&quot; aspect of these cases for a reason, people respond to it. We are jealous of our freedom, and rightly so, freedom forms the basis of our society.</p>

<p>But as I alluded above, freedom is not an absolute and unalloyed good under all circumstances. It comes up against restrictions in all sorts of ways, some epistemological (as in the case of whether a choice is really free if the person is not aware of all the factors affecting that choice) and some are practical (as in should we allow freedom to include the freedom to sell harmful products?).</p>

<p><strong>Conclusion</strong></p>

<p>The natural/alternative remedies debate is not, at it&#39;s heart, about freedom at all. Rather it is about education and quality control. We should subject all medical practices to the same rigorous examination regarding safety and efficacy. Long term &quot;after market&quot; monitoring should also factor into this equation to catch those practices that looked good in the necessarily limited testing that they are subjected to prior to being rolled out to the general public but may still have safety problems.</p>

<p>In this way we should be able to serve the public&#39;s health interests and avoid false choices about freedom.</p>
', '2019-02-06', 34, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'God, UFOs, Life After Death: What do New Zealanders Believe?', '<p>Reading the paper today I learned that<a href="http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&amp;objectid=10771443" target="_blank"> 1/3 of New Zealanders believe</a> that we have been visited by extra terrestrials. I thought this was an interesting juxtaposition of stories given that a page or two later there was a report about a <a href="http://www.nzherald.co.nz/the-changing-world/news/article.cfm?c_id=1502962&amp;objectid=10771252" target="_blank">possibly habitable planet</a>. Maybe aliens are visiting us from Kepler-22b.</p>

<p>Keplerites aside, I decided to look up the report from <a href="http://www.umr.co.nz/" target="_blank">UMR Research</a> about the beliefs of my fellow citizens.</p>

<p><a href="http://www.umr.co.nz/Media/WhatDoNewZealandersBelieveDec11.pdf" target="_blank">The report</a> makes for interesting reading (if somewhat disconcerting in places) and I&#39;ll be looking for the follow-up reports around Maori culture and Herbal remedies. The first thing to note is that this was an on-line survey, so right off we should be wary about how representative these findings are of the general population. In that vein there was some attempt to make the results as representative as possible with quotas and weighting of responses. I couldn&#39;t find details of how this was carried out so with that in mind do take the results with a grain of salt.</p>

<p>One of the first things that jumped out at me was how uncertain people were regarding their answers. The questions seems to have 4 possible answers for both the affirmative and negative, from Absolutely Certain through Fairly Certain, Not Too Certain and Not At All Certain.</p>

<p>So while 61% believe &quot;That there is a God or some sort of universal spirit&quot; only 28% are absolutely certain of this. If we lump in the fairly certains then it goes to 41% (from now on I&#39;ll consider both groups to make up the &quot;Certain&quot; category). Compared to 38% who don&#39;t believe (27% of who are certain-ish). 38% non-believers in NZ. It&#39;s difficult to compare data sets but this appears to be up somewhat from ~34% (depending on how you count) religiously unaffiliated at the 2006 Census.</p>

<p>57% of us believe that there is life after death. 32% are certain. 31% are certain this isn&#39;t the case. 55% of us believe in psychic powers, 27% are certain; 27% are certain that they don&#39;t exist.</p>

<p>Now we get to the headline grabbing UFO question. 33% believe we have been or are being visited. How many are certain? 11%.</p>

<p>That&#39;s a bit of a relief.</p>

<p>Then there&#39;s Astrology. 24% think there is something to that malarkey. Only 6% are certain though. Whew...That&#39;s lucky. Still, those horoscopes are <em>everywhere</em>.</p>

<p>It seems that the hardcore believers tend to only make up a minority of the population, even for the mainstream beliefs. With the more mainstream the belief the more evenly spilt the believers and non-believers. i call that interesting. As well as somewhat heartening.</p>

<p>The report breaks down the results further into gender and ethnic responses but I&#39;m happy with looking at the top level stuff here. Check it out to see how women answered differently than men and how ethnicities are split between the different questions.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://openparachute.wordpress.com/2011/10/18/you-can-be-good-with-god/">You CAN be good with God!</a> (openparachute.wordpress.com)</li>
</ul>
', '2019-02-06', 98, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'Confounding Variables', '<p>Over at <a href="http://www.psychologyinaction.org" target="_blank">Psychology in Action</a> there&#39;s a decent post on <a href="http://www.psychologyinaction.org/2011/10/30/what-is-a-confounding-variable/?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=what-is-a-confounding-variable" target="_blank">confounding variables</a>.</p>

<p>The focus is on conducting and reading research and determining good research study practice but I think there is value in everyone knowing what confounding variables are.&nbsp; So what are they?</p>

<p>Well, read that post......</p>

<p>Ok, good.</p>

<p>Another example that I got in my stats class (many moons ago) was the correlation of matches with cancer. Those people who tend to carry boxes of matches in their pockets also have a higher risk of cancer.</p>

<p>As in the Murder vs Ice-cream example given at the link above, there is no direct link between matches and cancer (though it&#39;s obviously related), the most probable explanation is that those who carry matches are more likely to be those who smoke and it is the smoking that relates to cancer.</p>

<p>Smoking can then be said to be the confounding variable - the variable that explains both of the explicitly stated variables and either ties them together with a causal mechanism (Matches -&gt; Cancer) or shows that there is no direct relation (Ice-cream -/-&gt; Murder).</p>

<p>A similar effect may be seen with something like surveys, the manner in which a survey is carried out may introduce confounding variables (say a phone or internet survey which pre-selects participants by their access to said communication methods) or the questions asked may smuggle in assumptions that do not separate out confounding variables.</p>

<p>For example a survey may ask &quot;Are you Religious&quot; and &quot;Are you Happy&quot; (as many have). The Religious question smuggles in a number of extra factors that may contribute to a person&#39;s level of happiness eg religions usually come with a feeling of belonging to a community, social interaction, social support networks or guilt over actions and feelings. Each of which may more directly impact happiness that religion <em>per se</em>.</p>

<p>Other areas may also suffer from the confounding variable problem, alternative medicine springs to mind. Say you suffer from a cold, you soldier through it until you can&#39;t take it any more and start downing some homeopathic remedy. In a day or two your symptoms resolve and you feel better. Did the remedy work?</p>

<p>In this case the confounding variable could be the natural history of the disease. Colds don&#39;t last for ever (it is &quot;self limiting&quot;), it could be that you took the remedy right before the cold would have resolved itself anyhow. If this is so the conclusion that the remedy &quot;cured&quot; your cold would be invalid, there would not be a causal connection between the remedy and the cold symptoms going away.</p>

<p>The natural history of the disease would explain the reason you took the remedy when you did (symptoms had reached a climax) and why the remedy appeared to work (the cold would have resolved anyway).</p>

<p>When we examine issues closely we can see that confounding variables crop up, and should be carefully considered, every time we try to determine a causal connection between two events or phenomena. This is the reason that skeptics chant &quot;Correlation does not equal causation&quot; like a mantra.</p>

<p>Just for fun, suggest some instances of confounding variables in the comments. The more obscure the better.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://sciblogs.co.nz/skepticon/2011/04/24/%e2%80%9ci-was-a-skeptic-but%e2%80%a6%e2%80%9d/">I Was A Skeptic, but...</a> (sciblogs.co.nz)</li>
	<li><a href="http://sciblogs.co.nz/skepticon/2011/03/25/five-signs-you-might-be-wrong/">Five Signs You Might Be Wrong</a> (sciblogs.co.nz)</li>
	<li><a href="http://sciblogs.co.nz/skepticon/2011/02/21/amber-teething-beads-a-few-points-to-consider//">Amber Teething Beads a Few Points to Consider</a> (sciblogs.co.nz)</li>
</ul>
', '2019-02-06', 90, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'Are You Ready for the Faux-Zombie Apocalypse?', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>The title of this post is a somewhat obscure reference to an episode of the Nineties Sci-Fi show &quot;<a href="http://en.wikipedia.org/wiki/Sliders" target="_blank">Sliders</a>&quot;.&nbsp; The premise of the show was based on the <a href="http://en.wikipedia.org/wiki/Multiverse" target="_blank">Multiverse</a> theory in physics, a favourite trope in science fiction.</p>

<p>In &quot;Sliders&quot; a small group of individuals are accidentally set adrift in the multiverse, travelling from Earth to Earth in the hopes of getting home again. The show gave a unique opportunity to explore interesting historical counter factual situations, you know, of the &quot;What if Hitler won the war&quot; variety.</p>

<p>This was handled a little unevenly throughout the 5 season run of the show and some episodes were definitely more plausible than others. One of the less plausible ones sprang to mind when I read <a href="http://www.eurekalert.org/pub_releases/2011-06/tes-cbf060411.php" target="_blank">this press release</a> a couple of months ago*.</p>

<p>In this <a href="http://earthprime.com/episode-guide/sole-survivors.html" target="_blank">episode</a> the intrepid inter-dimensional explorers find themselves on a world where a fat-loss pill turns it&#39;s users into mindless fat craving monsters - coincidentally resembling movie zombies. Cool huh?</p>

<p>Okay, so that isn&#39;t likely to happen. But in a world where approximately a <a href="http://www.who.int/mediacentre/factsheets/fs311/en/index.html" target="_blank">fifth of the population is overweight</a> and a majority of the world populace has more to fear from over rather than under-eating the focus on obesity and methods to combat it is only going to grow. The solutions to obesity likely will need to be multi-pronged, each facet adding an incremental advantage. With this in mind medical solutions have their place alongside education, social and legislative approaches.</p>

<p>One of the medically based avenues open to us is to regulate the activity and/or amount of brown fat in our bodies.</p>

<p>Brief biology lesson: Humans (and other animals) have two main types of fat cell; brown and white. White fat is the regular old fat that we think of, know, and loath. It contributes to unsightly cellulite, increases our risk of various diseases that kill us and is generally something that you want to only have in moderation.</p>

<p><a href="http://en.wikipedia.org/wiki/Brown_adipose_tissue" target="_blank">Brown fat</a> on the other hand is a slightly different beast. Like white fat it is a repository of energy, but unlike white fat it is not simply a passive receptacle for these lipids1. Brown fat gets it&#39;s colour (and hence name) from the high density of mitochondria in the cells. The presence of such high numbers of mitochondria allows the cells to channel the force, oh wait that&#39;s a different organelle, sorry. They mean that the cells can burn energy and contribute to thermal regulation, what&#39;s known as Non-shivering thermogenesis.</p>

<p>This type of heat regulation is most important to infants and it was thought that as we grew and matured the brown fat disappeared. This is now known not to be the case, further, manipulation of brown fat in adults may give us a means to burn extra calories and hence reduce our white fat.</p>

<p><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2880836/" target="_blank">Another paper</a> released last year attempts to give a detailed background of the possibility of manipulating brown fat for the purpose of weight regulation. It also notes the potential benefits and drawbacks of this approach and is a very interesting read. Essentially though the idea with utilising brown fat is to increase the body&#39;s basal metabolic rate (the amount of energy you consume while at rest), by doing this we can increase the total number of calories used by the body and effectively burn up the excess calories consumed.</p>

<p>The press released that sparked this post is related to a <a href="http://edrv.endojournals.org/cgi/content/meeting_abstract/32/03_MeetingAbstracts/OR11-6?sid=2f6236ec-8f0f-4aa7-87dc-e35d94229c70" target="_blank">paper presented</a> at The Endocrine Society&#39;s 93rd Annual Meeting in Boston. It details an investigation into the location of brown fat in the adult body and the mechanisms involved in the creation of brown fat from undifferentiated progenitor fibroblast cells. The fat tends to be deep within the adipose tissue of the neck and chest as well as mixed in with white fat, hence the thinking that we lost it as we age - it&#39;s not easy to find. This incremental step in understanding gives us another tool that may be used to increase our proportion of brown fat cells and thus increase our metabolic capability.</p>

<p><a href="http://www.ncbi.nlm.nih.gov/pubmed/19357406" target="_blank">Previous work</a> by the same lead author has correlated amount of brown fat with BMI, finding an inverse relationship. The implication here is that a larger amount of brown fat does contribute to a higher metabolic rate and there for allows some individuals to avoid long term accumulation of fat, confirming that this is a potentially viable approach to weightloss. The numbers of this study were pretty low though and I&#39;m not sure how clinically relevant this finding is.</p>

<p>The authors caution however that benefits may be modest, the lead author is quoted:</p>

<blockquote>
<p>&quot;As powerful as brown fat could be at burning calories, we can easily out-eat the benefit.&quot;</p>
</blockquote>

<p>So don&#39;t be looking at this as a panacea for obesity, as noted above we will likely have to tackle this problem from multiple sides. Those who view our increasing dependence on drugs and medical interventions with suspicion may not be happy with treating obesity in this fashion2, but it is such a growing problem we should use every tactic at our disposal to reduce the risks associated with this threat to our health.</p>

<p>So, bring on the human experimentation and lets hope the reducing of people to vacant fat-starved cannibals is kept to a minimum.</p>

<p>---------------------------------------------------------------------</p>

<p>Cypess, A., Lehman, S., Williams, G., Tal, I., Rodman, D., Goldfine, A., Kuo, F., Palmer, E., Tseng, Y., Doria, A., Kolodny, G., &amp; Kahn, C. (2009). Identification and Importance of Brown Adipose Tissue in Adult Humans New England Journal of Medicine, 360 (15), 1509-1517 DOI: <a href="http://dx.doi.org/10.1056/NEJMoa0810780">10.1056/NEJMoa0810780</a></p>

<p>Tseng, Y., Cypess, A., &amp; Kahn, C. (2010). Cellular bioenergetics as a target for obesity therapy Nature Reviews Drug Discovery, 9 (6), 465-482 DOI: <a href="http://dx.doi.org/10.1038/nrd3138">10.1038/nrd3138</a></p>

<p>---------------------------------------------------------------------</p>

<p>* Yes, I really am that lazy.</p>

<p>1. Yes again, I know that&#39;s a gross over simplification. <a href="http://en.wikipedia.org/wiki/Adipocyte#White_fat_cells_.28unilocular_cells.29" target="_blank">White fat does stuff too</a>.</p>

<p>2. I have no evidence of this but I&#39;m sure they&#39;re out there.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://www.medicalnewstoday.com/articles/232712.php">Brown Fat, Also Known As Good Fat, More Common In Leaner Children</a> (medicalnewstoday.com)</li>
	<li><a href="http://www.eurekalert.org/pub_releases/2011-08/jdc-fm081111.php">&#39;Good fat&#39; most prevalent in thin children</a> (eurekalert.org)</li>
	<li><a href="http://www.eurekalert.org/pub_releases/2011-06/tes-cbf060411.php">Calorie-burning brown fat is a potential obesity treatment, researchers say</a> (eurekalert.org)</li>
</ul>
', '2019-02-06', 33, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Science Coverage in the Media - A Report on the BBC', '<p>I just read <a href="http://www.guardian.co.uk/science/2011/jul/20/bbc-climate-change-science-coverage" target="_blank">this story</a> about an independent review of science reporting from the BBC (via <a href="http://richarddawkins.net/articles/642250-bbc-gives-too-much-weight-to-fringe-views-on-issues-such-as-climate-change" target="_blank">RD.net</a>). It covers the release of a <a href="http://www.bbc.co.uk/bbctrust/our_work/other/science_impartiality.shtml" target="_blank">report</a> on the coverage of science by the BBC, while the report is mostly favourable there are a few things that could be addressed and done better.</p>

<p>The obvious one is the general media habit of false balance. This is the practice of inserting contrary views for the sake of it and in doing so providing a false sense of the actual sate of affairs. Examples given in the report are AGW, the MMR vaccine/Autism brouhaha and GM crops. For other markets I&#39;d throw in the creationism/evolution &quot;debate&quot;, clashes between so-called Complementary and Alternative Medicine (CAM, or now &quot;Intergrative Medicine&quot;) and other issues where the science strongly favours one view point.</p>

<p>This is a significant problem in the media where the pressure must be great to both appear &quot;impartial&quot; and to take advantage of &quot;<a href="http://www.urbandictionary.com/define.php?term=Manufactroversy" target="_blank">manufactroversies</a>&quot; to drive consumers up-take of content. Care must be taken when presenting stories to give due weight to each view point in order to convey the correct interpretation to the target audience if stories are to be presented correctly. Too often fringe views are given disproportionate air time in order to the provide &quot;balance&quot;, but this has the effect of giving these views more credence in the public consciousness than they deserve.</p>

<p>A great parody/analogy used in the report by the reviewer was &quot;<em>mathematician discovers that 2 + 2 = 4; spokesperson for Duodecimal Liberation Front insists that 2 + 2 = 5, presenter sums up that &ldquo;2 + 2 = something like 4.5 but the debate goes on</em>&rdquo;.</p>

<p>That&#39;s it in a nutshell.</p>

<p>A great video of comedian Dara O&#39;Briain covers the same point. (I know it&#39;s been posted before but it&#39;s hilarious)</p>

<div class="embeddedContent oembed-provider-">&nbsp;</div>

<p>[youtube=http://www.youtube.com/watch?v=YMvMb90hem8]</p>

<p>I have not yet had a chance to look at the entire report (it&#39;s over 100 pages) but hopefully I can wade through it at some point ad pull out a few more interesting points.</p>
', '2019-02-06', 65, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Wonderwoman', 'A Better World Through Video Games', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>Over the last few years evidence has been mounting that violence in the media and especially interactive media such as video games contributes to aggression displayed by individuals.[1] This ability to influence our behaviour in such a way is concerning and may undermine attempts to build a peaceful society that nevertheless respects a person&#39;s right of autonomy and the ability to choose the entertainment in which we wish to partake[2].</p>

<p>If we accept that the entertainment we consume may have negative effects on our behaviour, (and much as I hate to admit it the evidence is pretty convincing) at what point do we decide that it is our responsibility to curtail these forms of entertainment for the greater good?[3] I&#39;ll leave you with that to ponder &#39;cos that&#39;s not actually what I&#39;d like to address, this is just the lead-in.</p>

<p>The research suggests that, like yawns, aggression is contagious. Bad stuff. But, what about positive feelings and outcomes? Can we propagate happiness and kindness in the same way? A recent paper suggests: yes.</p>

<p>&#39;<em>&quot;Remain Calm. Be Kind.&quot; Effects of Relaxing Video Games on Aggressive and Prosocial Behavior</em>&#39; is the title of a paper recently published in the journal <em>Social Psychological and Personality Science</em>. In it authors&nbsp; Jodi Whitaker and Brad Bushman look at the effects playing different sorts of video games has on post game behaviour. The full paper remains locked behind a pay-wall but a decent overview is provided <a href="http://www.eurekalert.org/pub_releases/2011-06/osu-kgv060611.php" target="_blank">here</a>. Essentially the researchers randomly assigned participants to either relaxing, neutral or violent games. The participants then had their levels of aggression or <a href="http://scepticon.wordpress.com/2011/05/25/holy-hyperbole-batman/" target="_blank">prosocial</a> tendencies measured in one of two ways.</p>

<p>In the first experiment the subjects participated in a mock competition (not mock to them, they thought it was real) in which they had to push a button faster than their &quot;competitor&quot; If the competitor won the study participant chose an amount of money to give them. If the competitor lost then the subjects got to blast them with noise, and could choose how loud and long the blast was.</p>

<p>Predictably, those who had played the violent games hit their opponents with longer and louder sound blasts than those who played the neutral games, who in turn were more aggressive than the relaxed gamers. Conversely the most money was given to winners by the participants who played relaxing games.</p>

<p>The second experiment was a little more subtle. Post gaming the participants were given a questionnaire measuring their mood, once this was completed the participants were told the experiment was over. The researcher then asked for help sharpening pencils for another study, how many pencils the participants choose to sharpen was used as a measure of prosocial behaviour.</p>

<p>As you will have inferred, the (now) hippy-dippy gamers opted to spend more of their time sharpening pencils than their amped-up counterparts. Thus the world is made just a little bit better through the use of video games. Or at least there are slightly more sharp pencils than there would otherwise&nbsp;have been, that&#39;s progress, right?</p>

<p>I&#39;d like to know the full suite of games that were used in the studies but we do have one example from each category: &quot;<a href="http://en.wikipedia.org/wiki/Endless_Ocean" target="_blank">Endless Ocean</a>&quot; is one of the relaxing games, &quot;<a href="http://en.wikipedia.org/wiki/Super_mario_galaxy" target="_blank">Super Mario galaxy</a>&quot; is a neutral game and &quot;<a href="http://en.wikipedia.org/wiki/Resident_Evil_4" target="_blank">Resident Evil 4</a>&quot; is, of course, the violent game. Now I know what you&#39;re thinking, &quot;that stupid scuba game couldn&#39;t possibly be as much fun as blasting zombies!&quot; well, apparently, it is. An independent group of students rated the entertainment and enjoyment value of each of the games and the researchers were careful to match the game ratings.</p>

<p>I doubt that relaxing and calming games are likely to supplant the violent kind in the gaming ecosystem any time soon but it is nice to see that people can be influenced in good ways as well as bad by media.</p>

<p>To plagiarise and butcher a quote from Homer Simpson:</p>

<blockquote>
<p>&quot;To video games! The cause of, and solution to, all of life&#39;s problems.&quot;</p>
</blockquote>

<p>-------------------------------------------------------------------------------<br />
Whitaker, J., &amp; Bushman, B. (2011). &quot;Remain Calm. Be Kind.&quot; Effects of Relaxing Video Games on Aggressive and Prosocial Behavior Social Psychological and Personality Science DOI: <a href="http://dx.doi.org/10.1177/1948550611409760">10.1177/1948550611409760</a></p>

<p>Footnotes:</p>

<p>1.Here&#39;s a list of publications supporting a link between games and aggressive behaviour spanning couple of decades. Obviously more of a taster than a full list:</p>

<p>Bushman, B., &amp; Gibson, B. (2010). Violent Video Games Cause an Increase in Aggression Long After the Game Has Been Turned Off Social Psychological and Personality Science, 2 (1), 29-32 DOI: <a href="http://dx.doi.org/10.1177/1948550610379506">10.1177/1948550610379506</a><br />
<a href="http://scepticon.wordpress.com/2011/05/25/holy-hyperbole-batman/" target="_blank">http://spp.sagepub.com/content/2/1/29.full.pdf+html</a></p>

<p>Gentile, D., Lynch, P., Linder, J., &amp; Walsh, D. (2004). The effects of violent video game habits on adolescent hostility, aggressive behaviors, and school performance Journal of Adolescence, 27 (1), 5-22 DOI: <a href="http://dx.doi.org/10.1016/j.adolescence.2003.10.002">10.1016/j.adolescence.2003.10.002</a><br />
<a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.120.746&amp;rep=rep1&amp;type=pdf" target="_blank">http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.120.746&amp;rep=rep1&amp;type=pdf</a></p>

<p>Silvern, S. (1987). The effects of video game play on young children&#39;s aggression, fantasy, and prosocial behavior Journal of Applied Developmental Psychology, 8 (4), 453-462 DOI: <a href="http://dx.doi.org/10.1016/0193-3973(87)90033-5">10.1016/0193-3973(87)90033-5</a><br />
<a href="http://www.sciencedirect.com/science/article/pii/0193397387900335" target="_blank">http://www.sciencedirect.com/science/article/pii/0193397387900335</a></p>

<p>Anderson, C., Shibuya, A., Ihori, N., Swing, E., Bushman, B., Sakamoto, A., Rothstein, H., &amp; Saleem, M. (2010). Violent video game effects on aggression, empathy, and prosocial behavior in Eastern and Western countries: A meta-analytic review. Psychological Bulletin, 136 (2), 151-173 DOI: <a href="http://dx.doi.org/10.1037/a0018251">10.1037/a0018251</a><br />
<a href="http://psycnet.apa.org/psycinfo/2010-03383-001" target="_blank">http://psycnet.apa.org/psycinfo/2010-03383-001</a></p>

<p>2. With the normal caveats of not harming anyone else or restricting their freedoms, yada yada. My point is how far up the chain do we determine that an action harms another?</p>

<p>3. As we do with other forms of potentially harmful behaviour eg speeding limits, driving blood alcohol limits etc.</p>
', '2019-02-06', 78, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'Holy Hyperbole Batman!! ', '<p>Apparently the Armageddon predicted by <a href="http://scepticon.wordpress.com/2011/05/17/its-the-end-of-the-world-as-we-know-it-and-i-feel-fine/" target="_blank">Harold Camping</a> is not the only one we have to contend with. Watch out, it&#39;s The <a href="http://www.nextarmageddon.net/" target="_blank">Next Armageddon</a>!</p>

<p>Did you know that the WHO is not the health promoting organization we think it is but really the most nefarious institution in human history?[1]</p>

<p>According to one conspiracy nut[2] the WHO has put into action a plan to kill over 3 billion[3] people. Huh, WHO&#39;da thunk it?</p>

<p>WHO is really going to take this seriously?[4]</p>

<p>The target of this hysteria is the <a href="http://www.codexalimentarius.net/web/index_en.jsp" target="_blank">Codex Alimentarius</a>, a set of regulatory guidelines put out by the WHO to:</p>

<blockquote>
<p>&quot;..develop food standards, guidelines and related texts such as codes of practice under the Joint FAO/WHO Food Standards Programme. The main purposes of this Programme are protecting health of the consumers and ensuring fair trade practices in the food trade, and promoting coordination of all food standards work undertaken by international governmental and non-governmental organizations.&quot;</p>
</blockquote>

<p>At least, that&#39;s what &quot;They&quot; want you to think. Mwahahaha!</p>

<p><strong>Russians and Nazis and conspiracies, Oh My!</strong></p>

<p>The first thing you&#39;ll notice about this brightly coloured screed[5] is the complete absence of references. A large number of extremely serious allegations are made and not a single effort has been made to allow you to verify these for yourself.</p>

<p>The first real claim made (apart from the whole killing three billion people thing) is that a Nazi war criminal teamed up with the United Nations to control the population of the world through the food supply. Not a sniff of backing for this is included in the text. Searching on the name of the Nazi[6] and United Nations turns up only other conspiracy sites using virtually identical text. I&#39;m convinced.</p>

<p>This page also taps into the paranoia around water fluoridation by asserting that fluoride is both a poison and has the effect of eliminating aggression and ambition. the proof?</p>

<blockquote>
<p>&quot;.... and the fact that it is used in many drugs prescription[sic] shows that it eliminates aggression and ambition in people.&quot;</p>
</blockquote>

<p>What more do you need sheeple??? Obviously it must be true, its all in black and white (except the bits in red). Plus, you know, the Russians used it in experiments and got the same results. Uh, where did you get that tidbit? Show me that paper, and the others where it was replicated. Oh, it&#39;s part of the conspiracy you say. How convenient.</p>

<p>Regarding poisons, I&#39;ve said it before: Dose Matters. Things that in high doses would kill us are routinely used in medicine. There is a range where the benefit&#39;s of a substance outweigh the risks. To deny this is to fundamentally misunderstand medicinal and toxicological science.</p>

<p>Of course, this is all the work of evil entities that have been planing population control since the early 1960&#39;s. I have to say that this is some impressively long term planning. I&#39;m surprised there&#39;s no mention of the &quot;<a href="http://en.wikipedia.org/wiki/Illuminati" target="_blank">Illuminati</a>&quot; if anyone is good at long term, surely it&#39;s them.</p>

<p>Conspiracies, conspiracies everywhere....</p>

<blockquote>
<h2>&quot;Why Are You Not Aware Of This?</h2>

<p>Because the strategy was so thought-out that it&#39;s almost impossible to realise our food is being used against us.&nbsp; But when you dig deeper you will see that everything is set up to kill us slowly over time... without one factor being the main cause, because there are many. Basically, for those who die... it will be made out as their own fault&hellip;&quot;</p>
</blockquote>

<p>Yep, almost impossible. Almost. Only those who have the ability to scratch the surface, pull back the curtain and pierce the fog can work it out. Gosh they must have keen insight. Or perhaps they are engaging in overactive pattern recognition and faulty reasoning, not to mention MSU[7] syndrome. It could go either way.</p>

<p>But wait, there&#39;s more. Not only is this guy warning us out of the goodness of his heart, he&#39;s also willing to sell us a book that tells you how not to be killed by the evil powers that be. How nice.</p>

<p>The price of USD$37 is just symbolic, you know nominal, don&#39;t worry about it at all.</p>

<p>My favourite of the benefits touted as to why you should buy the book is:</p>

<blockquote>
<p>&quot;The naked truth behind UMAMI (the taste scam behind 90% of the foods today...which is so toxic and makes the foods taste so good and irresistible). What you don&#39;t know is that UMAMI has a terrible effect over your health. Here&#39;s how to avoid it... &quot;</p>
</blockquote>

<p>I&#39;ll give him the benefit of the doubt and presume that this is an example of poor editing. Umami isn&#39;t a chemical, it&#39;s the subjective experience of taste that we interpret as savoury/meaty taste. Perhaps what is being referred to here is glutamate or MSG, which for a short time was considered to have <a href="http://en.wikipedia.org/wiki/Monosodium_glutamate#Health_concerns" target="_blank">negative health effects</a>.[8] Subsequent study has failed to bear this out. So even that generous reading of this point is, well, wrong. Sorry.</p>

<p>If it seems like I haven&#39;t really taken any of this seriously, it&#39;s because I don&#39;t. There are some claims where the only reasonable response is ridicule. There are dozens of assertions put forward on the web page in question. Many of which don&#39;t even give enough information to know exactly what is being claimed[9], let alone providing any basis for refutation or confirmation.</p>

<p>Without providing any details the page is &quot;not even wrong&quot; it&#39;s incomprehensible. The only value it has is to whip up unfounded fear and paranoia, all to pave the way towards buying the book that will save you. From another perspective it has one other value, entertainment.</p>

<p>HT to <a href="http://sciblogs.co.nz/bioblog" target="_blank">Alison</a> for bringing this to my attention. Thanks for the hilarity.</p>

<p>--------------------------------------------------------------------------</p>

<p>Footnotes:</p>

<p>1. If you&#39;re an anti-vaxxer, don&#39;t answer that. Also, don&#39;t quote-mine me. Satire has it&#39;s weaknesses.</p>

<p>2. He says he&#39;s not but methinks he doth protest too much.</p>

<p>3. For maximum effect try to read this in the tone of Dr. Evil.</p>

<p>4. Last one, honest.</p>

<p>5. Though not nearly as bad as most conspiracy sites, so there&#39;s that.</p>

<p>6. Hermann Schmitz, president of I.G. Farben the major producer of poison gas for the Germans. <a href="http://www.ess.uwe.ac.uk/genocide/cntrl10_trials.htm#Farben" target="_blank">http://www.ess.uwe.ac.uk/genocide/cntrl10_trials.htm#Farben</a></p>

<p>7. Making Shit Up.</p>

<p>8. Ooooh, the dreaded Wikipedia. Inside joke, don&#39;t worry about it.</p>

<p>9. &quot;<em>All nutrients (vitamins and minerals) that have any positive health impact on the body are to be considered toxins/poisons and are to be removed from all food because Codex prohibits the use of nutrients to &quot;prevent, treat or cure any condition or disease&quot;.</em> &quot;&nbsp;&nbsp; -What does that even mean? Are they going to suck all of the beta-carotene out of carrots? How would you even go about implementing such a retarded scheme?</p>
', '2019-02-06', 98, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Five Signs You Might Be Wrong', '<p>Over the last few weeks several things have been happening both in my own life and in the wider public (Ken Ring) that have made me think about good rules of thumb to determine whether a claim is likely to be right or wrong. In particular when is it reasonable to perform a self examination and ask the question &quot;Could I be wrong?&quot;.</p>

<p>We have to make decisions with limited information every day, it&#39;s not a good idea to be frozen with existential doubt over every little factoid wondering if the entire basis of knowledge is up to the task of determining Truth. I suspect society would grind to a halt if everyone suddenly started doing this.</p>

<p>But, it is a good idea to clear the cobwebs every once in a while and put ourselves back on a firm footing. What sort of things should we be on the lookout for in our mental spring cleaning? I don&#39;t think I have the definitive answer but here are a few ideas that have presented themselves to me lately as a decent place to start:</p>

<p>5. Everyone knows that or &quot;Common Wisdom&quot;.</p>

<p>These are thing that you just absorb from the culture, you don&#39;t know where you heard them but it&#39;s so ingrained it just seems like common sense. Things like reading in low light being bad for your eyesight or only using ten percent of our brains. These permeate our popular consciousness like the air we breath.</p>

<p>Common myths get perpetuated in this way, you haven&#39;t looked into it but you just <em>know</em>, that&#39;s how the world works. This is a form of argument from popularity, reasonable most of the time but not a method of generating knowledge that is optimised for accuracy. On one level this is fairly harmless, usually these things don&#39;t impact the major decisions in life and you aren&#39;t especially invested in a particular conclusion. If you don&#39;t really lose most of your body heat through your head, well it doesn&#39;t matter, you were going to wear a hat anyway - it looks nice.</p>

<p>On the other hand this can be the most insidious method of creating misconceptions. Much of the time you aren&#39;t even aware of them, if you were raised in Japan then you might just &quot;know&quot; that women are subservient to men. If that seems too drastic, perhaps you just have a general feeling that boys are better at maths and science than girls. These are things that we implicitly learn from our culture and can be difficult to dispel even if we are aware of the actual facts.</p>

<p>4. You learned about it from a Chain email</p>

<p>You&#39;ve seen them. Emails that have been forwarded from one person to the next, each one thinking that someone earlier in the chain has probably checked it out and besides &quot;what if&quot; it&#39;s true. Better send it on just in case.</p>

<p>I&#39;ve seen several of these lately, see my report on <a href="http://scepticon.wordpress.com/2011/03/07/lemon-the-new-miracle-cure/" target="_blank">Lemons and Cancer</a>. Another has been making the rounds post earthquake ostensibly describing how to survive a a serious earthquake. This also has misleading and possibly dangerous information if the advice is followed. Basically I view everything transmitted in this manner as suspect until proven otherwise, the 21st century version of word of mouth seems to by-pass both verification and common sense because it is so easy to press the forward button and we appear to still afford the written word a default respect that it may not deserve (though you should respect my written words, &#39;cos I say so).</p>

<p>What I find most frustrating about this phenomenon is that de-bunking these things is almost as easy as hitting that forward button. It usually takes me about 20 seconds - depending on how my internet connection is faring that day - to copy and paste some of the text into google or check on a site like <a href="http://www.snopes.com/" target="_blank">Snopes.com</a>.</p>

<p>3. You&#39;re on the edge or just beyond what we currently know scientifically.</p>

<p>Here is where things start taking a turn toward the dark side. Prior to this stage we could just look up the correct information and set ourselves straight. At this point though we need to start applying actual critical thinking and assess new information on it&#39;s plausibility and merits. Sometimes this is just extrapolating from a recent breakthrough (invisibility cloaks anyone?), this tends to be easily recognised and dismissed.</p>

<p><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1182327/" target="_blank">We know</a> that when working on the frontiers of knowledge many of our conclusions are false, or wildly simplistic. Forming opinions on the back of these initial forays into the unknown is therefore fraught with peril for the unwary, or even the wary for that matter. This is where some quacks can move in, taking preliminarily positive results for some treatment or technology based on initial tests and making claims that are not backed by sound data. We could put &quot;black market&quot; stem cell therapies or cancer cures into this category.</p>

<p>Alternatively there are some who take plausible trends in scientific and technological progress and predict specific technologies will be developed in specific time frames (looking in your direction <a href="http://en.wikipedia.org/wiki/Ray_Kurzweil#Predictions" target="_blank">Kurzweil</a>). Others may claim that because our knowledge is underdeveloped, either in general or in a particular discipline, that their pet theory should be given a pass.</p>

<p>These ideas may or may not be correct but we can make reasonable determinations as to likelihood based on current scientific knowledge, science can bring up counter-intuitive facts about our universe but in general we tend to see incremental advancement on existing knowledge. Self replicating machines? perhaps, perpetual motion machines? no, not really.</p>

<p>In this way this category can tend to bleed into my next warning sign...</p>

<p>2. Your point of view goes against/disagrees with a large proportion of scientists/medical professionals.</p>

<p>This is the point where you are starting to cross over to crankery. This is where warning sirens should start going off in your head when you are confronted with &quot;alternative&quot; theories. Sometimes though it is not obvious straight away, only once we have accepted and become invested in a theory do we get confronted with disconfirming evidence. We should not be afraid to let go of ideas when they are shown to be incorrect.</p>

<p>Examples of this might be that vaccines cause autism, or that fluoridation is harmful. The key here is that it is not prima facie impossible that these things are true, but the preponderance of data has shown that they are very unlikely to be true. At this point it is incumbent upon a reasonable person to change their mind.</p>

<p>Once you decide that you are either better informed than the scientific or medical community or that there exists a conspiracy to keep these things from the public you start to cross the line between concerned citizen to outright crank. It&#39;s possible that you <em>really are</em> a genius, that the scientists <em>really are</em> wrong, that you <em>really are</em> right, that you <em>really can</em> compare yourself favourably to Galileo, but <em>really</em> - it&#39;s not likely. Luckily, you can come back from the edge - it takes courage though. One thing to keep in mind is that you should be committed to the enquiry, not the conclusion. This way you should be able to&nbsp; follow where the evidence leads and change you opinion accordingly.</p>

<p>1. Large chunks of science have to be wrong for you to be right</p>

<p>Congratulations, you have graduated to fully fledged crank. You are immune to evidence and reason and live in your own self-contained universe of nonsense, insulated from reality by your enormous&nbsp; self-righteousness ego. Examples of this extreme form of scientific inaccuracy are Creationism, <a href="http://en.wikipedia.org/wiki/Neal_Adams">Neal Adams&#39;</a> &quot;Growing Earth&quot; theory, and Homeopathy. If you have made it this far then chances are you are so committed to your ideas that no amount reasoned argumentation will sway you, you may have a tinfoil hat somewhere on your person right now.</p>

<p>But, and let me make this clear, you are NOT an idiot*. You have just invested so much in a particular point of view that changing you mind now would tantamount to repudiating a large portion or your life. That&#39;s painful and not a course of action anyone wants to undertake.</p>

<p>At this point it becomes farcical to even suggest that you might be right and the accumulated knowledge of the last 200-400 years is wrong. Recall how I said earlier that we make incremental advances based on existing knowledge? Well all of our past discoveries have inexorably been leading to the fact that..you can&#39;;t infinitely dilute a substance and still expect an effect...the earth really is 4, 500,000,000 years old...energy really can&#39;t be created from nothing... and on and on.</p>

<p>All that said, again,&nbsp; it still is possible for you to change your mind and come back to reality. A number of people have done it, but the chances do become lower. Good luck.</p>

<p>-------------------------------------------</p>

<p>The above should be taken as a light-hearted look at our foibles, no real offence is intended. - He said, trying futility to ward off trolls.</p>

<p>* Ok, I can&#39;t back that up. Some of you are bound to be idiots... just sayin&#39;...statistically... you know.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2011/03/03/moonquake-what-does-the-science-say/">MOONQUAKE: What Does the Science Say?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scienceblogs.com/tfk/2011/02/a_scientific_worldview_and_the.php">A &quot;scientific worldview&quot; and the definition of &quot;science&quot; [Thoughts from Kansas]</a> (scienceblogs.com)</li>
	<li><a href="http://openparachute.wordpress.com/2011/03/02/making-sense-of-ring-gate/">Making sense of Ring gate?</a> (openparachute.wordpress.com)</li>
	<li><a href="http://openparachute.wordpress.com/2011/03/07/acceptance-of-science-dangerous-for-some/">Acceptance of science - dangerous for some</a> (openparachute.wordpress.com)</li>
</ul>
', '2019-02-06', 543, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Robin', 'Amber Teething Beads: A Few Points to Consider', '<p>Being a new parent and a sceptic I have been on guard regarding dubious advice and practices. Parents, especially new parents like myself, are a vulnerable group. We tend to be full of anxiety that we are doing the &ldquo;right thing&rdquo; by our children. Where-ever you find a vulnerable group like this you also tend to find those who prey on such fears. I have actually been pleasantly surprised, despite my vigilance I have not yet been subjected to any dubious advice (that I&#39;ve noticed). But early last week I was confronted by a practice from a fellow new parent that I found a little disturbing. I&#39;m taking about using necklaces of amber beads to reduce the pain of teething for babies.</p>

<p>Teething can be an especially stressful time for parents and children, the child may be experiencing pain as the new teeth break through the gums. This means an irritable child and frazzled parents. Anything that promises to relieve or prevent this harrowing time is gratefully embraced.</p>

<p>On to the amber beads. This practice disturbs me for several reasons. First is safety, the necklace if left on the baby for long periods may pose a strangling hazard of it becomes caught on something. Most advertise that they are made to break easily to prevent this and that the beads are individually knotted onto the necklace to prevent scattering on breakage. However this still seems to leave a broken string of beads in reach of a baby, as as most people know - anything a baby can get it&#39;s hands on goes straight into the mouth. So choking is also a concern[1&amp;17].</p>

<p>Now, I&#39;m not one to be a worry wart over every little potential hazard, used correctly under parental supervision I suspect that the likelihood of a tragedy of this kind is low. But not zero[15&amp;16]. This coupled with the low possibility that the necklace actually does anything is what worries me. The second disturbing thing is that parents are accepting this via word of mouth and apparently not consulting their doctors before subjecting their child to an intervention of unknown safety and efficacy.</p>

<p>I have three main points I want to cover with regard to these amber beads that parents should consider before trying these beads (in addition to the physical safety above). The first relates to basic plausibility.</p>

<p>Before we get to that though it depends on which mechanism of action for the beads you subscribe to. There are several explanations regarding how the beads are supposed to work floating around the intertubes, many are of the tinfoil hat brigade variety, these will be ignored (but look <a href="http://www.holisticbaby.co.nz/Advice/BABYS+POSITIVE+ADVICE+SOLUTIONS+AND+RESOURCES./Teething/Natural+ideas+options+and+diet+recommendations..html" target="_blank">here</a> and <a href="http://www.articlesbase.com/health-articles/teething-necklace-advices-2808778.html" target="_blank">here</a> for a bit of a chuckle). Only one explanation I have found makes biological sense so that&#39;s the one I&#39;ll be focusing on.</p>

<p>That explanation is Succinic acid, baltic amber is known to contain between 3-8% succinic acid. According to proponents this is released from the beads and into your baby. The succinic acid then allegedly has an analgesic effect and so reduces the pain of teething. Here is where my first point regarding plausibility comes in:</p>

<p>Amber is tough, really tough. This is a material that has persisted for thousands and in some cases millions of years unchanged. Suffering through heating and cooling of innumerable climatic changes through the years. Yet this same tough unchanging material with happily give up it&#39;s chemical components upon the gentle heating it receives on being placed next to your baby&#39;s skin? Colour me unconvinced[1&amp;2]. Related to this point amber has a hardness on the Mohs scale of between 1 and 3 [3], baltic amber which is usually touted as the therapeutic variety (because of the high succinic acid content) is at the high end of this scale 2 &ndash; 2.5. To put this in perspective, Tin has a hardness of about 1.5 and Gold is 2.5-3 [4]. But forget about this point, I don&#39;t need it. Lets say for argument sake that clinically relevant amounts of succinic acid are released by the amber and absorbed by your baby&#39;s skin.</p>

<p>My second point then, relates directly to the claims made for succinic acid. Succinic acid is made in the body (and in plants) as part of the citric acid cycle (aka krebs cylce)[5]. It is also use in the food and beverage industry as a food acid (additive #363 to be precise)[6]. Interestingly in this capacity there are recommendations from some quarters to avoid the substance[7]. Even so, apart from it&#39;s early use as a topical treatment for rheumatic pain[8] there is no evidence that I could find (searching <a href="http://en.wikipedia.org/wiki/PubMed">Pubmed</a> at least, where I would expect a decent study to be referenced) that it is effective as either an anti-inflammatory or general analgesic. Let me be clear on that, I don&#39;t mean low quality evidence, I don&#39;t mean small poorly designed trials with equivocal effects, I mean nothing. Zip. Nada. In fact if anyone knows of any let me know because I find this complete lack quite surprising, I&#39;m open to the idea that I was looking in the wrong place or was using incorrect search terms. So, unless there is late breaking news, it fails on that count as well. Meh, what do we care about evidence of efficacy anyway? Throw this point out too. Lets move on to my final argument, uh, I mean point to consider.</p>

<p>Lets say that a. the beads do indeed release succinic acid into your baby and b. this succinic acid has an analgesic effect once it enters your baby&#39;s body. Doesn&#39;t the very fact that an unknown amount of a drug[9] is being put into your baby&#39;s body bother you? What is that I hear? It&#39;s natural? Oh, well, that&#39;s ok then. No wait, no it&#39;s not. I don&#39;t care what the origin of a compound is, the question is what are it&#39;s effects on the body and do the benefits out weigh the risks. Ok, lets replace succinic acid with some other naturally occurring substance, salicylic acid. This is a compound with known anti-inflammatory properties[10]. Would you be happy with a product that introduced unknown levels of this compound into your baby? What if I said that overdoses with this compound could lead to a 1% chance of death?[11] It&#39;s natural, it&#39;s also the precursor to acetylsalicylic acid, otherwise known as Aspirin[12].</p>

<p>Now, lest I be accused of unnecessary fear mongering and drawing false comparisons I would like to admit that at present there is no evidence to suggest that succinic acid is hazardous, nor even that it is potentially hazardous[5]. This does not detract from my main point however, the point isn&#39;t whether this particular compound is safe or not but that the reasoning[13] around it&#39;s use is faulty and cannot be used as a substitute for evidence.</p>

<p>Based on the complete lack of plausibility on any level of efficacy any potential for harm, however small, must tip the balance of this equation away from the use of this product. Don&#39;t trust me though, talk to your doctor, I suspect though that given the complete lack of reliable information on this topic they will be left to rely on their own philosophy of harm vs benefit. In the final analysis, there are not always clear answers[14], but developing good critical thinking skills will at least provide you with a small light in the darkness.</p>

<p>[Edit - I recently posted a follow-up article to this addressing some of the points raised in the comments below. It may be found <a href="http://scepticon.wordpress.com/2012/04/02/amber-teething-beads-a-follow-up/" target="_blank">Here</a>]</p>

<p>[Update 20/07/12:&nbsp; Commenter Heidi Pogner-Schultz has provided a thoughtful and researched perspective in support of amber beads (<a href="http://scepticon.wordpress.com/2011/02/21/amber-teething-beads-a-few-points-to-consider/#comment-5596">here</a>), I disagree for reasons outlined in my reply to her (<a href="http://scepticon.wordpress.com/2011/02/21/amber-teething-beads-a-few-points-to-consider/#comment-5599">here</a>). But this is exactly the type of reasoned evidence I was looking for so I thank her for the contribution.]</p>

<p>[Update 29/4/13: Apparently there is a chain email circulating blaming amber beads for a case of SIDS, a visitor mentioned this in the polling comments. This seemed implausible to me and a very brief check seems to back up my gut feeling. There is no reason to think that amber beads contribute to SIDS at all. For a more thorough break-down go here: <a href="http://www.hoax-slayer.com/amber-teething-necklace-sids.shtml" target="_blank">http://www.hoax-slayer.com/amber-teething-necklace-sids.shtml</a> . I am not one who feels we need to latch onto any reason to vilify our intellectual opponents and spreading misinformation (especially easily debunked misinformation) is a big no-no in my book.]</p>

<p>[Update 2/12/13: Before commenting that you tried amber and now your baby isn&#39;t/doesn&#39;t/has less trouble &quot;Y&quot; you might want to read <a href="http://www.sciencebasedmedicine.org/seperating-fact-from-fiction-in-pediatric-medicine-infant-teething/" target="_blank">This</a> to see what is actually associated with teething.]</p>

<p>[Update 14/4/14: Science Based Medicine finally covered the Amber Beads topic, see<a href="http://www.sciencebasedmedicine.org/amber-waves-of-woo/" target="_blank"> here</a>.]</p>

<p><strong>Informal Poll:</strong></p>

<p>After reading the preceding post I wonder if you&#39;d like to help me measure what sort of effect this research is having. Please indicate on the poll below your attitude to using Amber beads -</p>

<p>[Edit: Preliminary results from the poll - most consider their opinion unchanged, what a shock. Also the &quot;Other&quot; section is not for insults, if you wish to call me an idiot please do so in the comments of the post where you may be held up for ridicule.]</p>

<p>[googleapps domain=&quot;docs&quot; dir=&quot;spreadsheet/embeddedform&quot; query=&quot;formkey=dHhpb1dvSXBJWDZYaUNicXNOVzVobFE6MQ&quot; width=&quot;760&quot; height=&quot;823&quot; /]</p>

<p><strong>Footnotes:</strong></p>

<p>1. <a href="http://www.3news.co.nz/Teething-necklaces-dangerous---sceptics/tabid/423/articleID/160820/Default.aspx" target="_blank">http://www.3news.co.nz/Teething-necklaces-dangerous---sceptics/tabid/423/articleID/160820/Default.aspx</a></p>

<p>2. I found this paper that analysed the volatile out gassing of amber, succinic acid was not mentioned as an identified component. <a href="http://www.springerlink.com/content/865ku15055np3x78/" target="_blank">http://www.springerlink.com/content/865ku15055np3x78/</a></p>

<p>3. <a href="http://www.emporia.edu/earthsci/amber/physic.htm" target="_blank">http://www.emporia.edu/earthsci/amber/physic.htm</a></p>

<p>4. <a href="http://en.wikipedia.org/wiki/Mohs_scale_of_mineral_hardness" target="_blank">http://en.wikipedia.org/wiki/Mohs_scale_of_mineral_hardness</a></p>

<p>5. <a href="http://www.accessdata.fda.gov/scripts/fcn/fcnDetailNavigation.cfm?rpt=scogsListing&amp;id=339" target="_blank">http://www.accessdata.fda.gov/scripts/fcn/fcnDetailNavigation.cfm?rpt=scogsListing&amp;id=339</a></p>

<p>6. <a href="http://en.wikipedia.org/wiki/List_of_food_additives,_Codex_Alimentarius" target="_blank">http://en.wikipedia.org/wiki/List_of_food_additives,_Codex_Alimentarius</a></p>

<p>7. <a href="http://www.foodreactions.org/allergy/additives/300.html" target="_blank">http://www.foodreactions.org/allergy/additives/300.html</a></p>

<p>8. <a href="http://en.wikipedia.org/wiki/Succinic_acid#History" target="_blank">http://en.wikipedia.org/wiki/Succinic_acid#History</a></p>

<p>9. If it has biologic activity that can be used in a therapeutic fashion, it&#39;s a drug, no quibbling on that point please.</p>

<p>10. <a href="http://en.wikipedia.org/wiki/Salicylic_acid#Medicinal_and_cosmetic_uses" target="_blank">http://en.wikipedia.org/wiki/Salicylic_acid#Medicinal_and_cosmetic_uses</a></p>

<p>11. <a href="http://en.wikipedia.org/wiki/Salicylic_acid#Safety" target="_blank">http://en.wikipedia.org/wiki/Salicylic_acid#Safety</a></p>

<p>12. <a href="http://en.wikipedia.org/wiki/Aspirin" target="_blank">http://en.wikipedia.org/wiki/Aspirin</a></p>

<p>13. ie &quot;It&#39;s got to be good, it&#39;s <em>natural</em>.&quot;. Don&#39;t make me barf.</p>

<p>14. Who am I kidding, there are almost never clear answers. Who wants certainty anyway?</p>

<p>15. <a href="http://safekidspiercecounty.health.officelive.com/Documents/Choking%20and%20Suffocation%20Fact%20Sheet.pdf" target="_blank">http://safekidspiercecounty.health.officelive.com/Documents/Choking%20and%20Suffocation%20Fact%20Sheet.pdf</a> This is an american document but I don&#39;t think necklaces become safer just because we&#39;re in NZ.</p>

<p>16. <a href="http://www.nzchildren.co.nz/infant_mortality.php" target="_blank">http://www.nzchildren.co.nz/infant_mortality.php</a> NZ infant mortality statistics.</p>

<p>17. <a href="http://www.bpac.org.nz/magazine/2010/april/docs/bpj_27_oral_pages_30-41.pdf" target="_blank">http://www.bpac.org.nz/magazine/2010/april/docs/bpj_27_oral_pages_30-41.pdf</a> See page 33.</p>

<p>Related articles</p>

<ul>
	<li>
	<ul>
		<li><a href="http://www.sciencebasedmedicine.org/?p=8745">Death by &quot;alternative&quot; medicine: Who&#39;s to blame? (Revisited)</a> (sciencebasedmedicine.org)</li>
		<li><a href="http://scepticon.wordpress.com/2008/12/08/herbal-medicines/">Herbal medicines</a> (scepticon.wordpress.com)</li>
		<li><a href="http://scepticon.wordpress.com/2010/01/26/what-is-the-harm-of-alternative-medicine/">What is the Harm of Alternative Medicine?</a> (scepticon.wordpress.com)</li>
		<li><a href="http://scepticon.wordpress.com/2010/10/14/complexity-and-health-a-rant/">Complexity and Health - A Rant</a> (scepticon.wordpress.com)</li>
	</ul>
	</li>
</ul>
', '2019-02-06', 123, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'fMRI Foundations Get a Little More Secure', '<p>&nbsp;</p>

<blockquote>
<p>&quot;it&#39;s all about getting to the truth&quot; - Karl Deisseroth. May 20th Nature Podcast</p>
</blockquote>

<p>fMRI, it&#39;s everywhere. From studies looking at how we separate fantasy from reality to identifying psychopaths. The technique offers a very powerful way analyse that most elusive of domains, the contents of another person&#39;s brain. Even so the technology is not without it&#39;s flaws, one flaw is the method of determining relevant data from noise. Another is that the most common signal studied with fMRI, blood oxygenation level-dependent (BOLD) signals, are only an indirect measure of neural activity. Further, it has been until now* an assumption with little empirical support.</p>

<p>To put this in proper perspective it is necessary to explain a little more fully what a BOLD signal is. Essentially the oxygenation level of the blood can be determined via the fMRI by discerning the difference in magnetic properties of oxygenated versus deoxygenated haemoglobin (the protein responsible for shuttling oxygen around our body and keeping us alive). Now, the link between blood oxygenation levels and brain activity is made like this: active neurons are performing cellular functions, cellular functions require energy, energy requires metabolism, metabolism requires oxygen, higher metabolism requires higher oxygen levels. Thus you can make a logical connection from an increase in neuron activity (representing brain activity) and an increase in oxygenated blood flow to an area of the brain.</p>

<p>Because neurons do not have an internal supply of either glucose or oxygen the chain of reasoning above is valid but not having direct empirical support is a potential weakness. To the rescue comes <a href="http://en.wikipedia.org/wiki/Optogenetics" target="_blank">Optogenetics</a>, this relatively new field concerns itself with engineering neurons in such a way as to allow them to be activated by pulses of light. I think you can see where this is going.</p>

<p>The paper &quot;<a href="http://www.stanford.edu/group/dlab/papers/Durand%202010.pdf" target="_blank">Global and local fMRI signals driven by neurons defined optogenetically by type and wiring</a>&quot; published in <em>Nature</em> details how this technique can help in fMRI work.&nbsp; In this case rats were used as a model animal instead of humans. The procedure consisted of injecting the brains of the rats with a viral vector that was targeted to particular cell types in the brain, cortical neurons to be specific. This caused these cells to express a light sensitive protein that would in essence cause the cell to be activated**.</p>

<p>By specifically targeting the cell types of most interest this technique has shown that the BOLD signal detailed above really is correlated with neuronal activity. In reality, while this is an important result of the early implementation of this approach and provides a firmer foundation for the theoretical underpinnings of fMRI work it&#39;s true power lies elsewhere.</p>

<p>In addition to activating the cells of a specific region of the brain this technique can also highlight larger networks that operate within the brain. Neurons do not fire in a vacuum, often one will trigger another which set off a third and so on in a cascading chain which results in thoughts or actions. By selectively activating areas of the brain researchers can then watch the downstream effects of those activations in other parts of the brain that were not directly stimulated. In this way we can effectively build a map of neuronal networks.</p>

<p>This approach will both stimulate new research and perhaps provide a method of validating conclusions drawn from previous work. This looks to be an important new tool for brain research, the full power of which we may not yet realise.</p>

<p>Now that&#39;s exciting.</p>

<p>Footnotes:</p>

<p>* For a given definition of &quot;now&quot;, which corresponds to earlier this year when the paper was published. I really need to get to this stuff faster.</p>

<p>** An ion channel protein, stimulation of which causes ions to flow through the cell membrane. Just like the process that occurs when a neuronal signal is initiated<a href="http://scepticon.wordpress.com/2010/11/26/fmri-foundations-get-a-little-more-secure/">.</a></p>

<p>&nbsp;</p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><img alt="" src="http://s.wisestamp.com/pixel.png?p=mozilla&amp;v=2.0.4&amp;t=1290716835999&amp;u=3306622&amp;e=8815" style="height:1px; width:1px" /></p>

<p>--<br />
Lee, J., Durand, R., Gradinaru, V., Zhang, F., Goshen, I., Kim, D., Fenno, L., Ramakrishnan, C., &amp; Deisseroth, K. (2010). Global and local fMRI signals driven by neurons defined optogenetically by type and wiring Nature, 465 (7299), 788-792 DOI: 10.1038/nature09108</p>

<p>Related articles</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/10/22/gamblers-rewarded-by-near-misses/">Gamblers Rewarded by Near Misses</a> (scepticon.wordpress.com)</li>
	<li><a href="http://psychologyinaction.org/2010/09/14/mind-reading-lie-detection-and-telekinesis-with-fmri-and-eeg-science-fact-and-fiction/">Mind-reading, lie-detection and telekinesis with fMRI and EEG - Science fact and fiction</a> (psychologyinaction.org)</li>
	<li><a href="http://www.citeulike.org/user/kndiaye/article/6742807">Functional MRI at the crossroads.</a> (citeulike.org)</li>
</ul>
', '2020-02-10', 167, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', '"Mantrol" and the Psychology of Destructive Behaviour', '<p>Recently the NZ Transport Authority and the Police introduced a <a href="http://theinspirationroom.com/daily/2010/mantrol-in-mandom-for-nz-male-drivers/" target="_blank">new campaign</a> for reducing driver speeding. The &quot;<a href="http://www.3news.co.nz/Mantrol---the-full-advert/tabid/423/articleID/185847/Default.aspx" target="_blank">Mantrol</a>&quot; concept attempts to link the safe driving with &quot;manliness&quot; and in this way induce those who might speed out of machismo to take a second look at what constitutes that ever mercurial definition of &quot;real man&quot;.</p>

<p>Looking at a few of the comments on the <a href="http://www.stuff.co.nz/national/4332106/Ad-urges-males-to-drive-with-mantrol" target="_blank">Stuff version</a> of this story there is a lot of negativity regarding this campaign. I think many of the comments miss the point. I agree many of the comments (in aggregate) that auto fatalities are a multi-factorial problem, not only speed but driver competency, road condition, alcohol, culture, road laws etc are all contributing to the current situation. To expect one ad campaign to address all of these disparate causes is obviously unrealistic. It is even unrealistic to expect one campaign to address and counter every reason that a person might speed. We have had the graphic advertisements showing the consequences of out of control speeding, these will work on one sub-set of the population. Now we have an approach that may have an effect on a different sub-set.</p>

<p>I <a href="http://scepticon.wordpress.com/2010/01/21/smoking-bans-and-the-effect-of-health-warnings/" target="_blank">wrote in January</a> about a study comparing types of cigarette warnings, the study found that warnings emphasising mortality were less effective on individuals who based their self esteem, at least in part, on their smoking behaviour. As a result those individuals would rate themselves as more inclined to continue smoking. On the other hand, warnings that directly attack the source of self esteem are more effective. Individuals who consider smoking to make them more attractive (the &quot;coolness&quot; factor) will be more influenced by warnings that state the opposite.</p>

<p>One of the conclusions of the study was that warnings may need to be tailored to the population you are trying to influence.The depending factor is how much the behaviour is related to a person&#39;s self esteem and self image.</p>

<p>In attempting to tie safe driving to manliness there is a move towards reaching people at a place in their psyche that relates to their self esteem and how that manifests in their behaviour. In other words if speeding is an expression of one of a person&#39;s core beliefs about themselves ie that they are macho man, then pointing out that in the eyes of others their behaviour is inconsistent with that label may lead to a behavioural change.</p>

<p>Now I do think that the current set of ads don&#39;t quite hit that mark but I do think that they are a step in the right direction. The implicit humour of the ads are also a mark in their favour but they are perhaps a little too close in flavour to a number of other ads currently in the market (multitude of beer ads, even McDonalds ads) trying to tie their product to what real manly men do.</p>

<p>In any event, it is too early to tell whether or if the current crop of anti-speeding ads will have an effect on behaviour. I do think that the use of multiple approaches is valuable in itself as a one-size-fits-all attempt is certainly doomed to failure.</p>

<p>Related articles</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/10/22/gamblers-rewarded-by-near-misses/">Gamblers Rewarded by Near Misses</a> (scepticon.wordpress.com)</li>
	<li><a href="http://adweek.blogs.com/adfreak/2010/11/new-zealand-to-guy-drivers-stay-in-mantrol.html">New Zealand to guy drivers: Stay in mantrol</a> (adweek.blogs.com)</li>
	<li><a href="http://www.eurekalert.org/pub_releases/2010-11/plos-dbd111010.php">Disruptive behaviour disorders in male teenagers associated with increased risk of road crashes</a> (eurekalert.org)</li>
</ul>
', '2020-02-05', 140, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'A Side Benefit of the ''Flu Vaccine - Reduction in Heart Attacks ', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>It&#39;s a bold claim, that being vaccinated for Influenza will protect you against having a heart attack or Myocardial Infarction (MI). Well according to a study published last month in the <a href="http://www.cmaj.ca/" target="_blank">Canadian Medical Association Journal</a> that could well be the case.</p>

<p>The study, &quot;<a href="http://www.cmaj.ca/cgi/rapidpdf/cmaj.091891v1" target="_blank">Influenza vaccination, pneumococcal vaccination and risk<br />
of acute myocardial infarction: matched case&ndash;control study</a>&quot;, included 78,706 individuals over the age of 40 in an attempt to determine whether and how much of an effect the vaccine had on MI. The results showed that the vaccine was associated with an almost 20% reduction in risk of MI compared to the unvaccinated population.</p>

<p>My first thought when I read this was that those individuals who get vaccinated might engage in other activities that would lend themselves to reducing risk of MI, healthy eating habits, regular exercise etc. The so called &ldquo;healthy user&rdquo; effect. This study attempted to control for this sort of confounding factor by using matched controls with similar risk factors. In particular they performed two further analyses that I think comfortably undermine this interpretation of the results.</p>

<p>First they compared the timing of the receipt of the vaccine, ie early or late in the &#39;flu season. Second they looked at subjects who had been vaccinated with the pneumococcal vaccine as a comparison. In the first case there was a greater reduction in risk for those who had received the vaccine early in the season, 21% compared to only 12%. In the second case they found no protective effect for those who had received the pneumococcal vaccine. It seems unlikely that the subjects of the study would vary their healthy habits in precisely the ways they would have to in order to see these results as being independent of the vaccine itself.</p>

<p>An interesting aspect of this kind of epidemiology though is that simply looking at the raw numbers there is a greater incidence of MI in the vaccinated group compared to the unvaccinated group. This is an artefact of how the vaccines are administered clinically. Those patients who have greater cardiovascular risk are also the patients who are more likely to be recommended for vaccination. It&#39;s like noticing that people who buy antiperspirant tend to have sweatier armpits than those who don&#39;t*. Those who are prone to sweaty armpits will likely tend to be those who will buy and use antiperspirant, to accurately gauge effectiveness you would have to control for this factor.</p>

<p>Tying this into the anti-vax focus of this week, one of the claims I&#39;ve seen is that the flu vaccine is useless as it is based on strains that were around the previous season (<a href="http://www.naturalnews.com/029334_flu_vaccines_seizures.html" target="_blank">Here</a> via <a href="http://www.ias.org.nz/natural-immunity/flu-vaccine-side-effects/" target="_blank">IAS</a>). This is distressingly simplistic thinking. This paper shows that the vaccine is far from useless, in fact the most benefit was found within &#39;flu seasons. If there was no effect of the vaccine on the current &#39;flu season strains then the study would not have shown the protective effect that it did. Once again the anti-vax crowd (and by extension the altmed crowd as there tends to be overlap) has shown their inability to grasp the nuance of the situation.</p>

<p>There is a gradation of effect when a vaccine does not exactly match the wild strain, the interaction of antigens and antibodies is more complicated than you might suppose. Changes in the antigen (mutation of the virus over the &#39;flu season) mean that antibodies may bind less tightly and therefore have a reduced effect but that is not the same as no effect.</p>

<p>To sum up, health is a complicated thing. Many factors are inter-related and in order to get the whole picture we sometimes have look at things from an unusual perspective. Vaccinating for the &#39;flu can reduce your risk of heart attack, who knew? But, we should also be wary that we are keeping things within the realm of plausibility. Infection does cause inflammation which can plausibly effect heart function**. This does not mean it&#39;s valid to blame the Wi-fi at your local school for your vague aches and pains.***</p>

<p>&nbsp;</p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><img alt="" src="http://s.wisestamp.com/pixel.png?p=mozilla&amp;v=2.0.3&amp;t=1288662927990&amp;u=8836297&amp;e=8070" style="height:1px; width:1px" /></p>

<p>Footnotes:</p>

<p>*To make up an example.</p>

<p>** See <a href="http://www.sciencebasedmedicine.org/?p=6926" target="_blank">http://www.sciencebasedmedicine.org/?p=6926</a></p>

<p>*** <a href="http://theness.com/neurologicablog/?p=2215" target="_blank">http://theness.com/neurologicablog/?p=2215</a></p>

<p>----</p>

<p>Siriwardena AN, Gwini SM, &amp; Coupland CA (2010). Influenza vaccination, pneumococcal vaccination and risk of acute myocardial infarction: matched case-control study. CMAJ : Canadian Medical Association journal = journal de l&#39;Association medicale canadienne, 182 (15), 1617-23 PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20855479">20855479</a></p>

<p>Related articles</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/11/01/anti-vaccination-in-nz/">Anti-Vaccination In NZ</a> (scepticon.wordpress.com)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=7555">High Dose Flu Vaccine for the Elderly</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=7223">Some Flu Vaccine Updates</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=7838">What does &quot;anti-vaccine&quot; really mean?</a> (sciencebasedmedicine.org)</li>
</ul>
', '2019-02-06', 45, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Wonderwoman', 'Gamblers Rewarded by Near Misses', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>Earlier this year a <a href="http://www.jneurosci.org/cgi/content/abstract/30/18/6180" target="_blank">study</a> published in the Journal of Neuroscience looked at the brains of compulsive gamblers and concluded that when the the gamblers suffered &quot;near-miss&quot; losses their brains reacted as if they had won. <a href="http://seab.envmed.rochester.edu/Jeab/articles/2010/jeab-93-03-0313.pdf" target="_blank">Another study</a> published slightly later in the Journal of the Experimental Analysis of Behaviour also looked at the brains of gamblers but included a control groups of non-gamblers as well. The results were interesting.</p>

<p>First off, what exactly is a near-miss loss? The experiments were performed with slot machine type visual stimuli so in this case lines that contained two out of three matching symbols were considered near-misses. Now from a practical stand point, as the out put is meant to be random a line like this is no closer to winning than one with three different symbols and does not predict any greater likelihood of winning in the future. Although people might realise this from an intellectual stand point it&#39;s still hard not to think &quot;Almost got it that time&quot;.&nbsp; Hence &quot;near-miss&quot;.</p>

<p>The second study did indeed find that gamblers reacted to the near misses more like they were wins, they also found that non-gamblers reacted to the near misses more as if they were true losses. The thing I found interesting though was that the reactions of gamblers to winning while consistent within the gambling group had nothing in common with the reaction in the non-gambling group.</p>

<p>A further interesting finding was that although the non-gambler group had neural responses more similar to losses when confronted with near-misses, they gave similar answers as the gamblers when asked how close they were to a win. In other words they also rated a near-miss as closer to winning than a more random looking output.</p>

<p>By comparing the neural activation of gamblers and non-gamblers the researchers were able to see that non-gamblers had stronger reactions to losing outcomes than did the gamblers. This makes sense in several ways, humans are generally quite loss averse. We will tend to think of a loss as more negative than a similar gain is positive ie a win of a certain amount versus a loss of the same amount do not cancel out, there is a larger negative emotional balance. In contrast, problem gamblers would be expected to view losses as less damaging over all otherwise continuing losses would result in ceasing gambling activities.</p>

<p>In addition the study authors linked this work with a <a href="http://www.economics.harvard.edu/faculty/laibson/files/laibson_science.pdf" target="_blank">previous study</a>, suggesting that problem gamblers are activating regions&nbsp; of their brains associated with impulsive behaviour when wins are experienced while non-gamblers activate regions associated with reflective behaviour when experiencing losses. This part of the study discussion is very interesting and worth a read in itself.</p>

<p>The current study is insufficient to establish the causal direction in the relationship between compulsive gambling behaviour and the network of neural activation that accompanies gambling wins. Even so it is tempting to view the brain response as predisposing a person to becoming a compulsive gambler.&nbsp; If the &quot;high&quot; for a win is greater in certain individuals because of this difference in brain response then this might lead them to gambling abuse behaviour. I hardly think though that there will ever be wide-scale screening programmes to identify potential gamblers.</p>

<p>The more we learn about the functioning of the brain in these sort of situations though the better equipped we will be to effectively help those who are affected by problem gambling.</p>

<p>&nbsp;</p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p>--<br />
Chase, H., &amp; Clark, L. (2010). Gambling Severity Predicts Midbrain Response to Near-Miss Outcomes Journal of Neuroscience, 30 (18), 6180-6187 DOI: <a href="http://dx.doi.org/10.1523/JNEUROSCI.5758-09.2010">10.1523/JNEUROSCI.5758-09.2010</a></p>

<p>Habib, R. &amp; Dixon, M.R. (2010). Neurobehavioral evidence for the &ldquo;near-miss&rdquo; effect in pathological gamblers JOURNAL OF THE EXPERIMENTAL ANALYSIS OF BEHAVIOR, 93 (3), 313-328 : <a href="10.1901/jeab.2010.93-313">10.1901/jeab.2010.93-313</a></p>
', '2019-02-06', 123, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'Breast Cancer Awareness Month Plug', '<p>October is Beast Cancer Awareness Month, or as <a href="http://www.nzbcf.org.nz/" target="_blank">The New Zealand Breast Cancer Foundation </a>is re-branding it &quot;<a href="http://www.nzbcf.org.nz/index.php/about-nzbcf/news/take%20action%20this%20october" target="_blank">Action Month</a>&quot;.</p>

<p>Because of this my post last week regarding the screening technology Thermography has been picked up and included in a blogging carnival. If you are not yet aware of blogging carnivals they are collections of blog posts from across the web highlighting single issues or interests, usually they are hosted on a regular schedule and pass from blog to blog.</p>

<p><a href="http://blogcarnival.com/bc/clist.html" target="_blank">Pick a topic</a> you&#39;re interested in and there&#39;s likely a carnival out there dedicated to it. Anyway, <a href="http://www.highlighthealth.com/" target="_blank">Highlight Health</a> is hosting this month&#39;s edition of the <a href="http://www.highlighthealth.com/breast-cancer/cancer-research-blog-carnival-38-breast-cancer/" target="_blank">Cancer Research Blog Carnival (#38)</a>. Go over and check it out, there looks to be a large selection of high quality writing gathered in one place and I&#39;m honoured to have been selected to be included with the likes of <a href="http://www.sciencebasedmedicine.org" target="_blank">Science-Based Medicine</a> and <a href="http://www.the-scientist.com/blog/" target="_blank">The Scientist</a>.</p>

<p>So get over there and educate yourself, learn about breast cancer screening effectiveness, possible new treatments, scams perpetrated on breast cancer sufferers, underlying causes and other fascinating topics.</p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/09/28/breast-cancer-thermography-good-bad-or-in-between/">Breast Cancer Thermography - Good, Bad or In-between?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=6674">Avastin and metastatic breast cancer: When science-based medicine collides with FDA regulation</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=6940">The mammography wars heat up again</a> (sciencebasedmedicine.org)</li>
</ul>
', '2019-02-06', 431, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'Breast Cancer Thermography - Good, Bad or In-between?', '<p>Breast cancer screening is in the news once again. Late last year <a href="http://tvnz.co.nz/health-news/new-us-mammogram-rules-upset-some-3150574" target="_blank">controversy flared</a> around <a href="http://www.annals.org/content/151/10/716.full.pdf+html" target="_blank">new recommendations</a> in the frequency of screening. (A review of this change can be found <a href="http://www.sciencebasedmedicine.org/?p=1926" target="_blank">Here</a>.) Now it seems that even the type of screening available is generating controversy.</p>

<p>Current breast screening technology in the form of mammograms is a mature approach, the advantages and drawbacks have been extensively studied and are now quite well understood (though more is still being discovered, see the related article at the bottom of this post). As such we can use the mass of accumulated data to create guidelines that attempt to optimise how screening should be done. A balance must be struck between the benefits gained by accurate detection of cancer and the costs, both psychological and financial, of false positives generated by the screening process.</p>

<p>These are the points that must be kept in mind when evaluating new approaches for appropriateness in the consumer screening market. Recently the media has highlighted a minor battle between private medical industry, in the form of breast screening clinic <a href="http://www.clinicalthermography.co.nz/" target="_blank">Clinical Thermography</a>, and professional medical organisations and cancer research and advocacy groups. Yesterday the <a href="http://www.nzherald.co.nz/health/news/article.cfm?c_id=204&amp;objectid=10676347" target="_blank">NZ Herald</a> ran a story about the clash, and you can see TV3 coverage <a href="http://www.3news.co.nz/Women-warned-against-alternative-breast-screening-methods/tabid/372/articleID/152460/Default.aspx" target="_blank">Here</a>*.</p>

<p>At issue is the offering of a breast cancer screening service using thermal imaging to detect incipient or established tumours in the breast. The operating principle behind this technique is the requirement of tumours to generate access to their own blood supply early in the process of formation in order to secure the nutrients required for the unrestrained cell growth that is typically associated with cancer.</p>

<p>This process is called <a href="http://en.wikipedia.org/wiki/Angiogenesis" target="_blank">angiogenesis</a> and entails the formation of new blood vessels in and around the tumour. This brings a larger amount of blood to the area than would be seen normally and this blood would bring extra body heat with it. Given this information it is not unreasonable to attempt to use this physiological change to attempt to detect cancer. By using high resolution thermal imaging the surface temperature of the breast can be measured and used to infer the presence of &quot;hot spots&quot; that may indicate tumours.</p>

<p>Thermography is not a new technology, developed in the 1960s it was largely abandoned in the early 1980s after an influential trial found the false positive rate and sensitivity unsatisfactory. As such, while the approach is not novel there is a distinct lack of large scale trials from which we can determine the appropriate sensitivity and specificity to apply to the technique and thereby judge it&#39;s efficacy in the screening market, especially using current technology. In particular the technique has been emphasised in the existing literature to have the most impact when used in conjunction with more understood approaches such as mammography.</p>

<p>The current halabaloo seems to centre around concerns by the medical community that the appropriate caveats will not be adequately conveyed to the public and thereby may lead to either unwarranted panic in individuals receiving false positive results or complacency in those that may receive false negative tests. In particular <a href="http://www.cancernz.org.nz/assets/files/info/CSNZ_PS_Thermography23August2010.pdf" target="_blank">position paper put out by the Cancer Society et al.</a> attempts to raise awareness that the thermography screening process does not have the data to allow the decisions regarding balance between benefit and harm mention above to be made effectively (a position echoed by the UK organisation <a href="http://www.cancerhelp.org.uk/about-cancer/cancer-questions/thermography-or-heat-mapping" target="_blank">CancerHelp UK</a>).</p>

<p>In investigating this issue I visited the Clinical thermography website to see what they had to say about the relative effectiveness of Thermography and mommography. I found it revealing that on the <a href="http://www.clinicalthermography.co.nz/Site/FAQs.ashx" target="_blank">FAQ</a> page there were two questions addressing accuracy for Thermography and mammograms. The Thermography entry was essentially an apologetic giving reasons why thermography may give a false negative result, no numbers regarding sensitivity or specificity were included in this.Conversely the mammography entry was a condemnatory piece giving percentages of false negative for the two age groups of particular interest, without any further explanation.</p>

<p>The ommision of hard data in the Thermography portion of the FAQ may reflect the dearth of adequate information as decried by the position statement above or may simply be standard marketing tactics. Either way Clinical Thermography aren&#39;t doing themselves any favours with this evasive approach. Now this is not to say that Clinical Thermography is wholly against mammography, they accurately state earlier in the FAQ that Thermography is not a replacement for mammograms and that the technique should be used in conjunction with other screening methods.</p>

<p>Interestingly an <a href="http://www.breastthermography.com/" target="_blank">American site</a> for Breast Thermography is careful to include a disclaimer on the front page of the website stating:</p>

<blockquote>
<p>&quot;Breast thermography offers women information that no other procedure can provide. However, breast thermography is not a replacement for or alternative to mammography or any other form of breast imaging. Breast thermography is meant to be used in addition to mammography and other tests or procedures. Breast thermography and mammography are complementary procedures, one test does not replace the other....&quot;</p>
</blockquote>

<p>This is a straight forward upfront statement that Clinical Thermography would do well to emulate on the front page of it&#39;s own site.</p>

<p>Finally, I&#39;d like to point out that the position I have taken in this post is not contested even by those who would be proponents of this screening method. A commentary piece, published in <a href="http://www.minnesotamedicine.com" target="_blank">Minnesota Medicine</a> last December, entitled &quot;<a href="http://www.minnesotamedicine.com/CurrentIssue/CommentaryPlotnikoffDec2009/tabid/3272/Default.aspx" target="_blank">Emerging Controversies in Breast Imaging: Is There a Place for Thermography</a>?&quot; was essentially pro-thermography at least in terms of investigating the technique further in order to address the questions raised regarding it&#39;s efficacy.</p>

<p>Quoting from the text of this article:</p>

<blockquote>
<p>&quot;The biggest question concerns the efficacy of thermography to detect breast cancer. Despite various studies that suggest positive results for thermography, there has never been a major randomized controlled trial to determine baseline measurements of sensitivity and specificity. It is hard to imagine thermography being accepted by the conventional medical establishment without such data or evidence of cost-effectiveness.&quot;</p>
</blockquote>

<p>and:</p>

<blockquote>
<p>&quot;In lieu of any industry or professional standards for thermography, a variety of practices and protocols have emerged among practitioners and equipment manufacturers. As one practitioner described it, the industry is in its &ldquo;Wild West&rdquo; days.&quot;</p>
</blockquote>

<p>In addition, a <a href="http://www.thermalhealthscreening.com/study3.pdf" target="_blank">study</a> linked to on a pro-thermography website concluded:</p>

<blockquote>
<p>&quot;we currently limit the role of infrared imaging to that of a closely<br />
controlled compliment to clinical exam and high quailty mammography. Our initial data should not be extrapolated to either formal screening or noncontrolled diagnostic environments without appropriate evaluation, preferably in prospective controlled multicenter trials.&quot;</p>
</blockquote>

<p>In summary, Thermography is at this stage a screening method of unproven efficacy. This is not to say that it could not become a valuable adjunctive tool in screening for breast cancer but that caution should be exercised by both promoters and customers of the technology not to oversell or over rely on the results obtained using this technique. Further research should be undertaken to determine the answers the the questions raised about the technique, namely accurate estimates of the sensitivity and specificity of the technique and what role in the screening process it is best suited for.</p>

<p>---</p>

<p>*This link seems to be down currently, but a note with regard to Breast Self-Examination (BSE) as mentioned in the TV3 story. This has also come under scrutiny as a practice that does not lead to improved outcomes. See the review of the new breast cancer screening guidelines linked to above.</p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://www.sciencebasedmedicine.org/?p=6940">The mammography wars heat up again</a> (sciencebasedmedicine.org)</li>
</ul>
', '2019-02-06', 341, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Is Acupuncture Worth a Punctured Lung? or Does the Risk Out Weigh the Benefit?', '<p>Friday&#39;s issue of <a href="http://www.nzma.org.nz/journal/" target="_blank">The New Zealand Medical Journal</a> includes a case report of pneumothrorax in a recipient of acupuncture. For the interested layperson out there a <a href="http://en.wikipedia.org/wiki/Pneumothorax" target="_blank">pneumothorax</a> in the collection of air in the space between the lung and chest wall leading in extreme cases to cardiac arrest. Acupuncture can result in pneumothorax when the needle is inserted into the lung tissue while the patient is breathing leading to the laceration of the lung and air being forced out of the lung and into the pleural cavity<strong>1</strong>. Mmm-mmm, gimmie some of that lung collapsing goodness.</p>

<p>Now lest I give the impression that complications from acupuncture use are common I will hasten to add that they are not. One paper estimates the rate of serious adverse events at approximately 1 per 20,ooo patients<strong>2</strong>. Though if we look at the rates of acupuncture use in the United States as an example, as of about 2007 approximately 1% of the population reported using acupuncture in the previous 12 months<strong>3</strong>. This translates to about 155 serious adverse effects per year. Another study found over 2% of patients reported adverse reactions that required treatment<strong>4</strong>, commonly for bleeding or pain. Multiply these figures by the likely worldwide numbers of people receiving acupuncture.</p>

<p>Lets compare this with the conventional medical field, the drug Terfenadine marketed under the trade name Seldane (Teldane here in NZ) was removed from the market in the US due to increased risk of cardiac arrhythmia when used in conjunction with certain other drugs. This expressed itself as a risk of 0.04 - 0.08 per million &quot;defined daily doses&quot;<strong>5</strong>. Once a replacement drug came on the market Terfenadine was taken off.</p>

<p>Pneumothorax as a complication from acupuncture is&nbsp; rare even in this subgroup. More common is infection. As I&#39;ve noted before<strong>6</strong>, the underlying theory of acupuncture is the manipulation of life energies (Qi or Chi), blockages or imbalances in which are the cause of disease. If such is the case then why should the treating physician<strong>7</strong> bother with proper antiseptic technique? I suspect that most modern practitioners are however not so far down the rabbit-hole that they have thrown away germ theory completely, at least the outward practical side involved in cleaning and sterilising implements. Which is why even infections are still relatively infrequent.</p>

<p>I would like to point out however that given the implausibility of the treatment basis, coupled with the fact that most large well designed studies find no benefit beyond placebo does make the existence of <em>any</em> complications ethically troubling. If your treament is no more than an elaborate placebo, are you willing to suffer adverse effects because of it? As reported by Dr Novella of Science Based Medicine<strong>8</strong>, a recent review of acupuncture admitted that sham (placebo) acupuncture was as good a &quot;real&quot; acupuncture.</p>

<p>Lets delve into the definition of &quot;sham&quot; acupuncture a little more to give the proper context to this revelation. Whereas &quot;real&quot; acupuncture depends on the proper insertion of the needles in specific meridian points on the body sham acupuncture can be considered to be either the placement of needles into non-meridian points, or the use of implements that feel like needles to the patients but do not pierce the skin like toothpicks<strong>9</strong>. This indicates that it doesn&#39;t matter where you stick the needles and it doesn&#39;t even matter <em>if</em> you stick the needles. How then can we conclude that acupuncture works if you don&#39;t need to perform the two defining characteristics of acupuncture?</p>

<p>Given this background I find it difficult to imagine why acupuncture continues to be recommended despite convincing evidence of efficacy and indisputable evidence of harm. All medical interventions carry some element of risk, this is then weighed against the potential for benefit. However when there is no benefit any amount of risk must make that equation lopsided with regard to harm. With that in mind, if you are attracted to acupuncture as a therapy let me recommend sham acupuncture as the way to go. All the placebo-y goodness of real acupuncture without the potential for the nasty drawbacks of infection, bleeding, pain or even pneumothorax.</p>

<p><strong>Further reading:</strong></p>

<p>Type &quot;Acupuncture&quot; and &quot;Infection&quot; or &quot;Pneumothorax&quot; into <a href="http://www.ncbi.nlm.nih.gov/pubmed/" target="_blank">Pubmed</a> as key words and you will find a variety of papers, a selection of which are below:</p>

<p><a href="http://findarticles.com/p/articles/mi_6794/is_3_26/ai_n30913759/" target="_blank">Acupuncture induced pneumothorax:a case report</a> (not the report mentioned in the post)</p>

<p><a href="http://www.bmj.com/cgi/content/full/340/mar18_1/c1268" target="_blank">Editorial:Acupuncture transmitted infections</a></p>

<p><a href="http://www.ncbi.nlm.nih.gov/pubmed/20534038" target="_blank">Cutaneous Mycobacterium haemophilum infection in a kidney transplant recipient after acupuncture treatment.</a></p>

<p><a href="http://www.ncbi.nlm.nih.gov/pubmed/20617357" target="_blank">Acupuncture needle-associated prosthetic knee infection after total knee arthroplasty</a></p>

<p><strong>Footnotes:</strong></p>

<p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19358511" target="_blank">Clinical analysis on 38 cases of pneumothorax induced by acupuncture or acupoint injection</a></p>

<p>2. <a href="http://aim.bmj.com/content/22/3/122.abstract" target="_blank">A cumulative review of the range and incidence of significant adverse events associated with acupuncture</a></p>

<p>3. <a href="http://nccam.nih.gov/health/acupuncture/introduction.htm" target="_blank">http://nccam.nih.gov/health/acupuncture/introduction.htm</a></p>

<p>4. <a href="http://content.karger.com/ProdukteDB/produkte.asp?Aktion=ShowPDF&amp;ArtikelNr=209315&amp;Ausgabe=247634&amp;ProduktNr=224242&amp;filename=209315.pdf" target="_blank">Safety of acupuncture: results of a prospective observational study with 229,230 patients and introduction of a medical information and consent form.</a></p>

<p>5. <a href="http://europace.oxfordjournals.org/content/9/suppl_4/iv23.full" target="_blank">Detection and reporting of drug-induced proarrhythmias: room for improvement</a></p>

<p>6. <a href="http://scepticon.wordpress.com/2007/10/29/acupuncture/" target="_blank">Scepticon: Acupuncture</a></p>

<p>7. And here I use the term <em>loosely</em>.</p>

<p>8.<a href="http://www.sciencebasedmedicine.org/?p=6391" target="_blank">Acupuncture Pseudoscience in the New England Journal of Medicine</a></p>

<p>9. I kid you not, here are a couple of the studies:<br />
<a href="http://www.chiro.org/research/DISCONTINUED/Noninvasive_Placebo_Acupuncture.pdf" target="_blank">Description and Validation of a Noninvasive Placebo Acupuncture Procedure</a><br />
<a href="http://www.ncbi.nlm.nih.gov/pubmed/19433697" target="_blank">A randomized trial comparing acupuncture, simulated acupuncture, and usual care for chronic low back pain</a></p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="http://scepticon.wordpress.com/2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END--></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://www.sciencebasedmedicine.org/?p=6485">NEJM and Acupuncture: Even the best can publish nonsense.</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=6391">Acupuncture Pseudoscience in the NEJM</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=5112">A Pair of Acupuncture Studies</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=5998">Acupuncture and Modern Bloodletting</a> (sciencebasedmedicine.org)</li>
</ul>
', '2020-02-05', 245, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'Pharmacy Customers Perception of Complementary and Alternative Medicine in Pharmacies', '<p>Going through the papers cluttering my inbox I found this survey of Australian pharmacy customers relating to their use of CAM and their impressions of how pharmacists should approach the subject.</p>

<p>Regular readers of Sciblogs may remember a kerfuffle earlier in the year regarding the sale of homeopathic remedies in pharmacies, I and others were uncomfortable with these items being sold in pharmacies to begin with. Fortunately, when surveyed homeopathy didn&#39;t make it into the top ten modalities used in the last 12 months, though 3% noted that they had seen a homeopath.</p>

<p><a href="http://www.biomedcentral.com/content/pdf/1472-6882-10-38.pdf" target="_blank">This survey</a> was published in <a href="http://www.biomedcentral.com">BioMed Central</a>&#39;s journal of Complementary and Alternative Medicine. I might point out that I disagree with the authors views of Complementary Medicine (CM) but I agree with many of the conclusions of the survey, though I suspect for different reasons.</p>

<p>The survey included data from 1,221 respondents from 54 pharmacies that cover both rural and urban areas. Beyond that the methods aren&#39;t particularly interesting, people filled out forms.</p>

<p>Findings of the survey showed that a significant number of pharmacy customers think that it is important for pharmacists to be knowledgeable about CM and to know about their customer&#39;s CM use. I would agree with this, pharmacists should be aware of how CM is marketed and of the claims made on order to give customers appropriate advice on effectiveness. Another result of the survey that helps with this point is that almost 70% of respondents agreed that they trust their pharmacist&#39;s advice regarding CM. This reveals an excellent opportunity for education of the public regarding these modalities.</p>

<p>In addition many of the respondent felt comfortable telling pharmacist about their CM use whereas previous research has shown this not to be the case for patients of other medical practitioners. Again this is an opportunity for pharmacists to assess the safety of CM modalities their patients are using, especial in conjunction with other treatments (this was also a conclusion of the survey).</p>

<p>That said, the survey also revealed that many customers rely on family and friends as information sources. This accords with with existing research on the importance of personal anecdote in making decisions. Next most popular were medical doctors (not bad) and in third place (disturbingly) was the media. Pharmacists were in 6th place after naturopaths and pharmacy assistants. While far down on the list pharmacists still rank and one of the important sources of information and should not be under estimated.</p>

<p>One of the questions that I disagree with the majority of respondents on is regarding the inclusion of natural medicine practitioners in pharmacy practices. To me this is inviting abuse of the pharmacist&#39;s position of authority, it might even undermine some customers trust of the institution (I&#39;d certainly think twice about any pharmacy that did this). At the very least it may allow pharmacists to divest themselves of the responsibility to actually learn about the alternative products they may be selling.</p>

<p>In conclusion, I consider the results of this survey important to keep in mind when considering the role of pharmacists in the field of CM. Pharmacists are in a somewhat unique position to educate the public regarding CM as a consequence of the level of trust afforded to them by customers. It also reveals that pharmacies are vulnerable to particular abuse for exactly the same reason, products sold in pharmacies are lent an aura of respectability by association.</p>

<p>It behoves pharmacists to take seriously the responsibility to be current on the debate around the safety and efficacy of CM modalities and be able to confidently relay this information to customers. No longer should pharmacists sit on the sidelines while irrationality invades their practice, hiding behind public demand as an excuse for not taking a stand for science based therapies.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END-->--<br />
Braun, L., Tiralongo, E., Wilkinson, J., Spitzer, O., Bailey, M., Poole, S., &amp; Dooley, M. (2010). Perceptions, use and attitudes of pharmacy customers on complementary medicines and pharmacy practice BMC Complementary and Alternative Medicine, 10 (1) DOI: <a href="http://dx.doi.org/10.1186/1472-6882-10-38">10.1186/1472-6882-10-38</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://sciblogs.co.nz/code-for-life/2010/01/25/homeopathic-remedies-in-nz-pharmacies/" target="_blank">What are homeopathic Remedies Doing in New Zealand Pharmacies?</a> (sciblogs.co.nz/code-for-life)</li>
	<li><a href="http://sciblogs.co.nz/skepticon/2010/02/24/new-zealand-pharmacy-ethics-in-relation-to-homeopathy-in-the-wake-of-homeopathy-report/" target="_blank">New Zealand Pharmacy Ethics in Relation to Homeopathy in the Wake of Homeopathy Report</a> (sciblogs.co.nz/skepticon)</li>
	<li><a href="http://sciblogs.co.nz/skepticon/2010/01/26/what-is-the-harm-of-alternative-medicine/" target="_blank">What is the Harm of Alternative Medicine?</a> (sciblogs.co.nz/skepticon)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=5211">What Do You Expect From Your Pharmacy?</a> (sciencebasedmedicine.org)</li>
</ul>
', '2019-02-06', 281, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'Randomness and Clustering: Is the Number of Twins in Timaru a Mystery?', '<p>If you saw 3 News last night you might have caught the story about the bumper crop of twins born this year. The prologue to the story gave the impression of a mystery with words to the effect of&nbsp; &quot;Experts are at a loss to explain it&quot;, I personally think this was a sloppy attempt to generate some &quot;Experts are baffled&quot; buzz around an essential pointless story that just filled up a slow news day.</p>

<p><a href="http://www.stuff.co.nz/timaru-herald/news/3816099/Triple-the-twins-for-Timaru" target="_blank">Stuff also covered the story</a> but without the mystery aspect, good thing because the stats given at the end of the piece kind of belie that approach.</p>

<blockquote>
<p>&quot;The previous year was another big year for twins with ten sets born out of 620 babies. In 2005 and 2006 there were 542 babies born, including six sets of twins. In 2004 and 2005 only two sets of twins were found among the 571 babies born and in the 2003 and 2004 year, there were sevens sets of twins and one set of triplets in the 557 babies born.&quot;</p>
</blockquote>

<p>So in other words the number goes up and down every year and this year just happened to be a cluster of births higher than average. Boring.</p>

<p>What&#39;s the deal with randomness though and why are we so poor at recognising it? We tend to think of random events or locations as those that are approximately evenly distributed in time or space. This view of randomness however gives a false impression of what it means to be truly random.</p>

<p>Randomness is more a measure of unpredictability than it is of aesthetic impression. There are different ways of defining this property but one approach is to apply the criteria of an algorithm. An algorithm is essentially a series of instructions, the more instructions, the more complicated the algorithm. One such might be &quot;1. from an initial number add 5, 2. repeat step 1.&quot;. This would be an algorithmic representation of a sequence of numbers at regular increments of 5 eg 1,6,11,16,21.</p>

<p>Nothing random about that, the key here though would be that a sequence of really random numbers wouldn&#39;t be able to be represented by an algorithm that was less complicated than the sequence itself, ie it would be it&#39;s own algorithm and would not be able to be compressed any further.</p>

<p>What has this got to do with groups of twins? Well, if events such as the birth of twins are actually random (simplifying the world somewhat) then we would expect to see variations in the number of births in any one place. Based on this assumption we can look back at previous numbers to see whether this year is within the range we would expect.</p>

<p>Using the figures from the story and removing this year&#39;s number and the year that only 2 twins were born as a possible outlier I get a range of between 0.5% and 2% of births being twins, with a high probability that normal variation will fall in this range. The percentage of twin births this year is 1.8%, high but apparently normal.</p>

<p>Now the sample size here is very small so I wouldn&#39;t put too much trust in it but it is indicative that there is nothing really out of the ordinary going on here. According to the <a href="http://www.nzmba.info/" target="_blank">NZ Multiple Birth Association</a> there were 900 multiple births last year in NZ (incl. triplets) this is about 1.4% of the 63,000 live births in NZ last year. So rough and ready these numbers may be but they aren&#39;t too far off the mark, some places will be higher than average and others lower.</p>

<p>So when several rare(ish) events happen at the same time or place, consider; is this really unusual? What would we expect if it was just random?</p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/07/14/miracles-what-do-we-mean/">Miracles: What Do We Mean?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/05/21/is-your-boss-a-better-liar-than-you-probably-yes/">Is Your Boss A Better Liar Than You? Probably, Yes</a> (scepticon.wordpress.com)</li>
	<li><a href="http://www.escapistmagazine.com/news/view/102085-The-Big-Dose-of-Random-Returns">The Big Dose of Random Returns!</a> (escapistmagazine.com)</li>
</ul>
', '2019-02-06', 736, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Is there a Biochemical Marker for Suicide?', '<p>Suicide is a sensitive subject, by it&#39;s very nature it seems we are obliged to treat it with kid gloves. In public it is virtually taboo to even mention suicide, in news media <a href="http://www.salient.org.nz/features/quiet-pages-the-reporting-of-suicide-in-new-zealand%E2%80%99s-news-media" target="_blank">euphemisms are employed</a> in order to avoid explicit use of the &quot;S&quot; word. Attitudes are beginning to change, with more vocal discussion about mental illness and euthanasia in both this country and abroad.</p>

<p>One of the key issues is whether a person is capable of deciding to end their own life or if such a decision automatically excludes them from the definition of mentally competent. I found myself pondering these things as I attempted to come up with a way to introduce the research that is ostensibly the focus of this post.</p>

<p>Regardless of your moral position on the subject of suicide I think we can mostly agree that identifying persons at risk of suicidal tendencies would be helpful in alleviating the pain that accompanies this choice (if indeed it can be described as such). This is where a <a href="http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0011089" target="_blank">paper published recently in PLoSONE</a> comes in. The study authors point out in the introduction that previous work has been able to correlate increased blood brain barrier permeability with suicide in patients with prior mental disorder.</p>

<p>Perhaps at this point I should take a step back and provide a little more information on what we are discussing here. The blood brain barrier (BBB) is a system of control that restricts what can and cannot pass between the normal circulatory system and the cerebrospinal fluid (CSF) or the bath that your brain sits in.</p>

<p>In practical terms this means tight connections between the cells of your <a href="http://en.wikipedia.org/wiki/Capillary" target="_blank">capillaries</a> to prevent leaks and transport systems to get nutrients back and forth across the barrier. Imagine a dam made of tightly packed stones with channels for the controlled movement of water and you have the basic idea.</p>

<p>Anyway, if the BBB becomes more permeable then it is reasonable to suppose that proteins found in the CSF would be found in higher concentrations in the blood than would normally be expected. If the permeability of the BBB is also correlated with suicidal behaviour then the presence of these proteins become an indirect test for suicidal tendencies.</p>

<p>This is the hypothesis that the research then tested, ie. does the presence of proteins in the blood normally found in the CFS correlate with suicidal tendencies? This study looked specifically at a protein known as S100B, primarily associated with certain cells in the brain and spinal cord. Included in the study were 64 adolescents (average age ~14.5 yrs) diagnosed with either psychosis or mood disorders and 20 healthy control subjects.</p>

<p>The subjects were evaluated and their <a href="http://medical-dictionary.thefreedictionary.com/Suicidality" target="_blank">suicidality</a> was ranked from 1-7*, Blood tests then determined the levels of S100B. The findings showed that levels of S100B significantly correlated with suicidality in the subjects. Looking at the data accompanying the study it seems there is a wide margin of uncertainty on these readings. With a relatively small number of subjects I&#39;m not particularly surprised by this but I would be looking to see more investigation into this approach to determine it&#39;s reliability.</p>

<p>Obviously this technique will not replace psychiatric evaluation, it may prove useful though in helping identify those that are most at risk of suicidal behaviour. If I may return to the broader issues I raised at the start of this post, I would also find it interesting if this test (once extensively validated) could separate those who wish to end their lives due to illness into groups consisting of those with suicidal thoughts because of mood disorders and those who are otherwise of sound mind.</p>

<p>Something to think about.<br />
--</p>

<p>* 1-no suicidality is present, 2-very mild (thoughts when angry), 3-mild (occasional thoughts), 4-moderate (thoughts present in the last week), 5-moderately-severe (recurrent thoughts present almost daily), 6-severe (current suicidal plan), 7-extremely severe (patient attempted suicide within the last week)</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><a href="http://twitter.com/Scepticon"><img alt="Scepticon on Twitter" src="http://sciblogs.co.nz/skepticon/files/2009/11/Twitter-2.png" style="width:75,height=60" /></a></p>

<p><!--WISESTAMP_SIG_END-->--</p>

<p>Falcone T, Fazio V, Lee C, Simon B, Franco K, Marchi N, &amp; Janigro D (2010). Serum S100B: a potential biomarker for suicidality in adolescents? PloS one, 5 (6) PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20559426">20559426</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/07/02/is-there-something-fishy-about-psychosis/">Is there Something Fishy about Psychosis?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://www.theness.com/neurologicablog/?p=2140">Locked-In Syndrome and the Right to Die</a> (theness.com)</li>
	<li><a href="http://sciblogs.co.nz/macdoctor/2010/07/21/killing-me-softly%e2%80%a6/">Killing Me Softly</a> (sciblogs.co.nz/macdoctor)</li>
</ul>
', '2020-02-05', 125, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'The Risky Business of Hunger', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a></p>

<p>We like to think of ourselves as rational actors when it comes to making decisions, we take in information, process it and choose the path that we think will lead to a desirable outcome (if we aren&#39;t deep-seated masochists I suppose). Regular readers of this blog and others that espouse a sceptical viewpoint will know that this isn&#39;t really the case. We are influenced by a large number of factors from implicit biases, to environmental factors, and errors of thinking. The hope is that if we are aware of these factors we can go some way toward mitigating their effects and making choices that are both rational and lead to improving our lives.</p>

<p>Well, here&#39;s another one for you. You may have guessed by the title of this post that it involves food and risk taking behaviour. <a href="http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0011090" target="_blank">A paper published last month</a> in <a href="http://www.plosone.org/">PLoS ONE</a> out of the <a href="http://maps.google.com/maps?ll=51.5247888889,-0.133577777778&amp;spn=0.01,0.01&amp;q=51.5247888889,-0.133577777778%20%28University%20College%20London%29&amp;t=h">University College London</a> looked at how hunger and food intake affected choices that had a monetary reward. The actual experimental design ran something like this, subjects fasted for 14 hours they then performed tasks that in effect were an idealised lottery, the tasks were performed before, directly after and an hour after a standardised meal. Over this time the subjects also had blood samples taken to measure hormones that correlate with hunger and energy reserves.</p>

<p>The task subjects had to perform consisted of choosing one of a pair of &quot;lotteries&quot; where there was a 25% chance of receiving one of four monetary amounts. Each pairing was designed so that there was always a difference in risk between the two&nbsp; (see the picture it&#39;s hard to explain).</p>

<p>[caption id=&quot;attachment_2081&quot; align=&quot;aligncenter&quot; width=&quot;300&quot;]<a href="http://scepticon.files.wordpress.com/2010/07/lottery.gif"><img alt="" src="http://scepticon.files.wordpress.com/2010/07/lottery.gif?w=300" style="height:144px; width:300px" /></a> Paired Lottery doi:10.1371/journal.pone.0011090.g002[/caption]</p>

<p>How the subjects performed on the tasks was measured to determine the amount of risk aversion. In other words, humans have a tendency to normally prefer less risky choices. The effect of of hunger and especially immediate satiation (right after eating the meal) is to decrease this risk aversion and to make the subjects more risk neutral.</p>

<p>This way of referring to the subject matter is a little counter intuitive and can take a bit of getting used to, the bottom line is that the researchers looked for the point at which the subjects were equally likely to choose the &quot;safe&quot; bet which promised an certain average reward, and a &quot;risky&quot; bet that may lead to a higher average pay off but a lower chance of receiving it. Thus risk aversion has been reduced. By varying the reward amounts the researchers can measure the degree of risk aversion in each subject as the trial proceeds.</p>

<p>Actually the correlation is more complex than I would have thought, not only is the fact that calories are received taken into account but also the amount of calories. It seems that the size of the meal (in terms of calorific intake) is assessed to determine if it meets the rate of food intake required to meet baseline energy requirements. If it does risk aversion is increased (less risky behaviour) if not the risk aversion is decreased.</p>

<p>In hormonal terms this meant that a greater drop in the hormone associated with hunger was correlated with greater risk aversion but a smaller drop meant an increase in risk taking behaviour. The study authors also note that the adiposity of an individual (eg higher BMI) correlated with the size of the hormonal decrease after eating with higher BMI subjects experiencing a smaller drop and a corresponding greater increase in risk taking behaviour.</p>

<p>In effect we not only look at the reward in terms of the gain we will receive compared to our external resources (cash in the bank, say) but also in relation to our internal resources (metabolic requirements for example). This makes sense if we consider that for most of our history true advantage was not measured in abstract accumulation of &quot;wealth&quot; which we would recognise today but in available energy, including that within our bodies. That&#39;s just a speculation of course, I&#39;m no expert in this area.</p>

<p>What is the take home message of this research then? Well first off we should be careful to realise that risk taking behaviour is not limited to single domains in our lives. If you take away from this that only financial decisions are affected then that is too narrow an interpretation. In the final analysis though, no matter our eating habits or body size, we should endeavour to think over important decisions carefully and be aware of the changeable nature of our biology and it&#39;s effects on our thinking.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END-->--</p>

<p>Symmonds M, Emmanuel JJ, Drew ME, Batterham RL, &amp; Dolan RJ (2010). Metabolic state alters economic decision making under risk in humans. PloS one, 5 (6) PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20585383">20585383</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/06/28/can-the-order-you-view-things-affect-your-opinion-of-them-or-to-make-a-good-impression-ensure-youre-introduced-first/" target="_blank">Can the Order You View Things Affect Your Opinion of Them? Or, To Make a Good Impression Ensure You&#39;re Introduced First</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/06/11/does-your-environment-determine-your-behaviour/" target="_blank">Does Your Environment Determine Your&nbsp;Behaviour? </a>(scepticon.wordpress.com)</li>
	<li><a href="post.php?post=1841&amp;action=edit">Dig-in Or Adapt: The Effect of Political Views on Changing One&rsquo;s&nbsp;Mind</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/07/02/is-there-something-fishy-about-psychosis/" target="_blank">Is there Something Fishy about&nbsp;Psychosis?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/04/01/like-i-needed-another-reason-to-lose-weight/">Like I Needed Another Reason To Lose Weight</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/06/04/alcohol-effects-on-teenage-brains-and-correlations-between-availability-and-violence/">Alcohol: Effects on Teenage Brains and Correlations Between Availability and Violence</a> (scepticon.wordpress.com)</li>
</ul>
', '2019-02-06', 123, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Wonderwoman', 'Miracles: What Do We Mean?', '<p>Have you ever described an event as miraculous? Perhaps it was a near-miss accident, recovery from an illness or some other fortuitous moment in your life. Did you stop to consider what you meant by that description or did or roll off your tongue like so many other cultural conventions, without a second thought?</p>

<p>One of the reasons I write this blog1 is to allow people the opportunity to examine the world and themselves in more detail and more reflectively than they might ordinarily be inclined to do. In this I have largely attempted to do so using science directly, by showing research that reveals facts about ourselves and the world around us that are not necessarily intuitively obvious (such as <a href="http://scepticon.wordpress.com/2010/06/28/can-the-order-you-view-things-affect-your-opinion-of-them-or-to-make-a-good-impression-ensure-youre-introduced-first/" target="_blank">biases in our reasoning</a>).</p>

<p>I thought I would deviate from the strictly scientific today to discuss miracles, especially the depiction of miracles in the media and what is really meant when we resort to the designation of &quot;miracle&quot; in describing events.</p>

<p>Recently I have started reading popular philosophy books, trying to be a well rounded person or something, or possibly just so I sound intelligent at parties2. I may delve a little bit into philosophy here but hopefully can keep it light enough that you won&#39;t even notice.</p>

<p>One of the books on my reading list brought up the concept of miracles and attempted to outline the different definitions that are attached to this word3. This sparked in me a thought about how the word is used by those around me, in the general population these multiple versions of the meaning get seem to get merged into an amorphous description that verges on meaninglessness.</p>

<p>Many of the definitions of the word that I could find invoked some sort of supernatural component, in particular the assertion that such an event contravenes the laws of nature. By this criteria I have never witnessed, nor seen credible reports of a single miracle, yet I hear the word used all the time4. How can we reconcile how the word is defined and how it is used?</p>

<p>Let us note one instance of the (over)use of this word, last year when an aeroplane crash landed in the Hudson river after hitting a flock of birds soon after take off the event was labelled a miracle. Currently no fewer than ten news stories with the word &quot;miracle&quot; in the title are listed in the <a href="http://en.wikipedia.org/wiki/US_Airways_Flight_1549" target="_blank">Wikipedia article</a> about this event and I suspect there are many more not mentioned. This seems to be the type of event that attracts exclamations of &quot;Miracle&quot; yet if we delve into the details there is no point at which we can reliably determine that the laws of nature have been suspended or otherwise altered to allow the final outcome.</p>

<p>If we are committed to the definition that for a miracle to have occurred the laws of nature must be violated then this event does not qualify.</p>

<p>Of the multiple meanings that I mentioned above it would appear the most frequently used makes the word &quot;miracle&quot; synonymous with &quot;unlikely coincidence&quot;. This though is insufficient to describe what most people would consider to be miracles as it ignores whether or not an event has any beneficial consequences, so lets add that requirement into our ad hoc definition.</p>

<p>The trouble with this definition is that it leaves us unable to determine what we might term &quot;True Miracles&quot; from merely random (beneficial) occurrences. Especially in as much as, like the Hudson river crash above, said miracles have no religious significance5. This pre-supposes however that we would wish to make such a distinction, if (as I suspect) our use of the word actually no-longer assumes the intervention of supernatural forces then our definition of &quot;True Miracles&quot; becomes superfluous, no different than what we might consider a regular miracle.</p>

<p>In this case the word simply becomes short hand for an amazing6 coincidence that is of benefit to a person or persons7. It would then seem that our definition of miracle actually stems from our own inability to sufficiently appreciate how probability acts in our lives. How many of us are in a position to calculate how probable any particular event is? Our normal day-to-day experience is a poor guide regarding this but if we cannot perform the calculation then by what basis do we conclude that an event is likely or unlikely?</p>

<p>I will readily admit that musings like this are have little practical significance but I think are still worth considering in order to develop for ourselves a more consistent and precise outlook. I hope that there are others beside myself that also see value in this.</p>

<h3>footnotes</h3>

<p>1. In general not this particular entry.</p>

<p>2. Who am I kidding? I don&#39;t go to parties.</p>

<p>3. The definitions were broken down into 4: a) Violation miracles where the laws of nature are violated; b) Willed miracles where miracles occur via an act of a supreme being&#39;s will; c) Inexplicable miracles where the event is unexplainable via the laws of nature though does not necessarily violate them; and finally d) Coincidence miracles, as discussed in this article.<br />
See <a href="http://books.google.co.nz/books?id=ar0XYJAhKXIC&amp;printsec=frontcover&amp;dq=Nicholas+Everitt+%22The+Non-Existence+of+God%22&amp;hl=en&amp;ei=Tis9TOPaL4T4swPZ9ozeAg&amp;sa=X&amp;oi=book_result&amp;ct=result&amp;resnum=1&amp;ved=0CC0Q6AEwAA#v=onepage&amp;q&amp;f=false" target="_blank">Nicholas Everitt&#39;s &quot;The Non-Existence of God&quot;</a> p112-ish.</p>

<p>4. Okay, that&#39;s an exaggeration, but certainly more often than my experience tells me it should be used.</p>

<p>5. Try putting the word &quot;miracle&quot; into <a href="http://news.google.com/" target="_blank">google news</a> and see how many look explicitly religious.</p>

<p>6. Or not so amazing, depending on your point of view.</p>

<p>7. Miracle is definitely easier to say, though it does leave us open to misinterpretation by those who apply a more strict definition of the word than we do.</p>
', '2019-02-06', 155, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'Is there Something Fishy about Psychosis?', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a></p>

<p>Psychosis is a scary word, conjuring images of people who have so lost touch with reality that they are unable to integrate with society. As with most everything else this condition exists on a continuum, mild symptoms may pose no problem for the sufferer<a href="#Back1" name="1">1</a> nor be outwardly visible. Previous studies have seen correlations between the intake of <a href="http://en.wikipedia.org/wiki/Polyunsaturated_fat">polyunsaturated fatty acids</a> (with the cute acronym <a href="http://en.wikipedia.org/wiki/Polyunsaturated_fatty_acid">PUFA</a><a href="#Back2" name="2">2</a>) and increased severity of psychotic symptoms, with this in mind a <a href="http://www.biomedcentral.com/content/pdf/1471-244x-10-38.pdf" target="_blank">study was performed in Sweden</a> looking at the dietary intake of fish and the incidence of psychosis symptoms in the general population.</p>

<p>In total 33,623 women completed the study which covered the period between 1991/92 to 2002/03 (with questionnaires at the beginning and end of this period). This group was then classified based on their answers to the questionnaires into 3 groups: Low, middle and high frequency of symptoms, where the low group included women with no symptoms. This gave a split of 18,411, 14,395 and 817 women in the groups respectively. The first question I had reading this study is how do you classify someone with psychotic symptoms? The women in the study completed two questionnaires to provide the information for this part.</p>

<p>The first was the <a href="http://cape42.homestead.com/index.html" target="_blank">Community Assessment of Psychic Experiences</a> (CAPE, another cool acronym), this contained questions ranging from those looking at emotional states such as &ldquo;Do you ever feel sad?&rdquo;, to those that address personal perception like &ldquo;Do you ever feel pessimistic about everything?&rdquo;. Also included are the questions that we would more easily recognise as relating to psychosis such as &ldquo;Do you ever feel as if a double has taken the place of a family member, friend or acquaintance?&rdquo; or &ldquo;Do you ever see objects, people or animals that other people cannot see?&rdquo;.</p>

<p>There are also questions that might seem to generate positive answers from a wide range of the population that we would not consider psychotic such as &ldquo;Do you ever think that people can communicate telepathically?&rdquo;, a belief that if I can take what I see in <a href="http://www.beloitdailynews.com/articles/2010/06/26/news/local_news/news062602.txt" target="_blank">the media</a> seriously is becoming more widespread. And &ldquo;Do you believe in the power of witchcraft, voodoo or the occult?&rdquo; which thinking back to the <a href="http://www.horrorseek.com/home/halloween/wolfstone/Hatred/brnhry_BurningHarryPotter.html" target="_blank">furore</a> that arose around the <a href="http://en.wikipedia.org/wiki/Harry_Potter_%28character%29">Harry Potter</a> books is a view that is held by a disturbing number of people<a href="#Back3" name="3">3</a>.</p>

<p>Quite obviously simply answering affirmatively to these questions does not place you in the psychotic camp, it is the aggregate of these answers that matter as well as further variables that relate to these answers such as how these thoughts and experiences make you feel. The experience of seeing or hearing a loved one that has died is quite widespread but I don&#39;t think general conclusions about the sanity of the general population can be reached using this information.</p>

<p>The second questionnaire was a variation on the <a href="http://schizophreniabulletin.oxfordjournals.org/cgi/reprint/25/3/553.pdf" target="_blank">Peters et al. Delusions Inventory</a> (PDI, and the good acronyms come to an end). There is significant overlap between the questions asked in the PDI and the CAPE questionnaires, the main difference seems to be how each question is followed up. The CAPE approach simply asks how distressed the respondent feels if they answered affirmatively to a question (with a 4 point scale, Not distressed to Very distressed) while the PDI covers this aspect as well as asking how much the respondent thinks about it and how much they believe it is true.</p>

<p>Now how do the categories that I mentioned above (low, middle and high) relate to the results of the questionnaires? Rather than attempt to paraphrase the study I&#39;ll just quote that bit:</p>

<blockquote>
<p>&ldquo;The &quot;low level symptoms group&quot; included women with no or few experiences of psychotic-like symptoms (&le;3 &quot;sometimes&quot; and no &quot;almost always&quot; and &quot;often&quot; answers to any of the questions). The &quot;high level symptoms group&quot; included women with frequent experiences of psychotic-like symptoms (&ge;3 &quot;almost always&quot; or &quot;often&quot; answers). The &quot;middle level symptoms group&quot; was defined as participants not included in the low level or high level groups.&rdquo;</p>
</blockquote>

<p>Fairly simple, not as nuanced as I expect an in-depth psychological evaluation might be but that&#39;s the limitation of performing a large scale study.</p>

<p>Finally, what were the results of the study regarding fish consumption and symptoms of psychosis? Interestingly the authors did not see a simple relationship between the two variables, there was no clear protective effect with increasing intake of fatty fish (those with high levels of PUFAs). Instead there was an optimal intake that was correlated with low (or no) symptoms, higher intake actually correlated with increased symptoms. The authors are unsure what could account for this effect stating:</p>

<blockquote>
<p>&ldquo;This puzzling finding may be due to unknown or known unhealthy constituents of fatty fish. For instance, environmental pollutants such as <a href="http://en.wikipedia.org/wiki/Polychlorinated_biphenyl">polychlorinated biphenyls</a> (PCB) and dioxins are known to accumulate in fatty fish. Another possible explanation may be that the frequent intake of fish and PUFA may be advantageous in lower doses but disadvantageous in higher doses.&rdquo;</p>
</blockquote>

<p>The authors also caution that the study was not geared to determine a causal relationship between the variables merely how these were correlated<a href="#Back4" name="4">4</a>. Another interesting finding was that high levels of psychotic symptoms are also correlated with women who are both overweight and are smokers (and also for some reason migration to Sweden<a href="#Back5" name="5">5</a>).</p>

<p>Bottom line? Hard to say really, the results of this study are indicative but not definitive. The take home message in my book looks to be that it is a balanced diet which is most beneficial, including fish in your meals between 1 and 3 times a week or so. For those of us who aren&#39;t keen on fish, supplements might be the answer but that&#39;s really another question.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><a href="#1" name="Back1">1</a>. Indeed they may not even consider themselves to be suffering from anything untoward.</p>

<p><a href="#2" name="Back2">2</a>. You might recognize Omega-3 as representative of this group.</p>

<p><a href="#3" name="Back3">3</a>. For me that number is 5.</p>

<p><a href="#4" name="Back4">4</a>. Remembering the adage: Correlation does not equal Causation.</p>

<p><a href="#5" name="Back5">5</a>. So should the tourism board adopt the slogan &ldquo;Sweden: You don&#39;t have to be crazy to move here, but it helps&rdquo;? Too insensitive?</p>

<p><!--WISESTAMP_SIG_END-->--<br />
Hedelin M, Lof M, Olsson M, Lewander T, Nilsson B, Hultman CM, &amp; Weiderpass E (2010). Dietary intake of fish, omega-3, omega-6 polyunsaturated fatty acids and vitamin D and the prevalence of psychotic-like symptoms in a cohort of 33 000 women from the general population. BMC psychiatry, 10 (1) PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20504323">20504323</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://blog.beliefnet.com/beyondblue/2010/06/are-you-spiritual-or-psychotic.html">Are You Spiritual Or Psychotic? The Fine Line Between Prophetic and Crazy</a> (beliefnet.com)</li>
	<li><a href="http://www.physorg.com/news191850989.html">First symptoms of psychosis evident in 12-year-olds</a> (physorg.com)</li>
</ul>
', '2019-02-06', 422, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Can the Order You View Things Affect Your Opinion of Them? Or, To Make a Good Impression Ensure You''re Introduced First ', '<blockquote>
<p>&quot;First the worst, Second the best, Third the golden eagle.&quot;</p>
</blockquote>

<p>The above is a rhyme from my youth<a href="#Back1" name="1">1</a>, the implication being that you really don&#39;t want to be first in anything. First is the worst after all. Obviously this is not true in the world beyond the playground, firsts in every field are celebrated, sometimes in great disproportion to their inherent worth<a href="#Back2" name="2">2</a>. How then is the position of first regarded by the human psyche? Do we give it more weight in other areas of our life? Can the order that we see things influence how we think about them, can we be manipulated into seeing things a certain way just by controlling what we see first?</p>

<p>Perhaps unsurprisingly the answer is yes. I should qualify that, it should be surprising that the order in which we observe something affects our decisions about it. We are after all rational beings aren&#39;t we? We take in information, process it and come to conclusions based on the merits of what we have considered. Well, sort of. We are in actuality a mess of conflicting thoughts and desires, we all come with our own preconceptions and biases that can distort how we view the world without our really being aware of them. Is it really so surprising that we&#39;ve found another one? Not to me, though this one is weird.</p>

<p>So what am I talking about really?<a href="#Back3" name="3">3</a> <a href="http://www.columbia.edu/~dc2534/FIB.Carney.Banaji.4-3-07.pdf" target="_blank">Work performed</a> by Dana Carney<a href="#Back4" name="4">4</a> and Mahzarin Banaji looked at how the order we are exposed to things can affect the way we think about them and lead us to conclusions that otherwise we might not be justified in making. In a series of six experiments the question of how much influence the order we are exposed to stimuli affects our actions is asked. The experiments progressed from relatively cautious beginnings showing an implicit bias (using the Implicit Association Test - IAT as discussed in my previous post <a href="http://scepticon.wordpress.com/2009/06/29/project-implicit/" target="_blank">here</a>) without explicit bias (self reporting) or real world consequences to actual choice biases in the real world.</p>

<p>In the first two experiments pairs of images (horses or people) were presented and then were rated for preference by the participants using either the IAT or an explicit seven point scale (eg &ldquo;I strongly prefer x to y&rdquo;). The implicit test showed a bias towards the first picture seen while the explicit measure did not.</p>

<p>Progressing from this the second two experiments attempted to determine if this implicit bias could be revealed in actions in the real world. Both used a pair of small consumables, either chewing gum or a lollypop, that would be placed on a table sequentially. The participant would then choose which item they preferred and would get to keep the item. In the first experiment subjects had to choose as quickly as they could (&ldquo;within one second or so.&rdquo;), this mimicked the IAT. In this condition 75% of participants chose the item placed on the table first.</p>

<p>The second experiment in this pair took place using commuters in a train station. Once again subjects were asked to choose one of two items which they were shown sequentially, this time however they were asked to either choose quickly or to take their time (mimicking the explicit test). In this set up 62% of the subject chose the first item in the time-pressured condition while no preference was seen if they could take their time.</p>

<p>Finally the study authors decided to test whether the preference for items seen first was due to an impulse to regard these items more positively than later seen items or if it was because the condition enhanced previously held beliefs about the object. In other words perhaps the first gum you see isn&#39;t taken because your are made to think it&#39;s better than the second gum but because you already have a positive view of gum and have this positive view reinforced by seeing one type first and so chose it for that reason.</p>

<p>To try and tease out these factors the authors used pictures of convicts in the final two experiments in order to use a stimulus that would naturally be seen negatively. In this set up participants were asked to indicate which criminal was a better candidate for parole. If first seem items have a more positive aspect imparted to them by virtue of being first then the first convict would be chosen. If on the other hand a baseline feeling about objects is enhanced then the second convict would be chosen. Once again however the IAT showed a preference for the first convict seen, labelling them better suited for parole. Also again, no preference was seen in the explicit measures.</p>

<p>What does this tell us about the general human capacity for decision making. Well superficially it&#39;s good news. Given the time we will deliberate and make decisions based on the evidence available. If however we are working under time pressures then our implicit biases may come to the fore. There are however certain decisions that the authors of this study point out are known to be subject to implicit bias, such as consumer brand preference or even treatment preferences of doctors for specific patients.</p>

<p>We should be wary then of this new bias that has been thrown into the mix. Our heads are already full of biases that we may or may not be aware of, only by attempting to identify them do we stand much hope in correcting or tempering their influence on us.</p>

<p>So next time you need to make a snap decision, try to slow down and think it through. You may make a better choice<a href="#Back5" name="5">5</a>.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END--><a href="#1" name="Back1">1</a>. There seem to be versions of this around the world but the one above is most common in New Zealand as evidenced by <a href="http://www.victoria.ac.nz/lals/research/grinze/publications/The_Children.pdf" target="_blank">this report</a>.</p>

<p><a href="#2" name="Back2">2</a>. Looking at you gold medal in <a href="http://en.wikipedia.org/wiki/Cycling_at_the_Summer_Olympics#BMX.2C_Men" target="_blank">Olympic BMX</a>.</p>

<p><a href="#3" name="Back3">3</a>. And how many rhetorical questions can I fit into one article? (does this one count? [or that one? {ok I&#39;ll stop now}])</p>

<p><a href="#4" name="Back4">4</a>. Of the &quot;<a href="http://scepticon.wordpress.com/2010/05/21/is-your-boss-a-better-liar-than-you-probably-yes/" target="_blank">Is Your Boss A Better Liar Than You? Probably, Yes</a>&quot; post.</p>

<p><a href="#5" name="Back5">5</a>. On the other hand you may not want to think <a href="http://www.hss.caltech.edu/~steve/rightchoice.pdf" target="_blank">complex decisions through too much</a>.</p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/04/30/i-know-what-i-mean-but-only-see-how-you-act/">I Know What I Mean but Only See How You Act</a> (scepticon.wordpress.com)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=5005">How do religious-based hospitals affect physician behavior?</a> (sciencebasedmedicine.org)</li>
	<li><a href="http://www.kk.org/quantifiedself/2010/05/a-visual-guide-to-cognitive-bi.php">A Visual Guide to Cognitive Bias</a> (kk.org)</li>
	<li><a href="http://www.sciencebasedmedicine.org/?p=4780">Brain-Training Products Useless in Study</a> (sciencebasedmedicine.org)</li>
</ul>
', '2019-02-06', 166, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Robin', 'Are Vegans and Vegetarians More Empathic Than Omnivores?', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>Humans are naturally <a href="http://en.wikipedia.org/wiki/Omnivore">omnivorous</a>, our digestive system is adapted to cope with both animal and vegetable matter. Obviously we can&#39;t eat everything, we wouldn&#39;t get as much sustenance out of a mouthful of grass as a cow does. <a href="http://en.wikipedia.org/wiki/Termite">Termites</a> can deal with the high <a href="http://en.wikipedia.org/wiki/Cellulose">cellulose</a> content of wood and derive energy from that source while we would starve on such a diet. On the whole however we can manage to survive on a wide range of foodstuffs<a href="#Back1" name="1">1</a>,<a href="#Back2" name="2">2</a> and can be quite happy doing so.</p>

<p>So far, so good. What on Earth has this got to do with empathy? Well, a <a href="http://www.plosone.org/article/info:doi/10.1371/journal.pone.0010847" target="_blank">recent study</a> published in <a href="http://www.plos.org">PLoS</a> One using <a href="http://en.wikipedia.org/wiki/Functional_magnetic_resonance_imaging">fMRI</a> looked at the brains of subjects while they viewed negative images of either humans or other animals<a href="#Back3" name="3">3</a> here is the reasoning from the study authors.</p>

<blockquote>
<p>&quot;In this study, we postulated that the neural representation of conditions of abuse and suffering might be different among subjects who made different feeding choice due to ethical reasons, and thus result in the engagement of different components of the brain networks associated with empathy and social cognition.&quot;</p>
</blockquote>

<p>The thinking here is that individuals who choose a vegetarian or vegan lifestyle based on ethical considerations<a href="#Back4" name="4">4</a> have a higher degree of empathy towards the suffering of both humans and animals compared to individuals who continue with an omnivorous diet. It might be of use to first define what empathy means in the context of this work, and why paraphrase when the authors have put it in their own words so much more concisely<a href="#Back5" name="5">5</a>:</p>

<blockquote>
<p>&quot;Empathy toward another person, which can be defined as the ability to share the other person&#39;s feeling in an embodied manner, has been related to recruitment of a network mostly including the somatosensory and insular cortices, limbic regions and the <a href="http://en.wikipedia.org/wiki/Anterior_cingulate_cortex">anterior cingulate cortex</a> (ACC).&quot;</p>
</blockquote>

<p>The study did find increased activation of those brain areas that correlate with empathy in vegans and vegetarians. In fact it was also found that vegans and vegetarians had much higher activation of these areas while watching images of animal suffering than human suffering implying greater empathy for animals than humans. As noted in the study:</p>

<blockquote>
<p>&quot;Remarkably, vegetarians and vegans have an higher engagement of empathy related areas while observing negative scenes regarding animals rather than humans, with the additional recruitment of the mPFC, PCC, and some visual areas. ACC has been associated with alert states, self awareness and pain processing, whereas mPFC and PCC activations are frequently observed in conditions involving representation of the self and self values ...the selective response of vegans to animals in the ACC (with reduced amygdala responses) might reflect a greater attribution of self-relevance and a greater recruitment of emotional regulation mechanisms, when viewing negative states of non-human beings... By contrast, omnivores, showed greater responses to human negative valence scenes in the ACC (together with reduced amygdala activation), suggesting that self-relevance and emotion control mechanisms were more specifically engaged by viewing suffering conspecifics than suffering animal beings.&quot;</p>
</blockquote>

<p>This suggests to me that vegans are actually incorporating their greater empathy toward non-human animals into their personal identity (self-relevance). Possibly elevating it to a defining characteristic of their personality. This makes sense to me as in today&#39;s world, while vegetarianism is becoming more common, the social pressure against a vegan lifestyle in most places would still be considerable. To retain such a choice in the face of this pressure would require a great deal of commitment.</p>

<p>This is pure speculation on my part but in the context of omnivores showing greater self-relevance activation when viewing negative scenes involving humans it appears to be a reasonable conjecture. This being a correlational study however there is no way to determine the causation of such responses. Do naturally highly empathic individuals simply gravitate to vegetarian or vegan lifestyles or does choosing the diet cause greater empathy, possibly as a means of rationalising a choice which may have originally been made for other reasons<a href="#Back6" name="6">6</a>.</p>

<p>Either way, this study implies that we omnivores are cold hearted in comparison to our meatless compatriots. How this might impact other decisions in our lives I&#39;m not sure, perhaps an interesting follow-up would be if vegetarians and vegans give more time/money to charities or perceived worthy causes outside of the animal welfare realm. Actually this reminds me of a woman I occasionally see holding up a sign outside of the supermarket. Upon the sign is written &quot;Compassion begins with dinner&quot;<a href="#Back7" name="7">7</a>. Perhaps she&#39;s onto something.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END--><a href="#1" name="Back1">1</a>. This is obviously a tautology, sorry about that.</p>

<p><a href="#2" name="Back2">2</a>. Can&#39;t be out done by my blogging colleagues, no mere footnotes for me. <em>Linked</em> footnotes is where it&#39;s at.</p>

<p><a href="#3" name="Back3">3</a>. I get quite uncomfortable making the arbitrary distinction between humans and animals.</p>

<p><a href="#4" name="Back4">4</a>. As opposed to the &quot;Meat is icky&quot; reason.</p>

<p><a href="#5" name="Back5">5</a>. Who says there&#39;s no advantage to laziness?</p>

<p><a href="#6" name="Back6">6</a>. See note 4.</p>

<p><a href="#7" name="Back7">7</a>. Or some such thing, who am I, Rainman? I can&#39;t remember everything.</p>

<p>--</p>

<p>Filippi, M., Riccitelli, G., Falini, A., Di Salle, F., Vuilleumier, P., Comi, G., &amp; Rocca, M. (2010). The Brain Functional Networks Associated to Human and Animal Suffering Differ among Omnivores, Vegetarians and Vegans PLoS ONE, 5 (5) DOI: <a href="http://dx.doi.org/10.1371/journal.pone.0010847">10.1371/journal.pone.0010847</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://r.zemanta.com/?u=http%3A//www.guardian.co.uk/environment/blog/2010/may/24/vegetarianism-pets-national-vegetarian-week-cats-dogs&amp;a=18469327&amp;rid=d52da370-3a2f-4bf2-8ad7-417317048c73&amp;e=65a532a3490d76d4cd87dbaf39148164">The ethics of veggie cats and dogs</a> (guardian.co.uk)</li>
	<li><a href="http://www.theness.com/neurologicablog/?p=1939">I Don&#39;t Feel Your Pain</a> (theness.com)</li>
</ul>
', '2019-02-06', 188, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Batman', 'Does Your Environment Determine Your Behaviour?', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>Last week I <a href="http://scepticon.wordpress.com/2010/06/04/alcohol-effects-on-teenage-brains-and-correlations-between-availability-and-violence/" target="_blank">posted</a> about the effects of alcohol availability on the amount of violence in an area and mentioned that disorder in the area might also contribute to this. I thought this needed expanding upon, are we really so shallow as a species that we allow the environment that we may be in only briefly to dictate how we behave?</p>

<p>This approach to behaviour is referred to as the <a href="http://en.wikipedia.org/wiki/Fixing_Broken_Windows">Broken Windows Theory</a> and states that signs of disorderly behaviour will beget more disorderly behaviour. These signs might be graffiti, littering or the observation that legitimate signage requesting a particular behaviour is being ignored. We are a species that learns by watching others, it is reasonable to conclude that we don&#39;t only pick up good and productive behaviours in this way but potentially antisocial or destructive behaviours as well.</p>

<p>Acting on this theory cities around the world have embarked on projects to clean up urban areas in order to reduce the instance of petty crime. Trouble was that there was no conclusive evidence to back this theory up. A <a href="http://www.ppsw.rug.nl/~lindenb/documents/articles/2008_Keizer_Lindenberg_Steg_The%20spreading_of_disorder_Science_Express.pdf" target="_blank">paper</a> that goes some way towards rectifying this was published in late 2008. It looked at the effects of manipulating the environment on the propensity for people to break either social norms, police enforced ordinances or requests by companies.</p>

<p>The paper hypothesised that people were not merely copying behaviours but that indications of social or legal norms not being followed in an area allowed other personal goals to override our normal instincts to act in socially acceptable ways. To investigate this the researchers set up conditions to see if the breaking of one rule lead to the breaking of a different rule e.g. graffiti leading to littering or litter leading to stealing.</p>

<p>In all 6 studies were performed, in the first an ally where bicycles were commonly parked was set up to have either clean or graffiti covered walls. The effect of this environment on littering was then observed by attaching a useless flyer onto the bicycle handlebars and counting instances of littering compared to those that took the flyer with them (there were no bins in the ally). In the clean wall condition 33% of those collecting bikes littered, compared with 69% in the graffiti condition.</p>

<p>In the second study the conditions were whether an injunction to not lock bicycles to a fence would affect whether or not people would take a police sanctioned detour. Two signs were placed along the fence of a car-park, the first prohibited locking bicycles to the fence while the second advised that the closest entrance to the car-park was closed and that a second entrance 200 metres further along had to be used. The entrance was closed with a temporary fence but a gap left that could be used. Bicycles were then conspicuously left either locked or not locked to the fence. When the bicycles were not locked to the fence 73% of people entering the car-park took the detour compared with only 18% when the bikes were seen violating the sign.</p>

<p>I think we can see a pattern emerging. I&#39;m going to skip the third and forth studies that are variations on the littering theme and to go straight for the fifth and sixth. These two studies concerned stealing and had very similar set-ups, an envelope visibly containing a &euro;5 note was set-up hanging out of a mail collection box. The box was then either clean and the area around it tidy or the box was graffitied or there was litter around the box. Taking or opening the envelope were both counted as stealing. The clean and tidy condition resulted in 13% of passers-by stealing the envelope, whereas the graffiti and litter conditions resulted in 27% and 25% of people stealing respectively.</p>

<p>What does this tell us about human behaviour? First off I see it as very positive, social conventions are a powerful factor for encouraging acceptable behaviour, on balance people prefer to act responsibly in the absence of cues that suggest irresponsible behaviour is tolerated. This suggests that policing general behaviour may be achieved in a subtle fashion by re-enforcing social conventions rather than with actual shows of force and punishment. That said the authors of the paper take a slightly more pessimistic view of areas that have already succumbed to spreading disorder, concluding with this thought:</p>

<blockquote>
<p>&quot;.. once disorder has spread, merely fixing the broken windows or removing the graffiti may not be sufficient anymore. An effective intervention should now address the goal to act appropriately on all fronts.&quot;</p>
</blockquote>

<p>The trick then is prevention rather than clean-up, at least in areas where behaviours have become entrenched. I think this is good news over all.</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END-->--<br />
Keizer, K., Lindenberg, S., &amp; Steg, L. (2008). The Spreading of Disorder Science, 322 (5908), 1681-1685 DOI: <a href="http://dx.doi.org/10.1126/science.1161405">10.1126/science.1161405</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://scepticon.wordpress.com/2010/04/16/can-our-fantasy-life-affect-our-perceptions-of-real-life/">Can Our Fantasy Life Affect Our Perceptions of Real Life?</a> (scepticon.wordpress.com)</li>
	<li><a href="http://scepticon.wordpress.com/2010/03/02/intelligence-monogamy-and-journalistic-licence/">Intelligence, Monogamy and Journalistic Licence</a> (scepticon.wordpress.com)</li>
	<li><a href="http://r.zemanta.com/?u=http%3A//www.guardian.co.uk/society/2010/jun/09/police-head-cameras-antisocial-behaviour-cornwall&amp;a=19218382&amp;rid=ed55c23d-8f93-4627-a365-f06afb9a3763&amp;e=6717a2bd2d0c7eb3b629b19023c132f2">Can &#39;headcams&#39; curb bad behaviour?</a> (guardian.co.uk)</li>
	<li><a href="http://r.zemanta.com/?u=http%3A//www.telegraph.co.uk/news/uknews/crime/7597529/Police-refused-to-log-graffiti-attack-as-crime-because-paint-could-be-washed-off.html&amp;a=16599199&amp;rid=ed55c23d-8f93-4627-a365-f06afb9a3763&amp;e=3757439ca1b533d04b92db573b0233ab">Police &#39;refused to log graffiti attack as crime because paint could be washed off&#39;</a> (telegraph.co.uk)</li>
</ul>
', '2019-02-06', 144, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('next', 'Alcohol: Effects on Teenage Brains and Correlations Between Availability and Violence', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a></p>

<p>I&#39;ve <a href="http://scepticon.wordpress.com/2009/11/13/rats-bad-decisions-and-jelloshots/" target="_blank">written previously</a> about the effects of alcohol on decision making. Today I&#39;ll be looking at two further aspects of alcohol consumption and how it affects society. First up is a study that examines the cognitive effects of moderate to heavy drinking during <a href="http://en.wikipedia.org/wiki/Adolescence">adolescence</a>, with different effects on males versus females. Secondly I&#39;d like to cover an interesting paper that relates the amount of violence in a community to the number alcohol outlets in the community.</p>

<p>There is an interesting contrast between the two papers, one focusing on the effects of alcohol on the individual and possible long term effects and the other on the wider community. Like tobacco the use of alcohol is coming under increased scrutiny in our society and this is a good thing. The difficult part in both processes is to ensure our actions are guided by evidence rather than knee jerk emotion. Smokers complain that they are unfairly victimised when alcohol causes so much harm, this may be true but it is irrelevant to the discussion of the harms of tobacco use. The same is true from the alcohol side of the debate.</p>

<p>Each substance must be approached and dealt with on it&#39;s own merits and derailing the discussion to decry the abuse of one or the other is unhelpful. I am not an expert on public policy and do not advocate any particular solution to either of these issues, I don&#39;t know what the best solution is but it is tiresome to see the same diversionary tactics arise time and again in these sorts of debates. With that, lets move on to the science.</p>

<p>The first paper, &quot;<a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2802333/" target="_blank">Initiating moderate to heavy alcohol use predicts changes in neuropsychological functioning for adolescent girls and boys</a>&quot;, follows a cohort of 76 adolescents starting at ages 12-14 years. The participants of the study were chosen to limit the amount of exposure at baseline to addictive/mind altering substances. These individuals were then followed over approximately 3 years and given a battery of tests and questionnaires to determine their drinking habits and cognitive abilities.</p>

<p>As we might expect heavier drinking patterns in the adolescents predicted poorer test results. The interesting result is the differences in the effects of the alcohol in male versus female subjects.</p>

<p>[caption id=&quot;attachment_1918&quot; align=&quot;alignright&quot; width=&quot;300&quot;]<a href="http://www.archimedes-lab.org/Rey_test.html"><img alt="" src="http://scepticon.files.wordpress.com/2010/06/complexfiguretest.jpeg?w=300" style="height:237px; width:300px" /></a> Take the test Here[/caption]</p>

<p>Female subjects that consumed higher amounts of alcohol in the preceding 12 months performed worse on tests of visuospatial functioning than the control group. One of the ways this was measured was using something called the <a href="http://en.wikipedia.org/wiki/Rey-Osterrieth_Complex_Figure">Rey-Osterrieth Complex Figure test</a> (see pic), this entails the subject copying the figure, and then drawing it again some interval of time later (in this case 30 mins) from memory.</p>

<p>The number of drinks required to see this effect appeared to be 12 or more a month (in the previous 3 months), with the larger doses leading to a more pronounced detriment. Of course this was a trend effect in a population, on average heavier drinking females did worse in these tests but any one female was not guaranteed to see a reduction in performance.</p>

<p>Males on the other hand tended to do worse in the attention tests. Going by this measure I&#39;m sure wives the world over would swear their husbands had been habitual binge drinkers throughout adolescence. The test of sustained attention in this case was done by the <a href="http://www.psychpress.com.au/Psychometric/product-page.asp?ProductID=1793" target="_blank">Digit Vigilance Test</a>, this simple test consists of rows of single digit numbers printed in either red or blue (single colour per page). Subjects must find either 6s or 9s on each page.</p>

<p>By timing the task and counting the errors committed a measure of sustained attention can be determined and compared to the control group.</p>

<p>A drawback of this work is that the number of subject was limited, the entire study had a total of 76 adolescents, of those 29 were female (leaving 47 males). These groups then had to be further bisected to give the drinking and control groups. This study was quite small but is consistent with previous research showing negative effect on the brain for developing individuals. The bottom line of research like this is not difficult to get to, there are detrimental effects to be had by allowing young people to indulging moderate to heavy alcohol consumption. What we do with this information is up to us.</p>

<p>The second paper was <a href="http://www.eurekalert.org/pub_releases/2010-02/iu-mas021110.php" target="_blank">presented</a> by <a href="http://www.indiana.edu/~crimjust/faculty_pridemore.php" target="_blank">William Pridemore</a> and <a href="http://www.indiana.edu/~geog/people/grubesic.shtml" target="_blank">Tony Grubesic</a> at the annual meeting of the American Association for the Advancement of Science in San Diego earlier this year. With the engaging title of &quot;<a href="http://media.eurekalert.org/aaasnewsroom/MCM/FIL_000000000017/Alcohol%20Outlets.doc" target="_blank">Alcohol outlets and community levels of interpersonal violence: Spatial density, type of outlet, and seriousness of assault</a>&quot; the paper examines the correlation between the number of places where alcohol is available in a community and the amount of violence the community experiences.</p>

<p>I think we can all guess the outcome but it&#39;s nice to have actual data to back up our intuitions. The study was quite detailed in it&#39;s approach with the violence broken down by simple assault and <a href="http://en.wikipedia.org/wiki/Assault">aggravated assault</a> and the type of alcohol outlets subdivided into bars, restaurants and so-called &quot;off-premise&quot; outlets where alcohol is sold but not consumed on site, such as supermarkets and liquor stores.</p>

<p>It comes as no real surprise that a higher density of alcohol outlets was correlated with a higher level of violence in the area. A result that I did not expect though was the significant effect the &quot;off=premise&quot; alcohol outlets appear to have on the levels of violence in the area. The study estimated such sites contribute between 25%-30% of the violence (depending on the category)&nbsp; of an area. This compares to approximately 10% each for bars and restaurants.</p>

<p>The reason for this difference is suggested to be that these areas can act as impromptu gathering places where it is perceived that the normal rules of society do not apply, especially if the area is <a href="http://www.ppsw.rug.nl/~lindenb/documents/articles/2008_Keizer_Lindenberg_Steg_The%20spreading_of_disorder_Science_Express.pdf" target="_blank">unkempt in appearance</a>. In such areas individuals may have an altered perception of the moral expectations and this coupled with the disinhibiting effects of alcohol could lead to the greater propensity for violence.</p>

<p>Once again the information may merely inform our decisions not make them for us. Is the best approach simply to limit the number of alcohol outlets, or ban then from some areas altogether (or completely?). Or should we use this information to put measures into place to reduce violence while retaining access to alcohol, perhaps by creating environments around outlets that are less conducive to violent confrontations. Maybe some nice waterfalls and soothing music would help, I&#39;m sure I have an <a href="http://www.enya.com/" target="_blank">Enya </a>CD around here somewhere I could donate to the cause (or possibly <a href="http://www.thecorrswebsite.com/index2.htm" target="_blank">The Corrs</a>).</p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END-->--</p>

<p>Squeglia, L., Spadoni, A., Infante, M., Myers, M., &amp; Tapert, S. (2009). Initiating moderate to heavy alcohol use predicts changes in neuropsychological functioning for adolescent girls and boys. Psychology of Addictive Behaviors, 23 (4), 715-722 DOI: <a href="http://dx.doi.org/10.1037/a0016516">10.1037/a0016516</a></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://www.newscientist.com/article/dn18999-bingeing-rots-teen-brain.html?DCMP=OTC-rss&amp;nsref=health">Bingeing rots teen brain</a> (newscientist.com)</li>
	<li><a href="http://news.bbc.co.uk/go/rss/-/2/hi/science/nature/8527806.stm">City area link to alcohol trouble</a> (news.bbc.co.uk)</li>
	<li><a href="http://news.bbc.co.uk/go/rss/-/2/hi/wales/10217751.stm">Parents in child alcohol warning</a> (news.bbc.co.uk)</li>
</ul>
', '2019-02-11', 266, 'Photos/Banner_1.jpg');
INSERT INTO group_project_article (user_id, title, content, publish_time, votes, banner) VALUES ('Cyborg', 'BioFuel Cell Batteries May Power New Future Implanted Devices', '<p><a href="http://www.researchblogging.org"><img alt="ResearchBlogging.org" src="http://www.researchblogging.org/public/citation_icons/rb2_large_white.png" /></a>When I think about the future I sometimes indulge in fantasies that include &quot;bionic&quot; type implants. Not so much artificial muscles that will enhance strength (check <a href="http://www.sciencedaily.com/releases/2010/01/100118091049.htm" target="_blank">this out</a>) but devices that will expand our mental capabilities. Implants that give us greater memory, faster thought processes, the ability to download skills and knowledge directly into our brains.<br />
Perhaps I read <a href="http://en.wikipedia.org/wiki/Commonwealth_Saga" target="_blank">too much science fiction</a>.</p>

<p>The trouble with this utopian view of the future is a practical one, where will these devices receive their power from? If I start forgetting things because I haven&#39;t plugged in a new 9volt, I won&#39;t be happy. The ideal solution would be some sort of battery that can be inserted into the body and generate energy from the food I eat, just like the rest of my organs.</p>

<p>Enter: The BioFuel cell. Fuel cells have been around for a few decades and while most people have no dealings with them their essential mechanism is easily understood. Basically a chemical reaction is allowed to proceed under very controlled conditions to generate a flow of electrons (ie an electric current). The usual example given is reacting Hydrogen (the fuel) with Oxygen to generate water and electricity, but really almost any electron donor/acceptor pair will do.</p>

<p>Biofuel cells replace the electron donor (eg Hydrogen as above) with a biological molecule, glucose. These Glucose BioFuel Cells (GBFCs) could then in theory utilise glucose dissolved the blood as a fuel to generate electricity and power implanted devices. The fanciful science fiction devices I dream about above may not arrive on the scene any time soon but there are medical devices and synthetic organs that would benefit from such a power source now.</p>

<p>The device that immediately springs to mind is a pacemaker but the possibilities are much wider, ranging from the <a href="http://www.americanmedicalsystems.com/mens_products_detail_objectname_male_AMS_800.html" target="_blank">artificial urinary sphincter</a> that recipients of <a href="http://www.webmd.com/prostate-cancer/radical-prostatectomy" target="_blank">Radical Prostatectomy</a> surgery depend on to artificial kidneys which to be portable must currently be <a href="http://en.wikipedia.org/wiki/Wearable_artificial_kidney" target="_blank">wearable</a> because of (among <em>many</em> other reasons) the inability to effectively supply it with power inside the body.</p>

<p>Existing GBFCs have a draw back that the electrodes are inhibited (work less efficiently) by chloride or urate ions, both of which are present in your blood, or require low (acidic) pH to work whereas your body likes to be around neutral pH. This makes them ineffective in a real biological environment. Luckily a recent paper in Plos ONE, &quot;<a href="http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0010476" target="_blank">A Glucose BioFuel Cell Implanted in Rats</a>&quot;, details an alternative type of fuel cell that overcomes these limitations and demonstrates it by implanting it inside a rat.</p>

<p>The GBFC produced a specific power of 24.4 &micro;W mL&minus;1 which, to put that in perspective, could power two <a href="http://en.wikipedia.org/wiki/Artificial_pacemaker">pacemakers</a> (just in case <a href="http://en.wikipedia.org/wiki/Doctor_%28Doctor_Who%29" target="_blank">The Doctor</a> gets into trouble). The mL&minus;1 part refers to the volume of the fuel cell, this really means that the power output is related to the size of the fuel cell, just like regular batteries. The volume of this cell appears to be little more than a quarter of a millilitre (0.266mL, two electrodes of 0.133mL each), think about how much volume a normal 6 sided die takes up, imagine one quarter of that and you&#39;ll be in the right ball park.</p>

<p>Inside the fuel cell electrodes are enzymes that react the glucose with dissolved oxygen also in the blood to produce an electric current. The glucose and oxygen required for the reaction diffuse through a membrane surrounding the electrodes while the waste products diffuse back out into the bloodstream to be taken care of by the body. In this way the fuel is constantly being replenished and so long as the enzymes retain their activity the fuel cell will continue to function and continuously produce energy. The time scales measured in this study were only a few months but experiments by others suggests that the enzymes will stay active in a device such as this for at least a year and possibly more.</p>

<p>The authors of the study consider that a scaled up version of the device would be able to power medical implants with much greater power requirements than a pacemaker, such as the artificial sphincter mentioned above that they calculate would need almost 10 times the amount of power of a pacemaker. Seems like the limit at the moment is how much room you have to spare inside your body to house the fuel cell. Early pacemaker batteries took up about 90mLs worth of volume, that&#39;s roughly a quarter the size of a drink can. It is mentioned that an animal such as a pig could accommodate a fuel cell 133mL in size but it is not made clear if this is an experiment that will actually occur. RoboPig.</p>

<p>All this is pretty exciting and with that sort of potential in a first generation fuel cell, I&#39;m betting I can get my memory expansion before I start going senile. Now I just have to figure out what to do in the mean time.</p>

<p>-</p>

<p>Cinquin P, Gondran C, Giroud F, Mazabrard S, Pellissier A, Boucher F, Alcaraz JP, Gorgy K, Lenouvel F, Math&eacute; S, Porcu P, &amp; Cosnier S (2010). A glucose biofuel cell implanted in rats. PloS one, 5 (5) PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20454563">20454563</a></p>

<p><!--WISESTAMP_SIG_START--></p>

<p><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"><img alt="OpenLab2010" src="http://scienceblogs.com/clock/Open_Lab_2010%20button.png" style="width:55,height=35" /></a><a href="http://openlab.wufoo.com/forms/submission-form/" target="_blank"> Submit To Open Laboratory 2010</a>(<a href="../2010/05/18/open-laboratory-project/" target="_blank">What&#39;s This?</a>)</p>

<p><!--WISESTAMP_SIG_END--></p>

<p>Related articles by Zemanta</p>

<ul>
	<li><a href="http://singularityhub.com/2010/05/24/new-biofuel-cell-uses-glucose-in-the-body-to-produce-electricity-for-cyborgs/">New BioFuel Cell Uses Glucose in the Body to Produce Electricity for Cyborgs</a> (singularityhub.com)</li>
	<li><a href="http://www.neatorama.com/2010/05/19/fuel-cell-for-pacemaker-is-powered-directly-from-the-human-body/">Fuel Cell for Pacemaker is Powered Directly from the Human Body</a> (neatorama.com)</li>
	<li><a href="http://green.venturebeat.com/2010/05/18/glucose-biofuel-cell-could-revolutionize-medical-technology/">Glucose biofuel cell could revolutionize medical technology</a> (green.venturebeat.com)</li>
	<li><a href="http://io9.com/5543590/glucose-biofuel-cells-could-provide-sustainable-power-for-human-implants">Glucose biofuel cells could provide sustainable power for human implants [Implants]</a> (io9.com)</li>
</ul>
', '2019-02-06', 111, 'Photos/Banner_1.jpg');
