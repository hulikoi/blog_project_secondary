<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 17/01/2019
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <%-----------------------------------CSS-----------------------------------------------------------%>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <%--CSS external stylesheet--%>
        <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">
        <%--Title (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <%--Date (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <%--Button (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
              integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
              crossorigin="anonymous">
        <%--RSS icon--%>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <style>
            input[type="file"] {
                width: 0.1px;
                height: 0.1px;
                opacity: 0;
                overflow: hidden;
                position: absolute;
                z-index: -1;
            }

            .form-control {
                margin-bottom: 5px;
            }
        </style>

        <%-----------------------------------JavaScript-----------------------------------------------------------%>

        <%--reCAPTCHA script--%>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <script type="text/javascript" src="Resources/jquery-3.3.1.js"></script>

        <script type="text/javascript">
            <%--Changes dropdown image selection--%>

            function switchImage() {
                var image = document.getElementById("imageToSwap");
                var dropd = document.getElementById("dlist");
                var picurl = document.getElementById("picurl");
                image.src = dropd.value;
                picurl.value = dropd.value;
            };

            //Tests if username already exists
            var user_info;
            var retrieveduser_ids = [];

            function load_users(page_url) {
                console.log("pgURL:" + page_url);
                $.ajax({
                    url: page_url,
                    type: 'GET',
                    success: function (msg) {
                        user_info = msg;
                        for (var i = 0; i < user_info.user_ids.length; i++) {
                            retrieveduser_ids.push(user_info.user_ids[i]);
                        }
                    }
                })
            };
            load_users("Usernamesservlet");

            //test username exists on recaptcha complete, disables creat button
            function usernameTest() {
                var inputuser_id = $("#newuser_id").val();
                var btnSubmit = document.getElementById("create");
                var att = document.createAttribute("disabled");
                $(retrieveduser_ids).each(function (index, value) {
                    if (jQuery.inArray(inputuser_id, retrieveduser_ids) != -1) {
                        $('#userExists').text('  **username taken**');
                        btnSubmit.setAttributeNode(att);
                    } else {
                        $('#userExists').text('');
                        btnSubmit.attributes.removeNamedItem("disabled");
                    }
                })
            };


            //Tests if username already exists and notifies user, disables create button
            $(function () {
                $("#newuser_id").change(function () {
                    var inputuser_id = $(this).val();
                    var btnSubmit = document.getElementById("create");
                    var att = document.createAttribute("disabled");
                    $(retrieveduser_ids).each(function (index, value) {
                        if (jQuery.inArray(inputuser_id, retrieveduser_ids) != -1) {
                            $('#userExists').text('  **username taken**');
                            btnSubmit.setAttributeNode(att);
                        } else {
                            $('#userExists').text('');
                            btnSubmit.attributes.removeNamedItem("disabled");
                        }
                    })
                })
            });


            //Tests password length
            $(function () {
                $("#newpassword").change(function () {
                    console.log("pass changed");
                    var newpassword = $(this).val();
                    if (newpassword.length < 6 && newpassword.length > 0) {
                        $('#passwordstrength').text('  **password should be longer**');
                    } else if (newpassword.length > 6) {
                        $('#passwordstrength').text('  WAY TO GO!');
                    } else {
                        $('#passwordstrength').text('');
                    }
                })
            });
        </script>

        <%--Populates Vote Chart--%>
        <core:if test="${user!=null}">
            <%--Chart for article votes--%>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">

                // Load Google Charts
                google.charts.load('current', {'packages': ['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                // Draw the chart and set the chart values
                function drawChart() {
                    <core:choose>
                    <core:when test="${fn:length(articleVotes)>0}">
                    var data = google.visualization.arrayToDataTable([
                        ['Title', 'Votes'],
                        <core:forEach items="${articleVotes}" var="articles">
                        ['${fn:substring(articles.title,0,50)}', ${articles.votes}],
                        </core:forEach>
                    ]);
                    </core:when>
                    <core:otherwise>
                    var data = google.visualization.arrayToDataTable([
                        ['Title', 'Votes'],
                        ['', 0]
                    ]);
                    </core:otherwise>
                    </core:choose>

                    // Optional; add a title and set the width and height of the chart
                    var options = {
                        'title': 'Article Votes',
                        'width': 500,
                        'height': 160,
                        legend: 'none',
                        colors: ['#e4e4e4'],
                        vAxis: {textPosition: 'none'}
                    };

                    // Display the chart inside the <div> element with id="chart"
                    var chart = new google.visualization.BarChart(document.getElementById('chart'));
                    chart.draw(data, options);
                }
            </script>
        </core:if>


        <title>User Profile</title>

    </head>
    <body>
        <jsp:include page="Resources/nav.jsp"/>
        <div>
            <br><br><br><br>
            <div class="container" id="slide-top">
                <div class="container-fluid">
                    <div class="card-deck">
                        <div class="col-sm-12 col-md-6 col-lg-<core:out value="${user eq null ? '6': '4'}"/> card"
                             style="border: hidden">

                            <%--Default pics and dropdown selection of uploaded pics--%>
                            <img style="align-content: center" id="imageToSwap"
                                 src="<core:out value="${user eq null ? 'Photos/DEFAULT_1.jpg': user.url}"/>"
                                 width="225" height="225">
                            <div>
                                <br><br>

                                <h4 style="font-family: 'Julius Sans One', sans-serif;">CHOOSE DEFAULT PICTURE:</h4>

                                <%--Form for uploading user pic--%>
                                <form action="UploadServlet" method="post">
                                    <select id="dlist" onchange="switchImage()" name="dropdown">
                                        <option style="font-family: 'Julius Sans One', sans-serif;"
                                                value="<core:out value="${user eq null ? 'Photos/DEFAULT_1.jpg': user.url}"/>">
                                            SELECT A PROFILE PICTURE
                                        </option>
                                        <core:forEach items="${pics}" var="url">
                                            <option value="<core:out value="${url}"/>"><core:out
                                                    value="${fn:substring(url,7,50)}"/></option>
                                        </core:forEach>
                                    </select>
                                </form>

                                <%--User must have an account to upload their own images--%>
                                <h4 style="font-family: 'Julius Sans One', sans-serif;">OR <core:out
                                        value="${user eq null ? 'create an account to ': ''}"/>UPLOAD YOUR OWN:</h4>
                                <form action="UploadServlet" method="post" enctype="multipart/form-data">
                                    <input style="font-family: 'Julius Sans One', sans-serif;"
                                           class="form-control btn btn-outline-secondary  btn-sm" type="file"
                                           name="userpic"
                                           size="50" id="file"
                                    <core:out
                                            value="${user eq null ? 'disabled': ''}"/>>
                                    <br><br>
                                    <label for="file" class="form-control btn btn-outline-secondary  btn-sm"
                                           style="font-family: 'Julius Sans One', sans-serif;padding-top: 10px">CHOOSE A
                                                                                                                FILE</label>
                                    <br>
                                    <input style="font-family: 'Julius Sans One', sans-serif;" type="submit"
                                           class="form-control btn btn-outline-secondary  btn-sm" value="Upload Pic"
                                    <core:out
                                            value="${user eq null ? 'disabled': ''}"/>>
                                </form>
                            </div>
                            <%--Display Vote Chart--%>
                            <core:if test="${user!=null}">
                                <div style="margin-left: -50px " id="chart"></div>
                            </core:if>
                        </div>
                        <br>

                        <%--User Details--%>
                        <div class="container-fluid col-sm-12 col-md-6 col-lg-<core:out value="${user eq null ? '6': '4'}"/>">
                            <div class="card">
                                <div class="card-header">
                                    <h4 style="text-align: center;background-color: white;font-family: 'Julius Sans One', sans-serif;">
                                        <core:out value="${user eq null ? 'ENTER': 'EDIT'}"/> YOUR DETAILS</h4>
                                </div>
                                <br>

                                <form action="<core:out value="${user eq null ? 'AccountCreation': 'AccountModify'}"/>"
                                      method="POST">
                                    <input type="hidden" id="picurl" name="picurl"
                                           value="<core:out value="${user eq null ? 'Photos/DEFAULT_1.jpg': user.url}"/>">
                                    <core:choose>
                                        <core:when test="${user!=null}">
                                            <input type="text" class="form-control" id="displayuser_id"
                                                   placeholder="USERNAME(THIS CAN NOT BE CHANGED LATER)"
                                                   name="user_id"
                                                   value="${user.user_id}" disabled>
                                            <input type="hidden" id="hiddenuser_id"
                                                   placeholder="USERNAME(THIS CAN NOT BE CHANGED LATER)" name="user_id"
                                                   value="${user.user_id}">
                                        </core:when>
                                        <core:otherwise>
                                            <input type="text" class="form-control" id="newuser_id"
                                                   placeholder="USERNAME(THIS CAN NOT BE CHANGED LATER)"
                                                   name="user_id"
                                                   value="${user.user_id}">
                                            <span id="userExists" style="color:red"></span>
                                        </core:otherwise>
                                    </core:choose>

                                    <input type="password" class="form-control" id="newpassword"
                                           placeholder="PASSWORD(MORE THAN 6 CHARACTERS)"
                                           name="password">
                                    <span id="passwordstrength" style="color:red"></span>
                                    <input type="text" class="form-control" id="fname" placeholder="FIRST NAME"
                                           name="fname" value="${user.fname}">
                                    <input type="text" class="form-control" id="lname" placeholder="LAST NAME"
                                           name="lname" value="${user.lname}">
                                    <input type="date" class="form-control" id="dob" max="2010-01-01" name="dob"
                                           value="1940-07-13">
                                    <input type="text" class="form-control" id="country" placeholder="COUNTRY/REGION"
                                           name="country" value="${user.country}">
                                    <textarea rows="9" class="form-control" id="bio" placeholder="BIO"
                                              name="bio">${user.bio}</textarea>
                                    <div class="card" style="border: none">
                                    </div>
                                    <br>

                                    <%--reCAPTCHA box--%>
                                    <core:if test="${user==null}">
                                        <div style="text-align: center;">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6LcqHo4UAAAAABxffzmNF0QeLrlaVzJhtKlUvTot"
                                                 data-callback="recaptchaCallback" style="display: inline-block;"></div>
                                        </div>
                                        <br>
                                        <%--Tests if user is creating or updating profile--%>
                                    </core:if>
                                    <input class="btn btn-outline-secondary form-control btn-sm"
                                    <core:out value="${user eq null ? 'disabled': ''}"/> type="submit" id="create"
                                           value="<core:out value="${user eq null ? 'CREATE': 'UPDATE'}"/>">
                                </form>

                                <script>
                                    function recaptchaCallback() {
                                        var btnSubmit = document.getElementById("create");
                                        btnSubmit.attributes.removeNamedItem("disabled");
                                        usernameTest();
                                    }
                                </script>

                                <%--Allows user to delete account--%>
                                <core:if test="${user!=null}">
                                    <form action="AccountCreation" method="get" style="border: none;"
                                          onsubmit="return confirm('Are you sure you want to delete your profile. This is total and irreversible');">
                                        <input type="hidden" id="deluser" name="deluser_id" value="${user.user_id}">
                                        <input class="btn btn-outline-secondary form-control btn-sm" type="submit"
                                               id="delete"
                                               name="delete"
                                               value="DELETE ACCOUNT">
                                    </form>
                                </core:if>
                            </div>
                        </div>
                        <hr>

                        <%--RSS Feed--%>
                        <core:if test="${user!=null}">
                            <div class="container-fluid col-lg-4 col-md-6 col-sm-12">
                                <iframe src="Resources/RSSv2.jsp" target="_parent" scrolling="yes" frameborder="0"
                                        height="690">
                                </iframe>
                            </div>
                        </core:if>
                    </div>
                    <br>
                </div>
                <br>

                <%--Articles that are due to be published--%>
                <core:if test="${user!=null}">
                <h4 style="text-align: center;font-family: 'Julius Sans One', sans-serif;">MY UP COMING ARTICLES</h4>
                <div class="card" class=col-sm-6">
                    <div class="card-header">
                    </div>
                    <core:forEach items="${articles}" var="articles">
                        <div class="card" class=col-sm-6">
                            <div class="card-body">
                                    <%--Article title links to article--%>
                                <a href="SingleArticleServlet?articleID=${articles.articleID}" class="text-dark"
                                   id="hp_article_title1">
                                    <h5 class="card-title"
                                        style="font-family: 'Julius Sans One', sans-serif;">${articles.title} </h5>
                                </a>
                                <a href="url" class="text-dark">
                                    <h6 id="hp_author1"
                                        style="font-family: 'Titillium Web', sans-serif;">${articles.user_id}</h6>
                                </a>
                                <p class="card-text" id="hp_article_content1"
                                   style="font-family: 'Titillium Web', sans-serif;">${articles.publish_time}</p>
                                <a href="SingleArticleServlet?articleID=${articles.articleID}"
                                   class="btn btn-outline-secondary  btn-sm"
                                   style="font-family: 'Rajdhani', sans-serif;">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"
                                          STYLE="color: black;padding-right: 10px"></span>
                                    VIEW ARTICLE
                                </a>
                                <a href="UserMediaServlet?articleID=${articles.articleID}"
                                   class="btn btn-outline-secondary  btn-sm"
                                   style="font-family: 'Rajdhani', sans-serif;">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"
                                        STYLE="color: black;padding-right: 10px"></span>
                                    EDIT
                                </a>
                                <core:if test="${user!=null && (user.user_id == articles.user_id)}">
                                    <a href="ArticleMaker?delArticleID=${articles.articleID}&user_id=${user.user_id}"
                                       class="btn btn-outline-secondary  btn-sm"
                                       style="font-family: 'Rajdhani', sans-serif;">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true" style="color: black;padding-right: 10px"></span>
                                        DELETE
                                    </a>
                                </core:if>
                            </div>
                        </div>
                    </core:forEach>
                </div>
            </div>
        </div>
        </core:if>
    </body>
</html>