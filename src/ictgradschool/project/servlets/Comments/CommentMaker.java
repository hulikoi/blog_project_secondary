package ictgradschool.project.servlets.Comments;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CommentMaker extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Comment comment = new Comment();
        comment.setComment(request.getParameter("comment"));
        comment.setAtricle_id(Integer.parseInt(request.getParameter("articleID")));
        comment.setComment_BY(request.getParameter("comment_BY"));
        comment.setComment_to(Integer.parseInt(request.getParameter("comment_to")));
        CommentDAO dao = new CommentDAO(getServletContext());
        dao.addComment(comment, user);
        request.setAttribute("articleID", comment.getAtricle_id());
        request.getRequestDispatcher("SingleArticleServlet").forward(request, response);
    }

    //get method does comment delete, returns user to page, on failure attaches a denied attribute
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int commentID = Integer.parseInt(request.getParameter("delCommentID"));
        int articleID = Integer.parseInt(request.getParameter("articleID"));
        String user_id = request.getParameter("user_id");

        CommentDAO dao = new CommentDAO(getServletContext());
        if (dao.delComment(commentID, articleID, user_id)) {
            request.setAttribute("articleID", articleID);
            request.getRequestDispatcher("SingleArticleServlet").forward(request, response);
        } else {
            request.setAttribute("denied", "denied");
            request.getRequestDispatcher("SingleArticleServlet").forward(request, response);
        }

    }
}
