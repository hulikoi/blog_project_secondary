package ictgradschool.project.servlets.Articles;

import ictgradschool.project.servlets.Accounts.User;
import ictgradschool.project.servlets.Comments.Comment;
import ictgradschool.project.servlets.Comments.CommentDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@WebServlet(name = "VoteServlet")
public class VoteServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int articleID = Integer.parseInt(request.getParameter("articleID"));
        HttpSession session = request.getSession();
        ArticleDAO dao = new ArticleDAO(getServletContext());
        String voteDirection = request.getParameter("voteDirection");
        dao.voteArticle(voteDirection, articleID);
        CommentDAO dao2 = new CommentDAO(getServletContext());
        Article article = dao.getArticleByID(articleID);
        request.setAttribute("article", article);
        ArrayList<Comment> comment = dao2.getCommentByArticleID(articleID);
        request.setAttribute("comment", comment);
        request.setAttribute("articleID", articleID);
        request.getRequestDispatcher("SingleArticleView.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String title = request.getParameter("title");
//        String content = request.getParameter("content");
//        int articleID = Integer.parseInt(request.getParameter("articleID"));
//        String publish_time = request.getParameter("publish_time");
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Date javaPT = null;
//        try {
//            javaPT = format.parse(publish_time);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        java.sql.Date sqlPT = new java.sql.Date(javaPT.getTime());
//
//        HttpSession session = request.getSession();
//        User user = (User) session.getAttribute("user");
//        ArticleDAO dao = new ArticleDAO(getServletContext());
//        dao.replaceArticle(articleID, title, content, sqlPT,banner);
//
//        ArrayList<Article> articles = dao.getArticles(user);
//        request.setAttribute("articles", articles);
//        dao.getArticles(user);
//
//        request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
    }


}
